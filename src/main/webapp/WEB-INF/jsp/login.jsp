<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function() {
	var resultMsg = "${resultMsg}";
	if(resultMsg != ""){
		alert(resultMsg);
		//deleteCookie("userInputId");
	}
	
	$("#searchIDtel").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
	$("#searchPWtel").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
	
    //저장된 쿠키값을 가져와서 ID에 넣어준다. 없으면 공백으로 들어감.
    var userInputId = getCookie("userInputId");
    $("input[name='userid']").val(userInputId); 
     
    //ID칸에 값이 있으면 아이디 저장 체크박스 체크 
    if($("input[name='userid']").val() != ""){
        $("#checkIDSave").attr("checked", true);
    }
    
    if(userInputId==""){
    	$("#userid").focus();
    }else{
    	$("#userpw").focus();
    }
});
 
function fnCookieSaveID(){
    if($("#checkIDSave").is(":checked")){ // ID 저장하기 체크했을 때,
        var userInputId = $("input[name='userid']").val();
        setCookie("userInputId", userInputId, 7); // 7일 동안 쿠키 보관
    }else{ // ID 저장하기 체크 해제 시,
        deleteCookie("userInputId");
    }
}
 
 
function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}
 
function deleteCookie(cookieName){
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}
 
function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}


function searchUserID(){
	if ($.trim($("#searchIDtel").val()) =="") {
		alert("전화번호를 입력하세요");
		$("#searchIDtel").focus();
	    return;
	}
	
	var userTEL = $("#searchIDtel").val();
	
	$.ajax({
		url : "/SearchUserInfo.do",
		type : "post",
		data :{ "TEL" : userTEL,
			    "GBN" : "searchid" }
	})
	.done(function(jsonData){
		var aryID = new Array();
		aryID = jsonData.data;
		
		if(aryID.length == 1){
			let id = aryID[0].USER_ID;
			var maskingID = fnMaskingID(id)
			alert("정보와 일치하는 아이디입니다.\n"+ maskingID);
			
		}else if(aryID.length > 1){
			var msg = "정보와 일치하는 아이디가 "+ aryID.length +"개 있습니다.\n";
			
			for(key in aryID){
				msg += fnMaskingID(aryID[key].USER_ID) + "\n";
			}
			alert(msg);
		}
		else{
			alert("정보와 일치하는 아이디가 없습니다.");
		}
	});
}

//비밀번호 재설정 아이디 확인
function searchUserPW(){
	if ($.trim($("#searchPWid").val()) =="") {
		alert("아이디를 입력하세요");    
		$("#searchPWid").focus();
	    return;
	}
	
	if ($.trim($("#searchPWtel").val()) =="") {
		alert("전화번호를 입력하세요");
		$("#searchPWtel").focus();
	    return;
	}

	var userID = $("#searchPWid").val();
	var userTEL = $("#searchPWtel").val().replace(/[\-]/gi,"");
	
	$.ajax({
		url : "/SearchUserInfo.do",
		type : "post",
		data :{ "USER_ID" : userID,
				"TEL" : userTEL,
			    "GBN" : "searchpw" }
	})
	.done(function(jsonData){
		var aryID = new Array();
		aryID = jsonData.data;
		
		if(aryID.length == 1){
			$('#layerbox').css('height', 660);
			$('#trPW1').css('display','table-row');
			$('#trPW2').css('display','table-row');
			$('#trPW3').css('display','table-row');
			
			$("#inputUSER_PWD").focus();
		}
		else{
			$('#layerbox').css('height', 460);
			$('#trPW1').css('display','none');
			$('#trPW2').css('display','none');
			$('#trPW3').css('display','none');
			
			alert("일치하는 정보가 없습니다.");
		}
	});
}

//비밀번호 재설정 
function resettingPW(){
	if ($.trim($("#inputUSER_PWD").val()) =="") {
		alert("비밀번호를 입력해주세요.");
		$("#inputUSER_PWD").focus();
		return;
	}
	
	if ($.trim($("#inputUSER_PWD_Check").val()) =="") {
		alert("비밀번호 확인을 입력해주세요.");
		$("#inputUSER_PWD_Check").focus();
		return;
	}
	
	if($.trim($("#inputUSER_PWD").val()) != "" && $.trim($("#inputUSER_PWD_Check").val()) != ""){
		if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
			alert("비밀번호가 일치하지 않습니다.");
			$("#inputUSER_PWD").focus();
			return;
		}
	}
	var User_ID = $("#searchPWid").val();
	var User_PW = $("#inputUSER_PWD").val();
	
	$.ajax({
		url : "/resettingPassword.do",
		type : "post",
		data :{ "USER_ID" : User_ID,
				"USER_PWD" : User_PW }
	})
	.done(function(jsonData){
		popupClose();
		alert("비밀번호가 재설정 되었습니다.\n로그인 해주세요.");
		$("#userid").focus();
	});
	
}

//로그인
function actionLogin() {
 	if ($.trim($("#userid").val()) =="") {
		alert("아이디를 입력하세요");
		$("#userid").focus();
	    return;
	    
	} else if ($.trim($("#userpw").val()) =="") {
	    alert("비밀번호를 입력하세요");
	    $("#userpw").focus();
	    return;
	    
	} else {
		
		fnCookieSaveID();
		
        var comSubmit = new ComSubmit();
        comSubmit.setUrl("<c:url value='/Login.do'/>");
        comSubmit.addParam("USER_ID",$("#userid").val());
        comSubmit.addParam("USER_PWD",$("#userpw").val());
        comSubmit.submit();
	}
}

//아이디 찾기 아이디 마스킹
function fnMaskingID(strID){
	let originID = strID;
	let strLength = originID.length;

	//아이디는 5자리 이상임
	var originName = originID.split('');
    originName.forEach(function(name, i) {
      if (i === 0 || i === originName.length - 1){
    	  return;
      }
      originName[i] = '*';
    });
    
    var maskingID = originName.join();
    return maskingID.replace(/,/g, '');
    /* 
	if(strLength < 3){
		maskingID = originID.replace(/(?<=.{1})./gi, "*");
	}else {
		maskingID = originID.replace(/(?<=.{2})./gi, "*"); 
	} 
	
	return maskingID; */
}

//유저 정보 찾기 팝업
function fnUserInfoSearch(){
	$('#layerbox').css('height', 460);
	$('#trPW1').css('display','none');
	$('#trPW2').css('display','none');
	$('#trPW3').css('display','none');
	
	$('#searchIDtel').val('');
	$('#searchPWid').val('');
	$('#searchPWtel').val('');
	$('#inputUSER_PWD').val('');
	$('#inputUSER_PWD_Check').val('');
	
	//팝업창 띄우기
    $('.layerpop').css("position", "absolute");
    //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
    $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
    $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
    $('.layerpop').draggable();
    $('#layerbox').show();
    
    wrapWindowByMask();
}

</script>
</head>
<body>
	<!-- <form id="loginForm" name="loginForm" class="login100-form validate-form flex-sb flex-w" method="post"> -->
	<div class="limiter" >
		<div class="container-login100" >
			<div class="wrap-login100">
				
					<span class="login100-form-title">
						<img src="images/logo_login.png"  alt="사후관리시스템"/> 
					</span>

					<span class="txt1">
						아이디
					</span>
					<div class="wrap-input100 validate-input " data-validate = "Username is required">
						<input id="userid" name="userid" class="input100" type="text" autocomplete="off">
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						비밀번호
					</span>
					<div class="wrap-input100 validate-input " data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input id="userpw"class="input100" type="password" name="userpw" onkeydown="javascript:if (event.keyCode == 13) { actionLogin(); }">
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full ">
						<div class="contact100-form-checkbox">
							<input id="checkIDSave" name="remember-me" class="input-checkbox100" type="checkbox" >
							<label for="checkIDSave" class="label-checkbox100">
								아이디저장
							</label>
						</div>

						<div>
							<a href="javascript:fnUserInfoSearch();" class="txt3">
								아이디/비밀번호 찾기
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button" onClick="javascript:actionLogin()">
							로그인
						</button>
					</div>

				
			</div>
		</div>
	</div>
	<!-- </form> -->


<!-- Popup Start -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 600px; height: 460px;">
    <article class="layerpop_area" >
      <div class="title" style="cursor:move;">아이디 / 비밀번호 찾기</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content" > 
	
        <!-- 문의 폼 시작 -->

        <div class="layerpop-write-con-id" >
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
				<caption>
             아이디찾기
              </caption>
				<thead><tr><td><i class="fas fa-user"></i> 아이디찾기</td></tr></thead>
              <tbody>
                <tr>
                  <th scope="row">전화번호</th>
                  <td><input id="searchIDtel" type="text" class="write-input width100" autocomplete="off" tabindex="1" placeholder="'-' 없이 숫자만 입력해주세요" onkeydown="javascript:if (event.keyCode == 13) { searchUserID(); }"></td>
                </tr>
                
				<tr><th style="height: 0"></th>
				  <td>
				  	<button type="submit" class="td_button" style="width: 100%" onClick="javascript:searchUserID()">
				  		찾기
				  	</button>
				  </td>
				</tr>
              </tbody>
            </table>
          </article>
        </div>
		  
        <div class="layerpop-write-con-pw" >
          <article class="layerpop-write-tbl-box">
			  
			   <table class="layerpop-write-tbl" >
              <caption>
             비밀번호 재설정
              </caption>
             <thead><tr><td> <i class="fas fa-unlock"></i> 비밀번호 재설정</td></tr></thead>
              <tbody >
                <tr>
                  <th scope="row">아이디</th>
                  <td><input id="searchPWid" type="text" class="write-input width70" autocomplete="off" tabindex="2" placeholder="아이디를 입력해주세요" onkeydown="javascript:if (event.keyCode == 13) { searchUserPW(); }"></td>
                </tr>
                <tr>
                  <th scope="row">전화번호</th>
                  <td><input id="searchPWtel" type="text" class="write-input width100" autocomplete="off" tabindex="3" placeholder="'-' 없이 숫자만 입력해주세요" onkeydown="javascript:if (event.keyCode == 13) { searchUserPW(); }"></td>
                </tr>
                <tr>
                  <th style="height: 0; padding: 20px 0 0 0;"></th>
				  <td>
				    <button type="submit" class="td_button" style="width: 100%" onClick="javascript:searchUserPW()">
				      	찾기
				    </button>
				  </td>
			    </tr>
                <tr id="trPW1" style="display:none;">
				  <th scope="row">비밀번호</th>
                  <td><input id="inputUSER_PWD" type="password" class="write-input width100" autocomplete="off" tabindex="4"></td>
			    </tr>
			    <tr id="trPW2" style="display:none;">
				  <th scope="row">비밀번호 확인</th>
                  <td><input id="inputUSER_PWD_Check" type="password" class="write-input width100" autocomplete="off" tabindex="5"></td>
			    </tr>
                <tr id="trPW3" style="display:none;">
                  <th style="height: 0; padding: 20px 0 0 0;"></th>
				  <td>
				    <button type="submit" class="td_button" style="width: 100%" onClick="javascript:resettingPW()">
				      	재설정
				    </button>
				  </td>
			    </tr>
              </tbody>
            </table>
          </article>
        </div>
    
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%; margin-top: 30px" >
        
          <button type="submit" class="btn-style01"  onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--Popup End -->

</body>
<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</html>