<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout.css">
<link rel="stylesheet" href="css/layout_responsive.css">
<link rel="stylesheet" href="css/board.css">
<!-- 게시판 제작시 사용 -->
<link rel="stylesheet" href="css/board_responsive.css">
<!-- 게시판(반응형, 모바일) 제작시 사용 --> 
<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
 <!-- 용도모르는 스크립트 확인요망!!! -->	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script>
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script> 
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 
<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<!-- 레이어팝업 스크립트 --> 
<link rel="stylesheet" href="css/layerpop.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script type="text/javascript">
	//삭제된 기존 첨부파일 SEQ
	var removeSEQ = new Array();
	
	//해당 글 쿼리 조회 결과 data
	var dataJSON = ${jsonArray};
	//ITEM_1 + ITEM_2 코드
	//콤보박스 선택 조건으로 사용
	var ITEM_1_2_CD;
	if(dataJSON.length == 1){
		ITEM_1_2_CD = dataJSON[0].ITEM_1 + "_" +dataJSON[0].ITEM_2;
	}
	
$(document).ready(function() {
	$("#inputTEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
	
	//콤보박스 변경 이벤트
	fnSelectChanged($("select[name=comboGBN_1]").val());
	
	//기존에 저장된 첨부파일 목록
	var jsonSavedFiles = ${jsonSavedFiles};
	
	var ul = $('#ulFiles');
	var addCode = "";
	
	for(i in jsonSavedFiles){
		addCode ="";
		addCode += "<li id='liSavedFile_"+ jsonSavedFiles[i].FLE_SEQ +"'><span style='color:#c89b43'><i class='far fa-file'></i>";
		addCode += jsonSavedFiles[i].FLE_INFO;
		addCode += "</span><a href='javascript:fnSavedFileRemove("+ jsonSavedFiles[i].FLE_SEQ +")'><i class='fas fa-times' style='padding-left:5px; color:#808080;'></i></a></li>";
		ul.append(addCode);
	}
	
	fnBtnAndMsgShowCheck();
});

	///////////////////// 공 통 /////////////////////
	//사용자 정보 전역변수
	var loginfo = ${loginfoJSON};
	
	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}

	//사용자 정보 수정
	function fnUserInfo(){
		$('#inputUSER_ID').val(loginfo.USER_ID);
		$('#inputUSER_NM').val(loginfo.USER_NM);
		$("#comboUSER_POS").val(loginfo.USER_POS);
		$("#inputCOMP_NM").val(loginfo.COMP_NM);
		$("#inputTEL").val(loginfo.TEL);
		$("#inputEMAIL").val(loginfo.EMAIL);
		$("input[name='radioSMS_YN']:radio[value='" + loginfo.SMS_RCV_YN + "']").prop('checked',true);
		
		//팝업 띄우기
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show();
        
        wrapWindowByMask();
	}
	
	//사용자 저장 이벤트
	function fnUserSave(){
		if ($.trim($("#inputUSER_NM").val()) =="") {
			alert("이름은 필수입력 항목입니다.");
			$("#inputUSER_NM").focus();
			return;
		}
		if($.trim($("#inputUSER_NM").val()).length > 20){
			alert("이름은 20자 이하로 설정해주세요.");
			$("#inputUSER_NM").focus();
			return;
		}
		
		if($("#inputUSER_PWD").val() != "" || $("#inputUSER_PWD_Check").val() != ""){
			if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
				alert("비밀번호가 일치하지 않습니다.");
				$("#inputUSER_PWD").focus();
				return;
			}
		}
		
		if ($.trim($("#inputCOMP_NM").val()) =="") {
			alert("회사명은 필수입력 항목입니다.");
			$("#inputCOMP_NM").focus();
			return;
		}
		if($.trim($("#inputCOMP_NM").val()).length > 20){
			alert("회사명은 20자 이하로 설정해주세요.");
			$("#inputCOMP_NM").focus();
			return;
		}
		
		if ($.trim($("#inputTEL").val()) =="") {
			alert("연락처는 필수입력 항목입니다.");
			$("#inputTEL").focus();
			return;
		}
		if($.trim($("#inputTEL").val()).length > 13){
			alert("연락처는 13자 이하로 설정해주세요.");
			$("#inputTEL").focus();
			return;
		}
		
		if ($.trim($("#inputEMAIL").val()) =="") {
			alert("이메일은 필수입력 항목입니다.");
			$("#inputEMAIL").focus();
			return;
		}
		if(!($.trim($("#inputEMAIL").val()).match("@"))){
			alert("이메일 형식으로 입력해주세요.");
			$("#inputEMAIL").focus();
			return;
		}
		if($.trim($("#inputEMAIL").val()).length > 30){
			alert("이메일은 30자 이하로 설정해주세요.");
			$("#inputEMAIL").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userMerge.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("MNU_KIND","edit");
	        comSubmit.addParam("USER_ID",$.trim($("#inputUSER_ID").val()));
	        comSubmit.addParam("USER_NM",$.trim($("#inputUSER_NM").val()));
	        comSubmit.addParam("USER_PWD",$("#inputUSER_PWD").val());
	        comSubmit.addParam("USER_POS",$("select[name=comboUSER_POS]").val());
	        comSubmit.addParam("COMP_NM",$.trim($("#inputCOMP_NM").val()));
	        comSubmit.addParam("TEL",$.trim($("#inputTEL").val()));
	        comSubmit.addParam("EMAIL",$.trim($("#inputEMAIL").val()));
	        comSubmit.addParam("AUTH_SETUP",loginfo.AUTH_SETUP);
	        comSubmit.addParam("SMS_RCV_YN",$('input[name="radioSMS_YN"]:checked').val());
	        comSubmit.addParam("USE_YN",loginfo.USE_YN);
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        comSubmit.submit();
		}
	}
	///////////////////////////////////////////////

	//목록으로
	function fnBoardList(){
		if(confirm('작성중인 내용은 저장되지 않습니다.')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/board.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
			comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
			comSubmit.submit();
		}
	}
	
	//게시글 삭제
	function fnDelete(){
		if(confirm('해당 게시글을 삭제하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/boardDelete.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
			comSubmit.submit();
		}
	}
	
	//첫번째 구분 콤보박스 변경 이벤트
	function fnSelectChanged(DTL_CD){
		$.ajax({
			url : "/ComboChanged.do",
			type : "post",
			data :{ "MNU_PATH" : "${MNU_PATH}",
					"DTL_CD" : DTL_CD  }
		})
		.done(function(jsonData){
			//console.log(jsonData.combo2Items);
			$("#comboGBN_2").find('option').remove();
			
			var selectOpt = "";
			
			var ary = jsonData.combo2Items;
		
			for(i in ary){
				
				if(ITEM_1_2_CD == ary[i].MST_CD + "_" + ary[i].DTL_CD){
					selectOpt += "<option value='"+ary[i].DTL_CD+"' selected>";
				}else{
					selectOpt += "<option value='"+ary[i].DTL_CD+"'>";					
				}
				selectOpt += ary[i].DTL_NM;
				selectOpt += "</option>";
			}
			//console.log(selectOpt);
			$("#comboGBN_2").append(selectOpt);

		});
	}


	//파일첨부버튼  & '파일을 선택해주세요' 메세지 표시 체크    
	function fnBtnAndMsgShowCheck(){
		var ary = new Array();
		var ul = $('#ulFiles');
		
		if($('#ulFiles').children().size() == 0){
			$("#lb_1").css('display','block');
			var addLiMsg = "<li id='liMsg'><p style='color: rgba(255, 255, 255, 0.3); font-size: 15px;'>동영상 파일을 선택해주세요 </p></li>"; 
			ul.append(addLiMsg);
		}else{
			//파일불러오기 1 버튼(label) 히든처리
			$("#lb_1").css('display','none');
			$('li').remove("#liMsg");
		}
		
	}
	
	//파일 첨부 이벤트
	function fnInputChanged(obj){
		var file = obj.files;
		
		if(file[0].size > 20 * 1024 * 1024){
			alert("20MB 이하 파일만 선택할 수 있습니다.");
			$("#"+obj.id).val('');
			return;
		}
		
		fnAddFileChanged();
	}
	
	//첨부파일 변경 이벤트 
	//첨부파일 추가
	function fnAddFileChanged(){
		var ul = $('#ulFiles');
		var addCode = "";

    	//기존에 저장된 첨부파일 목록(Query result)
    	var jArySavedFiles = ${jsonSavedFiles};
    	
		var liID = "#inputFile_1";
		if($(liID)[0].files.length == 1){
			
    		var sizeByte = $(liID)[0].files[0].size;
    		var sizeKB = parseInt(sizeByte / 1024);
    		var cal = sizeKB / 1024 * 100;
    		var sizeMB = Math.floor(cal) / 100;

    		addCode = "";
    		addCode += "<li style='padding: 5px 5px;'><span style='color:#c89b43;'><i class='far fa-file'></i>";
    		if(sizeKB >= 1000){
    			addCode += $(liID)[0].files[0].name + " (" + sizeMB + " MB)";
    		}else{
    			addCode += $(liID)[0].files[0].name + " (" + sizeKB + " KB)";
    		}
    		//var i_target = i +",'"+ target + "'";
    		addCode += "</span><a href=javascript:fnFileRemove()><i class='fas fa-times' style='padding-left:5px; color:#808080;'></i></a></li>";
     		ul.append(addCode);
		}
    	fnBtnAndMsgShowCheck();
	}
	
	//추가한 첨부파일 삭제
	//해당 파일 input 초기화한 후 값 있는 input file들로 다시 바인딩됨 
	function fnFileRemove(){
		//파일 input 초기화 시킴
		$("#inputFile_1").val('');
		$('#ulFiles').children()[0].remove();
		
		fnBtnAndMsgShowCheck();
	}
	
	//기존에 저장한 첨부파일 삭제
	function fnSavedFileRemove(fileSEQ){
		//전역 변수에 삭제할 file SEQ 담아서 저장시 삭제 처리
		removeSEQ.push(fileSEQ);
		console.log("removeSEQ = " + removeSEQ);
		
		//리스트 삭제
		var liID = "#liSavedFile_" + fileSEQ;
		$("li").remove(liID);
		
		fnBtnAndMsgShowCheck();
	}
	
	//파일 저장
	function fileSave(){
	
		if ($.trim($("#inputTTL").val()) =="") {
			alert("제목을 입력해주세요.");
			$("#inputTTL").focus();
			return;
		}
		
/* 		if ($.trim(CKEDITOR.instances.inputCONTENTS.getData()) =="") {
			alert("내용을 입력해주세요.");
			$("#inputCONTENTS").focus();
			return;
		} 
		
 		if ($.trim($("#inputCONTENTS").val()) =="") {
			alert("내용을 입력해주세요.");
			$("#inputCONTENTS").focus();
			return;
		}*/
		
 		if($('#ulFiles').children()[0].id == "liMsg"){
			alert("동영상 파일을 선택해주세요.");
			return;
		} 
		
		
		if(confirm('저장하시겠습니까?')){
			var form = $('#uploadForm')[0];
		    var formData = new FormData(form);
		 
		    $.ajax({
		        url : '/fileSave.do',
		        type : 'POST',
		        data : formData,
		        contentType : false,
		        processData : false,
		        enctype: 'multipart/form-data'
		    }).done(function(data){
		    	fnSave(data.savedBBS_SEQ);
		    });
		}
	}
	

	//저장
	function fnSave(savedBBS_SEQ){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/boardSave.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
		comSubmit.addParam("MNU_PATH","${MNU_PATH}");
		comSubmit.addParam("searchedBBSSeq",savedBBS_SEQ);
		comSubmit.addParam("BIZ_SEQ",$("select[name=comboSite]").val());
		comSubmit.addParam("TTL",$.trim($("#inputTTL").val()));
		comSubmit.addParam("ITEM_1_DTL",$("select[name=comboGBN_1]").val());
		comSubmit.addParam("ITEM_2_DTL",$("select[name=comboGBN_2]").val());
		comSubmit.addParam("CONTENTS",$.trim($("#inputCONTENTS").val()));
        comSubmit.addParam("searchedPageNo","");
        comSubmit.addParam("searchedWord","");
        comSubmit.addParam("searchedBasicCombo","");
        comSubmit.addParam("searchedBizSeq","");
        comSubmit.addParam("searchedItem1Combo","");
        comSubmit.addParam("removeFileSEQ",removeSEQ);
		comSubmit.submit();
	}

</script> 
</head>


<body>

<!-- code -->
<div id="wrap"> 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"> <a href="javascript:fnMain()"> <img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix"> 
          <div class="header-lang trans300">
            <span>
                <span><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.</span> 
                <a href="javascript:fnLogout()" style="margin-left:5px;">logout</a>
            </span>
          </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
         <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
          <!-- GNB Mobile --> 
          <a href="javascript;" class="nav-open-btn" title="네비게이션 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3" ></span> </a> </div>
      </div>
      <nav id="gnb" class="each-menu">
	      <ul class="clearfix">
	         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
	      </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
             <div class="header-lang-m "><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.<span ><a href="javascript:fnLogout()">logout</a></span></div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP == 'U'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
        </ul>
      </nav>
    </div>
  </header>
   
  <!-- //header -->
  <!-- container -->
  <div id="container"> 
    <!-- visual -->
    
    <section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
      <div class="area visual-txt-con">
        <h2 class="visual-tit trans400"> 동영상자료 </h2>
        <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
      </div>
    </section>
    <!-- //visual --> 
    <!-- middleArea -->
    <div id="middleArea"> 
      <!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
      
<!--       <aside id="sideMenu">
        <div class="side-menu-inner area">
          <ul id="subGnb" class="snb clearfix">
            <div class="nav-on-icon main-move-line">움직이는 바 
              <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
            <li class="on"> <a href="#"><span class="trans300"><em>교육자료관리</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>동영상자료</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>문서자료</em></span></a> </li>
          </ul>
        </div>
      </aside> -->
      
	<c:forEach items="${MNUList}" var="item_1" varStatus="status">
	  <c:if test="${item_1.MNU_CD == MNU_CD}">
	    <aside id="sideMenuM" class="cm-top-menu clearfix">
		  <div class="menu-location  location2">
			<span style="line-height:50px; color:#fff; font-weight:500; font-size:15px; margin-left:20px;">${item_1.MNU_NM}</span>
		  </div>
	    </aside>
	  </c:if>
    </c:forEach>
      <!-- // --> 
      
      <!-- 상단정보 (센터정렬) -->
      <aside id="contentInfoCon" class="content-info-style01 trans400">
        <h3 class="content-tit rubik">동영상 자료</h3>
      </aside>
      <!-- content -->
      <section id="content" class="area"> 
        <!-- 컨텐츠 내용 --> 

        <!-- 문의 폼 시작 -->
          <input type="hidden" name="hl" value="kr">
          <div class="bbs-write-con">
            <article class="bbs-write-tbl-box">
              <table class="bbs-write-tbl">
                <caption>
                공지사항 작석폼
                </caption>
                <colgroup>
                <col style="width:20%;">
                <col>
                </colgroup>
                <tbody>
                  
					 <tr>
                    <th scope="row">구분</th>
                    <td>
                      <select name="comboGBN_1" id="comboGBN_1" class="write-select width20" style="margin-right: 5px" onchange="fnSelectChanged(this.value)">
                    	<c:forEach items="${ListItem_1}" var="result" varStatus="status">
                        	<option value="${result.DTL_CD}" <c:if test="${data.ITEM_1==result.DTL_CD}">selected</c:if>>${result.DTL_NM}</option>
                        </c:forEach>
                      </select>
					
                      <select name="comboGBN_2" id="comboGBN_2" class="write-select width30">
                      </select>
						
				    </td>
                  </tr>
					
                  <tr>
                    <th scope="row">제목</th>
                    <td>
                    	<input id="inputTTL" name="inputTTL" type="text" class="write-input width100" 
                    			placeholder="제목을 입력해주세요" value="${data.TTL}">
                    </td>
                  </tr>
                 
                  <tr>
                    <th scope="row">내용</th>
                    <td>
                    	<textarea id="inputCONTENTS" name="inputCONTENTS" class="write-textarea" 
                    				placeholder="내용을 입력해주세요">${data.CONTENTS}</textarea>
                    </td>
                  </tr>
                  
                  <tr>
                    <th scope="row">파일</th>
                    <td class="file_input">
                    
				      <form id="uploadForm" enctype="multipart/form-data">
					      <div id="bo_v_file" class="bo_v_file_cnt"
					      	   style="width:50%; min-height:35px; background:#192942; border:1px solid #071a34; float:left;">
						      <ul id="ulFiles" style="text-align:left">
							 </ul>
					      </div>
					      
					      <div style="float:left; margin-left:10px; padding-top:10px;">
					      	<label id="lb_1"> 파일불러오기
	                        	<input id="inputFile_1" name="uploadFiles" type="file" accept="video/*" onchange="javascript:fnInputChanged(this)">
	                      	</label>
<!-- 	                      	<label id="lb_2" style="display:none;"> 파일불러오기
	                        	<input id="inputFile_2" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this)">
	                      	</label>
	                      	<label id="lb_3" style="display:none;"> 파일불러오기
	                        	<input id="inputFile_3" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this)">
	                      	</label>
	                      	<label id="lb_4" style="display:none;"> 파일불러오기
	                        	<input id="inputFile_4" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this)">
	                      	</label>
	                      	<label id="lb_5" style="display:none;"> 파일불러오기
	                        	<input id="inputFile_5" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this)">
	                      	</label> -->
					      </div>
                      </form>
                    </td>
                  </tr>
                </tbody>
              </table>
            </article>           
          </div>
        <!-- // 문의 폼 끝 --> 		  
		   <div class="cm-btn-controls">
              <button type="submit" class="btn-style01" onClick="javascript:fileSave();">저장</button>
              <button type="submit" class="btn-style01" onClick="javascript:fnDelete()">삭제</button>
              <button type="submit" class="btn-style01" onClick="javascript:fnBoardList()">목록</button>
            </div>
		  
		
        <!-- //컨텐츠 내용 --> 
      </section>
		
      <!-- //content --> 
    </div>
    <!-- //middleArea --> 
  </div>
  <!-- //container --> 
  <!-- footer -->
  
  <footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
    <div id="footerInner" class="clearfix">
      <article id="footerTop" class="">
        <div class="area-box clearfix">
          <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
        </div>
      </article>
    </div>
  </footer>
  
  <!-- 모달 레이어팝업 -->
  <article class="modal-fixed-pop-wrapper">
    <div class="modal-fixed-pop-inner">
      <div class="modal-loading"><span class="loading"></span></div>
      <div class="modal-inner-box">
        <div class="modal-inner-content"> 
          <!-- ajax 내용 --> 
        </div>
      </div>
    </div>
  </article>
  <!-- //footer --> 
</div>
<!-- //code -->
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>		  
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
       
        
         
          <article class="sitemap-wrapper">
		
	   <ul >
         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
           <c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP=='U'}">
             <li>
         	   <h2>${item_1.MNU_NM}</h2>
         	     <ul class="sitemap-2dep">
         		   <c:forEach items="${MNUList}" var="item_2" varStatus="status">
         		     <c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
         		 	   <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a> </li>
         			 </c:if>
         		   </c:forEach>
		        </ul>
		     </li>
	       </c:if>
         </c:forEach>
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>

    <!--sitemap Popup End -->

<!-- 사용자 정보 팝업 -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 700px; height: 570px;">
    <article class="layerpop_area">
      <div class="title">사용자 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <div class="layerpop-write-con" style="padding-right: 20px; float: left; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table class="layerpop-write-tbl" >
              <caption>
              사용자 관리 작석폼
              </caption>
              <colgroup>
              <col >
              <col>
              </colgroup>
              <tbody  >
                <tr>
                  <th scope="row">* 아이디</th>
                  <td>
                  	<input id="inputUSER_ID" type="text" class="write-input width100" autocomplete="off" readonly style="color:#808080;">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호</th>
                  <td>
                  	<input id="inputUSER_PWD" type="password" class="write-input width100" placeholder="비밀번호를 입력해주세요" autocomplete="off" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <th scope="row">직급</th>
                  <td>
                  	<select id="comboUSER_POS" name="comboUSER_POS" class="write-select width100" >
                  	<option value="">선택</option>
		               <c:forEach items="${listUserPOS}" var="UserPOS" varStatus="status">
		               	<option value="${UserPOS.DTL_CD}">${UserPOS.DTL_NM}</option>
		               </c:forEach>
                    </select></td>
                </tr>
                <tr>
                  <th scope="row">* 연락처</th>
                  <td>
                  	<input id="inputTEL" type="text" class="write-input width100" placeholder="연락처를 입력해주세요" autocomplete="off" tabindex="6">
                  </td>
                </tr>
                <tr>
                  <th scope="row">SMS 수신동의 <span class="info_text"> <i class="fas fa-info-circle"></i> SMS 수신 동의시 문자 발송이 됩니다.</span></th>
                  <td>
                  <fieldset class="custom-radio">
                  	<span class="radio-item" >
                      <input id="inputSMS_Y" name="radioSMS_YN" type="radio" value="Y" tabindex="12" checked>
                      <label for="inputSMS_Y" >동의</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputSMS_N" name="radioSMS_YN" type="radio" value="N" tabindex="13">
                      <label for="inputSMS_N" >미동의</label>
                    </span>
                    </fieldset>
                  </td>
                </tr> 
              </tbody>
            </table>
          </article>
        </div>
        <div class="layerpop-write-con" style="padding-left: 20px; float: right; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
              <tbody>
                <tr>
                  <th scope="row">* 이름</th>
                  <td>
                  	<input id="inputUSER_NM" type="text" class="write-input width100" placeholder="이름을 입력해주세요" autocomplete="off" tabindex="2">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호 확인</th>
                  <td>
                  	<input id="inputUSER_PWD_Check" type="password" class="write-input width100" placeholder="비밀번호를 확인해주세요" autocomplete="off" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 회사명</th>
                  <td>
                  	<input id="inputCOMP_NM" type="text" class="write-input width100" placeholder="회사명을 입력해주세요" autocomplete="off" tabindex="5">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 이메일</th>
                  <td>
                  	<input id="inputEMAIL" type="text" class="write-input width100" placeholder="이메일을 입력해주세요" autocomplete="off" tabindex="7">
                  </td>
                </tr>
              </tbody>
            </table>
          </article>
        </div>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%" >
          <button type="submit" class="btn-style01" onClick="javascript:fnUserSave();">저장</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--사용자 정보 팝업 End -->

<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>