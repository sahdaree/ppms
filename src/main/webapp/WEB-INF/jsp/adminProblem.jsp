<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->

<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout_admin.css">
<link rel="stylesheet" href="css/board.css">
<link rel="stylesheet" href="css/layerpop.css">

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">

<!-- 용도모르는 스크립트 확인요망!!! --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script> 
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>
<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script type="text/javascript">
	///////////////////// 공 통 /////////////////////
	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}
	///////////////////////////////////////////////
</script>

</head>

<body >
<div id="wrap"  > 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"><a href="javascript:fnMain()"><img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix">
          <div class="header-lang trans300">
            <span>
                <span>${loginfo.USER_NM}님 환영합니다.&nbsp;&nbsp;</span>
                <a href="javascript:fnLogout()">logout</a>
            </span>
          </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
            <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
      </div>
      <nav id="gnb" class="each-menu">
	      <ul class="clearfix">
	         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep" style="display: none;">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
	      </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
             <div class="header-lang-m " >${loginfo.USER_NM}님 환영합니다.<span > <a  href="#">logout</a> </span>  </div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
        </ul>
      </nav>
    </div>
	  </div>
  </header>
  <!-- //header --> 
<!-- container -->
<div id="container" >
<!-- visual -->

<section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
  <div class="area visual-txt-con">
    <h2 class="visual-tit trans400"> 장애통계 </h2>
    <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
  </div>
</section>
<!-- //visual --> 
<!-- middleArea -->
<div id="middleArea">
<!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->

<!-- <aside id="sideMenu">
  <div class="side-menu-inner area">
    <ul id="subGnb" class="snb clearfix">
      <div class="nav-on-icon main-move-line">움직이는 바 
        <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
      <li class="on"> <a href="#"><span class="trans300"><em>공지사항</em></span></a> </li>
      <li> <a href="#"><span class="trans300"><em>자유게시판</em></span></a> </li>
      <li> <a href="#"><span class="trans300"><em>FAQ</em></span></a> </li>
    </ul>
  </div>
</aside> -->
      <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	   	<c:if test="${item_1.MNU_CD == MNU_CD}">
		  <c:forEach items="${MNUList}" var="item_2" varStatus="status">
			  <c:if test="${item_1.UPPER_CD == item_2.MNU_CD}">
			  	<aside id="sideMenuM" class="cm-top-menu clearfix">
			  	<div class="menu-location  location2"><a href="javascript:;" class="cur-location"><span>${item_2.MNU_NM}</span><i class="material-icons arrow"></i></a>
			      <ul class="location-menu-con">
			  		<c:forEach items="${MNUList}" var="item_3" varStatus="status">
			  	      <c:if test="${item_2.MNU_CD == item_3.UPPER_CD}">
			  			  <c:if test="${item_3.MNU_CD == MNU_CD}">
			  			    <li class="on"><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  			  <c:if test="${item_3.MNU_CD != MNU_CD}">
			  				<li><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  		  </c:if>
			  		</c:forEach>
			  	  </ul>
			  	</div>
			  	</aside>
			  </c:if>
		  </c:forEach>
	   </c:if>
    </c:forEach>
<!-- // --> 

<!-- 상단정보 (센터정렬) -->
<aside id="contentInfoCon" class="content-info-style01 trans400">
  <h3 class="content-tit rubik">장애통계</h3>
</aside>

<!-- content -->
<section id="content" class="area">

	
	
<!----- 조회 시작 ----------->
<aticle >
	
		
	<div class="search-group " style="min-width: 1200px; margin-bottom:10px" >
	<span>
	  <label>사이트별칭</label>
      <select  name="region3" style="width:250px; ">
        <option value="제목">제목</option>
        <option value="시스템명">시스템명</option>
      </select>
	</span>
		 
	<span> 
	  <label >기간</label>
      <input type="date" style="width:130px;" > ~ <input type="date" style="width:130px;"   >
      
    </span>
		<button id="button" style="margin-left: 5px" ><img src="images/icon_input_search.png"  alt="조회"/></button>
			  <a href="#" title="조회">조회 </a> 
    </div>	
		

	

</aticle>
	
<!----- 조회  끝 ----------->
	
<!----- 차트 시작 ----------->
<aticle >
	<div  style="margin-bottom:15px; width: 100%; min-width: 1200px; height: 300px; text-align: center; border: 1px solid #071a34; background: #213753;">
		
		

	</div>	

</aticle>
	
<!----- 차트  끝 ----------->
<!----- 게시판 리스트 시작 ----------->

<article class="bbs-list-con" >
    <h2 class="sound_only">관리자 게시판 목록</h2>
    <div class="bbs-list-Tbl" style="min-width: 1000px">
      <ul>
        <li class="bbs-list-TblTr bbs-list-TblTh">
          <div class="mvInlineN">번호</div>
		  <div class="mvInlineN ">사이트명[별칭]</div>	
		  <div class="mvInlineN ">구분</div>
          <div class="mvInlineN ">제목</div>
		  <div class="mvInlineN td_datetime">접수일자</div>
          <div class="mvInlineN td_datetime">답변일자</div>
          
        </li>
     

        <li class="bbs-list-TblTr bbs-list-TblTd" >
          <div class="mvInlineN td_num2"> 10 </div>
		 <div class="td_site"> 횡성군 상수관망 구축</div>
		  <div class="td_group"> 공지사항 </div>
		  <div class="td_subject"><div class="bo_tit"> 
			  목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다 공지사항 목록입니다 공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다공지사항 목록입니다  </div> </div>         
         
		  <div class="mvInlinev td_datetime">2020-02-26</div>
		  <div class="mvInlinev td_datetime">2020-02-26</div>
         </li>
		  
		  <li class="bbs-list-TblTr bbs-list-TblTd">
          <div class="mvInlineN td_num2"> 9 </div>
		  <div class="td_site"> 횡성군 상수관망 구축</div>
		  <div class="td_group"> 공지사항 </div>
		  <div class="td_subject" ><div class="bo_tit"> 공지사항 목록입니다  </div> </div>     
		  <div class="mvInlinev td_datetime">2020-02-26</div>
	      <div class="mvInlinev td_datetime">2020-02-26</div>
          </li>
		  
		    <li class="bbs-list-TblTr bbs-list-TblTd">
          <div class="mvInlineN td_num2"> 8 </div>
		  <div class="td_site"> 횡성군 상수관망 구축</div>
		  <div class="td_group"> 공지사항 </div>
		  <div class="td_subject" ><div class="bo_tit"> 공지사항 목록입니다  </div> </div>     
		  <div class="mvInlinev td_datetime">2020-02-26</div>
	      <div class="mvInlinev td_datetime">2020-02-26</div>
         
          </li>
		  
		    <li class="bbs-list-TblTr bbs-list-TblTd">
          <div class="mvInlineN td_num2"> 7 </div>
		  <div class="td_site"> 횡성군 상수관망 구축</div>
		  <div class="td_group"> 공지사항 </div>
		  <div class="td_subject" ><div class="bo_tit"> 공지사항 목록입니다  </div> </div>     
		  <div class="mvInlinev td_datetime">2020-02-26</div>
	      <div class="mvInlinev td_datetime">2020-02-26</div>
         
          </li>
		  
		    <li class="bbs-list-TblTr bbs-list-TblTd">
          <div class="mvInlineN td_num2"> 6 </div>
		  <div class="td_site"> 횡성군 상수관망 구축</div>
		  <div class="td_group"> 공지사항 </div>
		  <div class="td_subject" ><div class="bo_tit"> 공지사항 목록입니다  </div> </div>     
		  <div class="mvInlinev td_datetime">2020-02-26</div>
	      <div class="mvInlinev td_datetime">2020-02-26</div>
         
          </li>
		  
		
      </ul>
    </div>
 
</article>
  <!--  게시판 리스트 종료 -->
  
 <!--페이징 필요시  <div class="paging"> <a href="#" class="cur">1</a> <a href="#">2</a> <a href="#">3</a> </div>  -->
	
		
  
  <!-- // 일반 FAQ 시작 --> 
  <!-- //컨텐츠 내용 --> 
</section>
<!-- //content -->
</div>
<!-- //middleArea -->
</div>
<!-- //container --> 
<!-- footer -->

<footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
  <div id="footerInner" class="clearfix">
    <article id="footerTop" class="">
      <div class="area-box clearfix">
        <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
      </div>
    </article>
  </div>
</footer>

<!-- 모달 레이어팝업 -->
<article class="modal-fixed-pop-wrapper">
  <div class="modal-fixed-pop-inner">
    <div class="modal-loading"><span class="loading"></span></div>
    <div class="modal-inner-box">
      <div class="modal-inner-content"> 
        <!-- ajax 내용 --> 
      </div>
    </div>
  </div>
</article>
<!-- //footer -->
</div>
<!-- //code -->
	
		<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 360px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
        <!-- 문의 폼 시작 -->
        
         
          <article class="sitemap-wrapper">
		
		<ul >
          <li>
				<h2>소식</h2>
           <ul class="sitemap-2dep">
              <li> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
              <li> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
              <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>장애관리</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_fault.html"><span class="trans300"><em>장애관리</em></span></a> </li>
            </ul>
          </li>
          <li>
				<h2>예방점검</h2>
           <ul class="sitemap-2dep">
              <li>  <a href="info_preventive.html"><span class="trans300"><em>예방점검</em></span></a>  </li>
            </ul>
          </li>
          <li>
				<h2>교육자료관리</h2>
            <ul class="sitemap-2dep">
                <li> <a href="info_video.html"><span class="trans300"><em>동영상자료</em></span></a> </li>
              <li> <a href="info_file.html"><span class="trans300"><em>문서자료</em></span></a> </li>
            </ul>
          </li>
			
			<li> <h2>관리자모드</h2>
            <ul class="sitemap-2dep">
              <li> <a href="info_admin_site.html"><span class="trans300"><em>사이트관리</em></span></a> </li>
              <li> <a href="info_admin_user.html"><span class="trans300"><em>사용자관리</em></span></a> </li>
              <li> <a href="info_admin_code.html"><span class="trans300"><em>코드관리</em></span></a> </li>
              <li> <a href="info_admin_problem.html"><span class="trans300"><em>장애통계</em></span></a> </li>
            </ul>
          </li>
          
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>
    

	
	
	
    <!--sitemap Popup End -->
	
<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>