<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout_admin.css">
<link rel="stylesheet" href="css/board.css">
<link rel="stylesheet" href="css/layerpop.css">

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">

<!-- 용도모르는 스크립트 확인요망!!! --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script> 
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#inputTEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
	$("#inputCONT_TEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
});

	///////////////////// 공 통 /////////////////////
	//사용자 정보 전역변수
	var loginfo = ${loginfoJSON};
	
	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}

	//사용자 정보 수정
	function fnUserInfo(){
		$('#inputUSER_ID').val(loginfo.USER_ID);
		$('#inputUSER_NM').val(loginfo.USER_NM);
		$("#comboUSER_POS").val(loginfo.USER_POS);
		$("#inputCOMP_NM").val(loginfo.COMP_NM);
		$("#inputTEL").val(loginfo.TEL);
		$("#inputEMAIL").val(loginfo.EMAIL);
		$("input[name='radioSMS_YN']:radio[value='" + loginfo.SMS_RCV_YN + "']").prop('checked',true);
		
		//팝업 띄우기
        $('.layerpopuserinfo').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpopuserinfo').css("top",(($(window).height() - $('.layerpopuserinfo').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpopuserinfo').css("left",(($(window).width() - $('.layerpopuserinfo').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpopuserinfo').draggable();
        $('#layerboxuser').show();
        
        wrapWindowByMask();
	}
	
	//사용자 저장 이벤트
	function fnUserSave(){
		if ($.trim($("#inputUSER_NM").val()) =="") {
			alert("이름은 필수입력 항목입니다.");
			$("#inputUSER_NM").focus();
			return;
		}
		
		if($("#inputUSER_PWD").val() != "" || $("#inputUSER_PWD_Check").val() != ""){
			if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
				alert("비밀번호가 일치하지 않습니다.");
				$("#inputUSER_PWD").focus();
				return;
			}
		}
		
		if ($.trim($("#inputCOMP_NM").val()) =="") {
			alert("회사명은 필수입력 항목입니다.");
			$("#inputCOMP_NM").focus();
			return;
		}
		
		if ($.trim($("#inputTEL").val()) =="") {
			alert("연락처는 필수입력 항목입니다.");
			$("#inputTEL").focus();
			return;
		}
		
		if ($.trim($("#inputEMAIL").val()) =="") {
			alert("이메일은 필수입력 항목입니다.");
			$("#inputEMAIL").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userMerge.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("MNU_KIND","board");
	        comSubmit.addParam("USER_ID",$.trim($("#inputUSER_ID").val()));
	        comSubmit.addParam("USER_NM",$.trim($("#inputUSER_NM").val()));
	        comSubmit.addParam("USER_PWD",$("#inputUSER_PWD").val());
	        comSubmit.addParam("USER_POS",$("select[name=comboUSER_POS]").val());
	        comSubmit.addParam("COMP_NM",$.trim($("#inputCOMP_NM").val()));
	        comSubmit.addParam("TEL",$.trim($("#inputTEL").val()));
	        comSubmit.addParam("EMAIL",$.trim($("#inputEMAIL").val()));
	        comSubmit.addParam("AUTH_SETUP",loginfo.AUTH_SETUP);
	        comSubmit.addParam("SMS_RCV_YN",$('input[name="radioSMS_YN"]:checked').val());
	        comSubmit.addParam("USE_YN",loginfo.USE_YN);
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        comSubmit.addParam("searchedItem2Combo","${searchedCondition.searchedItem2Combo}");
	        comSubmit.submit();
		}
	}
	///////////////////////////////////////////////
	
	//조회&페이지 이벤트
	function fnSearch(pageNo){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedPageNo",pageNo);
	    comSubmit.addParam("searchedBasicCombo","0"); //제목 검색
	    comSubmit.addParam("searchedWord","");
	    comSubmit.addParam("searchedBizSeq","");
	    comSubmit.addParam("searchedItem1Combo","");
	    comSubmit.submit();
	}

	//팝업(등록,수정) 이벤트
	function fnPopup(BIZ_SEQ){
		if(BIZ_SEQ == ""){
			$("#inputBIZ_NM").val('');
			$("#inputBIZ_NM_NICK").val('');
			$("#inputBIZ_START_DT").val('');
			$("#inputBIZ_END_DT").val('');
			$("#inputMATNC_END_DT").val('');
			$("#inputORDG_COMP").val('');
			$("#inputCONT_NM").val('');
			$("#inputCONT_TEL").val('');
			$("#inputBIZ_ADMIN").val('');
			$("#btnBIZ_SEQ").val('-1');
		}else{
			var jsonAry = ${jsonArray};
			for(key in jsonAry){
				if(jsonAry[key].BIZ_SEQ == BIZ_SEQ){
					$("#inputBIZ_NM").val(jsonAry[key].BIZ_NM);
					$("#inputBIZ_NM_NICK").val(jsonAry[key].BIZ_NM_NICK);
					$("#inputBIZ_START_DT").val(jsonAry[key].BIZ_START_DT);
					$("#inputBIZ_END_DT").val(jsonAry[key].BIZ_END_DT);
					$("#inputMATNC_END_DT").val(jsonAry[key].MATNC_END_DT);
					$("#inputORDG_COMP").val(jsonAry[key].ORDG_COMP);
					$("#inputCONT_NM").val(jsonAry[key].CONT_NM);
					$("#inputCONT_TEL").val(jsonAry[key].CONT_TEL);
					$("#inputBIZ_ADMIN").val(jsonAry[key].BIZ_ADMIN);
					$("#btnBIZ_SEQ").val(jsonAry[key].BIZ_SEQ);
					//console.log(jsonAry[key]);
				}
			}
		}
		
		//팝업창 띄우기
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show(); 
        
        wrapWindowByMask();

	}
	
	//저장
	function fnSave(){
		if ($.trim($("#inputBIZ_NM").val()) =="") {
			alert("사이트명은 필수입력 항목입니다.");
			$("#inputBIZ_NM").focus();
			return;
		}
		
		if ($.trim($("#inputBIZ_NM_NICK").val()) =="") {
			alert("사이트명 별칭은 필수입력 항목입니다.");
			$("#inputBIZ_NM_NICK").focus();
			return;
		}
		
		if ($.trim($("#inputBIZ_START_DT").val()) !="" && $.trim($("#inputBIZ_END_DT").val()) !="") {
			if($("#inputBIZ_START_DT").val() > $("#inputBIZ_END_DT").val()){
				alert("사업 시작일자가 종료일자보다 클 수 없습니다.");
				return;
			}
		}
		
		if ($.trim($("#inputMATNC_END_DT").val()) !="") {
			if($.trim($("#inputBIZ_END_DT").val()) !=""){
				if($("#inputBIZ_END_DT").val() >= $("#inputMATNC_END_DT").val()){
					alert("유지보수만료일은 사업기간 이후로 설정해 주세요.");
					return;
				}
			}
			if($.trim($("#inputBIZ_START_DT").val()) !=""){
				if($("#inputBIZ_START_DT").val() >= $("#inputMATNC_END_DT").val()){
					alert("유지보수만료일은 사업기간 이후로 설정해 주세요.");
					return;
				}
			}
		}
		
		if ($.trim($("#inputORDG_COMP").val()) =="") {
			alert("발주처는 필수입력 항목입니다.");
			$("#inputORDG_COMP").focus();
			return;
		}
		
		if ($.trim($("#inputCONT_NM").val()) =="") {
			alert("담당자는 필수입력 항목입니다.");
			$("#inputCONT_NM").focus();
			return;
		}
		
		if ($.trim($("#inputCONT_TEL").val()) =="") {
			alert("전화번호는 필수입력 항목입니다.");
			$("#inputCONT_TEL").focus();
			return;
		}
		
		if ($.trim($("#inputBIZ_ADMIN").val()) =="") {
			alert("현장대리인는 필수입력 항목입니다.");
			$("#inputBIZ_ADMIN").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/siteSave.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
			comSubmit.addParam("MNU_PATH","${MNU_PATH}");
			comSubmit.addParam("BIZ_SEQ",$("#btnBIZ_SEQ").val());
			comSubmit.addParam("BIZ_NM",$.trim($("#inputBIZ_NM").val()));
			comSubmit.addParam("BIZ_NM_NICK",$.trim($("#inputBIZ_NM_NICK").val()));
			comSubmit.addParam("BIZ_START_DT",$.trim($("#inputBIZ_START_DT").val()));
			comSubmit.addParam("BIZ_END_DT",$.trim($("#inputBIZ_END_DT").val()));
			comSubmit.addParam("MATNC_END_DT",$.trim($("#inputMATNC_END_DT").val()));
			comSubmit.addParam("ORDG_COMP",$.trim($("#inputORDG_COMP").val()));
			comSubmit.addParam("CONT_NM",$.trim($("#inputCONT_NM").val()));
			comSubmit.addParam("CONT_TEL",$.trim($("#inputCONT_TEL").val()));
			comSubmit.addParam("BIZ_ADMIN",$.trim($("#inputBIZ_ADMIN").val()));
			comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
			comSubmit.submit(); 
		}
	}

	//삭제
	function fnDelete(){
		if(confirm('삭제하시겠습니까?')){
			if($("#btnBIZ_SEQ").val() == "-1"){
				popupClose();
				return;
			}else{
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/siteDelete.do'/>");
				comSubmit.addParam("MNU_CD","${MNU_CD}");
				comSubmit.addParam("MNU_PATH","${MNU_PATH}");
				comSubmit.addParam("BIZ_SEQ",$("#btnBIZ_SEQ").val());
				comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
				comSubmit.submit(); 
			}
		}
	}
</script>
</head>

<body >
<div id="wrap"  > 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"><a href="javascript:fnMain()"><img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix">
          <div class="header-lang trans300">
            <span>
                <span><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.</span> 
                <a href="javascript:fnLogout()" style="margin-left:5px;">logout</a>
            </span>
          </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
            <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
      </div>
      <nav id="gnb" class="each-menu">
	      <ul class="clearfix">
	         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep" style="display: none;">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
	      </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep">  
             <div class="header-lang-m "><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.<span ><a href="javascript:fnLogout()">logout</a></span></div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
        </ul>
      </nav>
    </div>
	  </div>
  </header>
  <!-- //header --> 
  <!-- container -->
  <div id="container" > 
    <!-- visual -->
    
    <section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
      <div class="area visual-txt-con">
        <h2 class="visual-tit trans400"> 사용자관리 </h2>
        <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
      </div>
    </section>
    <!-- //visual --> 
    <!-- middleArea -->
    <div id="middleArea"> 
      <!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
      
<!--       <aside id="sideMenu">
        <div class="side-menu-inner area">
          <ul id="subGnb" class="snb clearfix">
            <div class="nav-on-icon main-move-line">움직이는 바 
              <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
            <li class="on"> <a href="#"><span class="trans300"><em>전체</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사이트PM</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사용자</em></span></a> </li>
          </ul>
        </div>
      </aside> -->
      <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	   	<c:if test="${item_1.MNU_CD == MNU_CD}">
		  <c:forEach items="${MNUList}" var="item_2" varStatus="status">
			  <c:if test="${item_1.UPPER_CD == item_2.MNU_CD}">
			  	<aside id="sideMenuM" class="cm-top-menu clearfix">
			  	<div class="menu-location  location2"><a href="javascript:;" class="cur-location"><span>${item_2.MNU_NM}</span><i class="material-icons arrow"></i></a>
			      <ul class="location-menu-con">
			  		<c:forEach items="${MNUList}" var="item_3" varStatus="status">
			  	      <c:if test="${item_2.MNU_CD == item_3.UPPER_CD}">
			  			  <c:if test="${item_3.MNU_CD == MNU_CD}">
			  			    <li class="on"><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  			  <c:if test="${item_3.MNU_CD != MNU_CD}">
			  				<li><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  		  </c:if>
			  		</c:forEach>
			  	  </ul>
			  	</div>
			  	</aside>
			  </c:if>
		  </c:forEach>
	   </c:if>
    </c:forEach>
      <!-- // --> 
      
      <!-- 상단정보 (센터정렬) -->
      <aside id="contentInfoCon" class="content-info-style01 trans400">
        <h3 class="content-tit rubik">사이트 관리</h3>
      </aside>
      
      <!-- content -->
      <section id="content" class="area"> 
        
        <!----- 화면분할 시작 ----------->
        
        <div class="admin-cont" > 
          
          <!----- 게시판 리스트 시작 -----------> 
          <!--<div class="admin-title"><span><i class="fas fa-angle-right"></i> 사용자</span>  </div>--> 
          <!----- 조회 시작 ----------->
          <aticle >
            <div class="search-group " > 
              
              <!--관리자만--> 
              <a href="javascript:fnPopup('');"> 등록 </a> </div>
          </aticle>
          <!----- 조회  끝 -----------> 
          
          <!----- 게시판 리스트 시작 ----------->
          
          <article class="bbs-list-con" >
            <h2 class="sound_only">관리자 게시판 목록</h2>
            <div class="bbs-list-Tbl" style="min-width: 1000px" id="divdiv">
              <ul>
                <li class="bbs-list-TblTr bbs-list-TblTh">
                  <div class="mvInlineN">번호</div>
                  <div class="mvInlineN ">사이트명[별칭]</div>
                  <div class="mvInlineN ">발주처</div>
                  <div class="mvInlineN ">사업기간</div>
                  <div class="mvInlineN ">유지보수만료일</div>
                  <div class="mvInlineN ">담당자명</div>
                  <div class="mvInlineN ">연락처</div>
                </li>
                
              <c:forEach items="${data}" var="result" varStatus="status">
	            <li class="bbs-list-TblTr bbs-list-TblTd" style="cursor: pointer;" onclick="javascript:fnPopup('<c:out value="${result.BIZ_SEQ}"/>')">
	              <div class="mvInlineN td_num2">${result.NO}</div>
	              <div class="td_site">${result.BIZ_NM_NICK}</div>
	              <div class="td_client"> ${result.ORDG_COMP} </div>
	              <div class="mvInlinev td_datetime"> ${result.BIZ_PERIOD} </div>
	              <div class="mvInlinev td_datetime"> ${result.MATNC_END_DT} </div>
	              <div class="td_name"> ${result.CONT_NM} </div>
	              <div class="td_tel"> ${result.CONT_TEL}</div>
	           </li>
	         </c:forEach>
	         
              </ul>
            </div>
          </article>
          <!--  게시판 리스트 종료 --> 
        </div>
        <!-----  화면분할  끝 -----------> 
        
        <!-- //컨텐츠 내용 --> 
      </section>
      <!-- //content --> 
    </div>
    <!-- //middleArea --> 
  </div>
  <!-- //container --> 
  <!-- footer -->
  
  <footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
    <div id="footerInner" class="clearfix">
      <article id="footerTop" class="">
        <div class="area-box clearfix">
          <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
        </div>
      </article>
    </div>
  </footer>
  
  <!-- 모달 레이어팝업 -->
  <article class="modal-fixed-pop-wrapper">
    <div class="modal-fixed-pop-inner">
      <div class="modal-loading"><span class="loading"></span></div>
      <div class="modal-inner-box">
        <div class="modal-inner-content"> 
          <!-- ajax 내용 --> 
        </div>
      </div>
    </div>
  </article>
  
  <!-- //footer --> 
</div>
<!-- //code --> 
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 360px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
        <!-- 문의 폼 시작 -->
        
         
          <article class="sitemap-wrapper">
		
	    <ul >
         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
           <c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP=='U'}">
             <li>
         	   <h2>${item_1.MNU_NM}</h2>
         	     <ul class="sitemap-2dep">
         		   <c:forEach items="${MNUList}" var="item_2" varStatus="status">
         		     <c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
         		 	   <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a> </li>
         			 </c:if>
         		   </c:forEach>
		        </ul>
		     </li>
	       </c:if>
         </c:forEach>
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>
    

	
	
	
<!--sitemap Popup End -->
	
<!--code  Popup Start -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 600px; height: 830px;">
    <article class="layerpop_area">
      <div class="title">사이트 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <article class="layerpop-write-tbl-box">
          <table  class="layerpop-write-tbl"  >
            <tbody>
              <tr>
                <th scope="row">* 사이트명</th>
                  <td>
                    <input id="inputBIZ_NM" type="text" class="write-input width100" placeholder="사이트명을 입력해주세요" autocomplete="off">
                  </td>
              </tr>
              <tr>
                <th scope="row">* 별칭  <span class="info_text" style="padding-left: 10px"> <i class="fas fa-info-circle"></i> 사이트 줄임말</span></th>
                  <td>
                    <input id="inputBIZ_NM_NICK" type="text" class="write-input width100" placeholder="별칭을 입력해주세요" autocomplete="off">
                 </td>
              </tr>
              <tr>
                <th scope="row"> 사업기간</th>
                  <td>
                    <input id="inputBIZ_START_DT" type="date" class="write-input width40" placeholder="기간을 선택해주세요">
                     ~
                    <input id="inputBIZ_END_DT" type="date" class="write-input width40" placeholder="기간을 선택해주세요">
                  </td>
              </tr>
              <tr>
                <th scope="row"> 유지보수만료일</th>
                  <td>
                    <input id="inputMATNC_END_DT" type="date" class="write-input width40" placeholder="날짜을 선택해주세요">
                  </td>
              </tr>
              <tr>
                <th scope="row">* 발주처</th>
                  <td>
                    <input id="inputORDG_COMP" type="text" class="write-input width100" placeholder="발주처를 입력해주세요">
                  </td>
              </tr>
              <tr>
                <th scope="row">* 담당자</th>
                  <td>
                    <input id="inputCONT_NM" type="text" class="write-input width100" placeholder="담당자를 입력해주세요">
                  </td>
              </tr>
              <tr>
                <th scope="row">* 전화번호</th>
                  <td>
                    <input id="inputCONT_TEL" type="text" class="write-input width100" placeholder="전화번호를 입력해주세요">
                  </td>
              </tr>
              <tr>
                <th scope="row">* 현장대리인</th>
                  <td>
                    <input id="inputBIZ_ADMIN" type="text" class="write-input width100" placeholder="현장대리인을 입력해주세요">
                  </td>
              </tr>
            </tbody>
          </table>
        </article>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%; " >
          <button id="btnBIZ_SEQ" type="submit" class="btn-style01" onClick="javascript:fnSave();">저장</button>
          <button type="submit" class="btn-style01" onClick="javascript:fnDelete();">삭제</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--Popup End -->


<!-- 사용자 정보 팝업 -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerboxuser" class="layerpopuserinfo" style="width: 700px; height: 570px;">
    <article class="layerpop_area">
      <div class="title">사용자 관리</div>
      <a href="javascript:popupUserClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <div class="layerpop-write-con" style="padding-right: 20px; float: left; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table class="layerpop-write-tbl" >
              <caption>
              사용자 관리 작석폼
              </caption>
              <colgroup>
              <col >
              <col>
              </colgroup>
              <tbody  >
                <tr>
                  <th scope="row">* 아이디</th>
                  <td>
                  	<input id="inputUSER_ID" type="text" class="write-input width100" autocomplete="off" readonly style="color:#808080;">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호</th>
                  <td>
                  	<input id="inputUSER_PWD" type="password" class="write-input width100" placeholder="비밀번호를 입력해주세요" autocomplete="off" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <th scope="row">직급</th>
                  <td>
                  	<select id="comboUSER_POS" name="comboUSER_POS" class="write-select width100" >
                  	<option value="">선택</option>
		               <c:forEach items="${listUserPOS}" var="UserPOS" varStatus="status">
		               	<option value="${UserPOS.DTL_CD}">${UserPOS.DTL_NM}</option>
		               </c:forEach>
                    </select></td>
                </tr>
                <tr>
                  <th scope="row">* 연락처</th>
                  <td>
                  	<input id="inputTEL" type="text" class="write-input width100" placeholder="연락처를 입력해주세요" autocomplete="off" tabindex="6">
                  </td>
                </tr>
                <tr>
                  <th scope="row">SMS 수신동의 <span class="info_text"> <i class="fas fa-info-circle"></i> SMS 수신 동의시 문자 발송이 됩니다.</span></th>
                  <td>
                  <fieldset class="custom-radio">
                  	<span class="radio-item" >
                      <input id="inputSMS_Y" name="radioSMS_YN" type="radio" value="Y" tabindex="12" checked>
                      <label for="inputSMS_Y" >동의</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputSMS_N" name="radioSMS_YN" type="radio" value="N" tabindex="13">
                      <label for="inputSMS_N" >미동의</label>
                    </span>
                    </fieldset>
                  </td>
                </tr> 
              </tbody>
            </table>
          </article>
        </div>
        <div class="layerpop-write-con" style="padding-left: 20px; float: right; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
              <tbody>
                <tr>
                  <th scope="row">* 이름</th>
                  <td>
                  	<input id="inputUSER_NM" type="text" class="write-input width100" placeholder="이름을 입력해주세요" autocomplete="off" tabindex="2">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호 확인</th>
                  <td>
                  	<input id="inputUSER_PWD_Check" type="password" class="write-input width100" placeholder="비밀번호를 확인해주세요" autocomplete="off" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 회사명</th>
                  <td>
                  	<input id="inputCOMP_NM" type="text" class="write-input width100" placeholder="회사명을 입력해주세요" autocomplete="off" tabindex="5">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 이메일</th>
                  <td>
                  	<input id="inputEMAIL" type="text" class="write-input width100" placeholder="이메일을 입력해주세요" autocomplete="off" tabindex="7">
                  </td>
                </tr>
              </tbody>
            </table>
          </article>
        </div>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%" >
          <button type="submit" class="btn-style01" onClick="javascript:fnUserSave();">저장</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupUserClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--사용자 정보 팝업 End -->

<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>