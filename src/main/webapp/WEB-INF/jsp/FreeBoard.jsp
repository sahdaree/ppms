<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">

<link rel="stylesheet" href="css/layout.css">

<link rel="stylesheet" href="css/layout_responsive.css">

	
<link rel="stylesheet" href="css/board.css">
<!-- 게시판 제작시 사용 -->
<link rel="stylesheet" href="css/board_responsive.css">
<!-- 게시판(반응형, 모바일) 제작시 사용 --> 

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
	
 <!-- 용도모르는 스크립트 확인요망!!! -->	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script>



<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script> 

<link rel="stylesheet" type="text/css" href="css/slick.css">

<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#inputTEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
});

	///////////////////// 공 통 ///////////////////// 
	//사용자 정보 전역변수
	var loginfo = ${loginfoJSON};
	
	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}

	//사용자 정보 수정
	function fnUserInfo(){
		$('#inputUSER_ID').val(loginfo.USER_ID);
		$('#inputUSER_NM').val(loginfo.USER_NM);
		$("#inputUSER_PWD").val('');
		$("#inputUSER_PWD_Check").val('');
		$("#comboUSER_POS").val(loginfo.USER_POS);
		$("#inputCOMP_NM").val(loginfo.COMP_NM);
		$("#inputTEL").val(loginfo.TEL);
		$("#inputEMAIL").val(loginfo.EMAIL);
		$("input[name='radioSMS_YN']:radio[value='" + loginfo.SMS_RCV_YN + "']").prop('checked',true);
		
		//팝업 띄우기
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show();
        
        wrapWindowByMask();
	}
	
	//사용자 저장 이벤트
	function fnUserSave(){
		if ($.trim($("#inputUSER_NM").val()) =="") {
			alert("이름은 필수입력 항목입니다.");
			$("#inputUSER_NM").focus();
			return;
		}
		if($.trim($("#inputUSER_NM").val()).length > 20){
			alert("이름은 20자 이하로 설정해주세요.");
			$("#inputUSER_NM").focus();
			return;
		}
		
		if($("#inputUSER_PWD").val() != "" || $("#inputUSER_PWD_Check").val() != ""){
			if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
				alert("비밀번호가 일치하지 않습니다.");
				$("#inputUSER_PWD").focus();
				return;
			}
		}
		
		if ($.trim($("#inputCOMP_NM").val()) =="") {
			alert("회사명은 필수입력 항목입니다.");
			$("#inputCOMP_NM").focus();
			return;
		}
		if($.trim($("#inputCOMP_NM").val()).length > 20){
			alert("회사명은 20자 이하로 설정해주세요.");
			$("#inputCOMP_NM").focus();
			return;
		}
		
		if ($.trim($("#inputTEL").val()) =="") {
			alert("연락처는 필수입력 항목입니다.");
			$("#inputTEL").focus();
			return;
		}
		if($.trim($("#inputTEL").val()).length > 13){
			alert("연락처는 13자 이하로 설정해주세요.");
			$("#inputTEL").focus();
			return;
		}
		
		if ($.trim($("#inputEMAIL").val()) =="") {
			alert("이메일은 필수입력 항목입니다.");
			$("#inputEMAIL").focus();
			return;
		}
		if(!($.trim($("#inputEMAIL").val()).match("@"))){
			alert("이메일 형식으로 입력해주세요.");
			$("#inputEMAIL").focus();
			return;
		}
		if($.trim($("#inputEMAIL").val()).length > 30){
			alert("이메일은 30자 이하로 설정해주세요.");
			$("#inputEMAIL").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userMerge.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("MNU_KIND","board");
	        comSubmit.addParam("USER_ID",$.trim($("#inputUSER_ID").val()));
	        comSubmit.addParam("USER_NM",$.trim($("#inputUSER_NM").val()));
	        comSubmit.addParam("USER_PWD",$("#inputUSER_PWD").val());
	        comSubmit.addParam("USER_POS",$("select[name=comboUSER_POS]").val());
	        comSubmit.addParam("COMP_NM",$.trim($("#inputCOMP_NM").val()));
	        comSubmit.addParam("TEL",$.trim($("#inputTEL").val()));
	        comSubmit.addParam("EMAIL",$.trim($("#inputEMAIL").val()));
	        comSubmit.addParam("AUTH_SETUP",loginfo.AUTH_SETUP);
	        comSubmit.addParam("SMS_RCV_YN",$('input[name="radioSMS_YN"]:checked').val());
	        comSubmit.addParam("USE_YN",loginfo.USE_YN);
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        comSubmit.addParam("searchedItem2Combo","${searchedCondition.searchedItem2Combo}");
	        comSubmit.submit();
		}
		
	}
	///////////////////////////////////////////////
	
	//조회&페이지 이벤트
	function fnSearch(pageNo){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedPageNo",pageNo);
	    comSubmit.addParam("searchedBasicCombo",$("select[name=comboGBN]").val());
	    comSubmit.addParam("searchedWord",$("#searchbox").val());
	    //comSubmit.addParam("searchedBizSeq",$("select[name=comboSite]").val());
	    //comSubmit.addParam("searchedItem1Combo",$("select[name=comboGBN]")ㅎ.val());
	    comSubmit.submit();
	}

	//게시판 글보기
	function fnView(selected_BBS_SEQ){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/boardView.do'/>");
	    comSubmit.addParam("MNU_CD","${MNU_CD}");
	    comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedBBSSeq",selected_BBS_SEQ);
	    comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	    comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	    comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	    comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	    comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	    comSubmit.submit();
	}
	
	//등록 이벤트
	function fnRegist(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/boardEdit.do'/>");    
	    comSubmit.addParam("MNU_CD","${MNU_CD}");
	    comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedBBSSeq","-1"); //신규 = -1
	    comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	    comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	    comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	    comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	    comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	    comSubmit.submit();
	}
</script> 	
<!-- 레이어팝업 스크립트 -->
<link rel="stylesheet" href="css/layerpop.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
</head>


<body>

<!-- code -->
<div id="wrap"> 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"><a href="javascript:fnMain()"><img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix"> 
          <div class="header-lang trans300">
            <span>
                <span><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.</span> 
                <a href="javascript:fnLogout()" style="margin-left:5px;">logout</a>
            </span>
          </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
         <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
          <!-- GNB Mobile --> 
          <a href="javascript;" class="nav-open-btn" title="네비게이션 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3" ></span> </a> </div>
      </div>
        <nav id="gnb" class="each-menu">
	      <ul class="clearfix">
	         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
	      </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
             <div class="header-lang-m "><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.<span ><a href="javascript:fnLogout()">logout</a></span></div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP == 'U'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
        </ul>
      </nav>
    </div>
  </header>
  
  
  <!-- //header -->
<!-- container -->
<div id="container">
<!-- visual -->

<section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
  <div class="area visual-txt-con">
    <h2 class="visual-tit trans400"> 소식 </h2>
    <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
  </div>
</section>
<!-- //visual --> 
<!-- middleArea -->
<div id="middleArea">
<!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->

<!-- <aside id="sideMenu">
  <div class="side-menu-inner area">
    <ul id="subGnb" class="snb clearfix">
      <div class="nav-on-icon main-move-line">움직이는 바 
        <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
      <li > <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
      <li class="on"> <a href="info_board.html"><span class="trans300"><em>자유게시판</em></span></a> </li>
      <li> <a href="info_faq.html"><span class="trans300"><em>FAQ</em></span></a> </li>
    </ul>
  </div>
</aside> -->
	<c:forEach items="${MNUList}" var="item_1" varStatus="status">
	  <c:if test="${item_1.MNU_CD == MNU_CD}">
	    <aside id="sideMenuM" class="cm-top-menu clearfix">
		  <div class="menu-location  location2">
			<span style="line-height:50px; color:#fff; font-weight:500; font-size:15px; margin-left:20px;">${item_1.MNU_NM}</span>
		  </div>
	    </aside>
	  </c:if>
    </c:forEach>
<!-- // --> 

<!-- 상단정보 (센터정렬) -->
<aside id="contentInfoCon" class="content-info-style01 trans400">
  <h3 class="content-tit rubik">자유게시판</h3>
</aside>

<!-- content -->
<section id="content" class="area">
	
<!----- 조회 시작 ----------->
<aticle >
	
		
	 <div class="search-group " >
    <fieldset id="bo_sch">
    <span>
      <select name="comboGBN" id="comboGBN" class="select_title">
        <option value="0" <c:if test="${searchedCondition.searchedBasicCombo == '0'}">selected="selected"</c:if>>제목</option>
        <option value="1" <c:if test="${searchedCondition.searchedBasicCombo == '1'}">selected="selected"</c:if>>내용</option>
      </select>
      
     <input id="searchbox" name="searchbox" type="text" class="input_title" placeholder="검색어를 입력해주세요" value="${searchedCondition.searchedWord}"
        onkeydown="javascript:if (event.keyCode == 13) { fnSearch(''); return false; }"><!-- autofocus -->
     <button id="button" style="margin-left: 5px">
        <img src="images/icon_input_search.png" alt="조회" onclick="fnSearch(''); return false;"/>
     </button>
    </span>
		
	</fieldset>	 
	<!--관리자만-->	 
    <a href="javascript:fnRegist()"> 등록 </a>
    </div>	


</aticle>
	
<!----- 조회  끝 ----------->

<!----- 게시판 리스트 시작 ----------->

<article class="bbs-list-con">
    <h2 class="sound_only">기본게시판 목록</h2>
    <div class="bbs-list-Tbl">
      <ul>
        <li class="bbs-list-TblTr bbs-list-TblTh">
          <div class="mvInlineN">번호</div>
		  <div class="mvInlineN ">구분</div>
          <div class="mvInlineN ">제목</div>
          <div class="mvInlineN ">작성자</div>
		  <div class="mvInlineN ">작성일</div>
          <div class="mvInlineN ">조회 </div>
        </li>

        <c:forEach items="${data}" var="result" varStatus="status">
            <li class="bbs-list-TblTr bbs-list-TblTd" style="cursor: pointer;" onclick="javascript:fnView('<c:out value="${result.BBS_SEQ}"/>')">
              <div class="mvInlineN td_num2"> ${result.NO} </div>
              <div class="td_group"> <span>${result.ITEM_1_NM}</span> </div>
              <div class="td_subject">
                <div class="bo_tit">
                    <a>${result.TTL}
                    <c:if test="${REPL_REG_YN == 'Y' && result.RPLY_CNT != 0 }">
	                  <span class="sound_only">댓글</span>
	                  <span class="cnt_cmt">+ ${result.RPLY_CNT}</span>
	                  <span class="sound_only">개</span>
                    </c:if>
                    </a>
                </div>
              </div>
              <div class="mvInlinev td_name sv_use"><span class="sv_member">${result.REG_NM}</span></div>
              <div class="mvInlinev td_datetime"><i class="fa fa-clock"></i> ${result.REG_DT}</div>
              <div class="mvInlinev td_num"><i class="fa fa-eye"></i> ${result.READ_CNT}</div>
            </li>
        </c:forEach>
      </ul>
    </div>
 
</article>
  <!--  게시판 리스트 종료 -->
  
  <div class="paging">
    <ul>
      <ui:pagination paginationInfo = "${paginationInfo}" type="customRenderer" jsFunction="fnSearch" />
    </ul>
  </div>
	

  
  <!-- // 일반 FAQ 시작 --> 
  <!-- //컨텐츠 내용 --> 
</section>
<!-- //content -->
</div>
<!-- //middleArea -->
</div>
<!-- //container --> 
<!-- footer -->

<footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
  <div id="footerInner" class="clearfix">
    <article id="footerTop" class="">
      <div class="area-box clearfix">
        <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
      </div>
    </article>
  </div>
</footer>

<!-- 모달 레이어팝업 -->
<article class="modal-fixed-pop-wrapper">
  <div class="modal-fixed-pop-inner">
    <div class="modal-loading"><span class="loading"></span></div>
    <div class="modal-inner-box">
      <div class="modal-inner-content"> 
        <!-- ajax 내용 --> 
      </div>
    </div>
  </div>
</article>
<!-- //footer -->
</div>
<!-- //code -->
	<!--sitemap Popup Start -->

	
    <article class="layerpop-cont">
    <div id="mask"></div>         
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
        	<a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
        
        <article class="sitemap-wrapper">
        <ul>
        
         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
           <c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP=='U'}">
             <li>
         	   <h2>${item_1.MNU_NM}</h2>
         	     <ul class="sitemap-2dep">
         		   <c:forEach items="${MNUList}" var="item_2" varStatus="status">
         		     <c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
         		 	   <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a> </li>
         			 </c:if>
         		   </c:forEach>
		        </ul>
		     </li>
	       </c:if>
         </c:forEach>
        
        </ul>
    	</article>
        </div>
        </article>
    </div>
    </article>

    <!--sitemap Popup End -->
    
<!-- 사용자 정보 팝업 -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 700px; height: 570px;">
    <article class="layerpop_area">
      <div class="title">사용자 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <div class="layerpop-write-con" style="padding-right: 20px; float: left; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table class="layerpop-write-tbl" >
              <caption>
              사용자 관리 작석폼
              </caption>
              <colgroup>
              <col >
              <col>
              </colgroup>
              <tbody  >
                <tr>
                  <th scope="row">* 아이디</th>
                  <td>
                  	<input id="inputUSER_ID" type="text" class="write-input width100" autocomplete="off" readonly style="color:#808080;">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호</th>
                  <td>
                  	<input id="inputUSER_PWD" type="password" class="write-input width100" placeholder="비밀번호를 입력해주세요" autocomplete="off" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <th scope="row">직급</th>
                  <td>
                  	<select id="comboUSER_POS" name="comboUSER_POS" class="write-select width100" >
                  	<option value="">선택</option>
		               <c:forEach items="${listUserPOS}" var="UserPOS" varStatus="status">
		               	<option value="${UserPOS.DTL_CD}">${UserPOS.DTL_NM}</option>
		               </c:forEach>
                    </select></td>
                </tr>
                <tr>
                  <th scope="row">* 연락처</th>
                  <td>
                  	<input id="inputTEL" type="text" class="write-input width100" placeholder="연락처를 입력해주세요" autocomplete="off" tabindex="6">
                  </td>
                </tr>
                <tr>
                  <th scope="row">SMS 수신동의 <span class="info_text"> <i class="fas fa-info-circle"></i> SMS 수신 동의시 문자 발송이 됩니다.</span></th>
                  <td>
                  <fieldset class="custom-radio">
                  	<span class="radio-item" >
                      <input id="inputSMS_Y" name="radioSMS_YN" type="radio" value="Y" tabindex="12" checked>
                      <label for="inputSMS_Y" >동의</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputSMS_N" name="radioSMS_YN" type="radio" value="N" tabindex="13">
                      <label for="inputSMS_N" >미동의</label>
                    </span>
                    </fieldset>
                  </td>
                </tr> 
              </tbody>
            </table>
          </article>
        </div>
        <div class="layerpop-write-con" style="padding-left: 20px; float: right; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
              <tbody>
                <tr>
                  <th scope="row">* 이름</th>
                  <td>
                  	<input id="inputUSER_NM" type="text" class="write-input width100" placeholder="이름을 입력해주세요" autocomplete="off" tabindex="2">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호 확인</th>
                  <td>
                  	<input id="inputUSER_PWD_Check" type="password" class="write-input width100" placeholder="비밀번호를 확인해주세요" autocomplete="off" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 회사명</th>
                  <td>
                  	<input id="inputCOMP_NM" type="text" class="write-input width100" placeholder="회사명을 입력해주세요" autocomplete="off" tabindex="5">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 이메일</th>
                  <td>
                  	<input id="inputEMAIL" type="text" class="write-input width100" placeholder="이메일을 입력해주세요" autocomplete="off" tabindex="7">
                  </td>
                </tr>
              </tbody>
            </table>
          </article>
        </div>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%" >
          <button type="submit" class="btn-style01" onClick="javascript:fnUserSave();">저장</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--사용자 정보 팝업 End -->

<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>