<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout_admin.css">
<link rel="stylesheet" href="css/board.css">
<link rel="stylesheet" href="css/layerpop.css">

<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">

<!-- 용도모르는 스크립트 확인요망!!! --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script> 
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 

<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script> 
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script>

<!-- 레이어팝업 스크립트 --> 
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#inputTEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
});

	///////////////////// 공 통 /////////////////////
	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}
	///////////////////////////////////////////////
	
    //콤보박스 변경 이벤트
	function fnSelectChanged(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedItem1Combo",$("select[name=comboGBN_1]").val());
	    comSubmit.submit();
	}
	
	//사용자 선택 이벤트
	function fnUserSite(USER_ID){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	    comSubmit.addParam("searchedItem1Combo",$("select[name=comboGBN_1]").val());
	    comSubmit.addParam("searchedWord",USER_ID);
	    comSubmit.submit();
	}
	
	//매핑 저장 이벤트
	function fnMappingSave(){
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userSiteSave.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        
			var arry = $('#ulSite').find('input');
			for(key in arry){
				if(arry[key].checked == true){
			        comSubmit.addParam("BIZ_SEQ",arry[key].value);
				}
			}
			comSubmit.submit();
		}	
	}
	
	//아이디 중복체크 
	function fnDupliCheck(){
		if ($.trim($("#inputUSER_ID").val()) =="") {
			alert("아이디를 입력하세요");
			return;
		}
		if ($.trim($("#inputUSER_ID").val()).length < 5) {
			alert("아이디는 5글자 이상으로 입력해 주세요.");
			return;
		}

		$.ajax({
			url : "/SearchUserInfo.do",
			type : "post",
			data :{ "USER_ID" : $("#inputUSER_ID").val(),
					"GBN" : "dupliCheck"}
		})
		.done(function(jsonData){
			if(jsonData.data.length == 1){
				alert("사용할 수 없는 아이디입니다.\n다른 아이디를 입력해주세요.");
				$("#inputUSER_ID").focus();
			}else{
				if(confirm("사용 가능한 아이디입니다. 사용하시겠습니까?")){
					$('#inputUSER_ID').attr('readonly', true);
					$('#inputUSER_ID').css('color',"#808080");
				}
				else{
					$('#inputUSER_ID').attr('readonly', false);
					$('#inputUSER_ID').css('color',"#ffffff");
				}
			}
		});
	}
	
	//사용자 저장 이벤트
	function fnUserSave(){
		if ($.trim($("#inputUSER_ID").val()) =="") {
			alert("아이디는 필수입력 항목입니다.");
			$("#inputUSER_ID").focus();
			return;
		}
		
		if ($('#inputUSER_ID').attr('readonly') !="readonly"){
			alert("아이디 중복확인을 해주세요.");
			return;
		}
		
		if ($.trim($("#inputUSER_NM").val()) =="") {
			alert("이름은 필수입력 항목입니다.");
			$("#inputUSER_NM").focus();
			return;
		}
		
		//중복확인 버튼 표시 유무로 등록중인지 수정중인지 판단함
		//중복확인 버튼 표시 = 등록
		//중복확인 버튼 미표시 = 수정
		if($('#btnDupli').is(':visible')){
			if ($.trim($("#inputUSER_PWD").val()) =="") {
				alert("비밀번호는 필수입력 항목입니다.");
				$("#inputUSER_PWD").focus();
				return;
			}
			
			if ($.trim($("#inputUSER_PWD_Check").val()) =="") {
				alert("비밀번호 확인은 필수입력 항목입니다.");
				$("#inputUSER_PWD_Check").focus();
				return;
			}
			if($.trim($("#inputUSER_PWD").val()) != "" && $.trim($("#inputUSER_PWD_Check").val()) != ""){
				if($.trim($("#inputUSER_PWD").val()) != $.trim($("#inputUSER_PWD_Check").val())){
					alert("비밀번호가 일치하지 않습니다.");
					$("#inputUSER_PWD").focus();
					return;
				}
			}
		}else{
			if($("#inputUSER_PWD").val() != "" || $("#inputUSER_PWD_Check").val() != ""){
				if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
					alert("비밀번호가 일치하지 않습니다.");
					$("#inputUSER_PWD").focus();
					return;
				}
			}
		}
		
		if ($.trim($("#inputCOMP_NM").val()) =="") {
			alert("회사명은 필수입력 항목입니다.");
			$("#inputCOMP_NM").focus();
			return;
		}
		
		if ($.trim($("#inputTEL").val()) =="") {
			alert("연락처는 필수입력 항목입니다.");
			$("#inputTEL").focus();
			return;
		}
		
		if ($.trim($("#inputEMAIL").val()) =="") {
			alert("이메일은 필수입력 항목입니다.");
			$("#inputEMAIL").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userMerge.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("MNU_KIND","board");
	        comSubmit.addParam("USER_ID",$.trim($("#inputUSER_ID").val()));
	        comSubmit.addParam("USER_NM",$.trim($("#inputUSER_NM").val()));
	        comSubmit.addParam("USER_PWD",$("#inputUSER_PWD").val());
	        comSubmit.addParam("USER_POS",$("select[name=comboUSER_POS]").val());
	        comSubmit.addParam("COMP_NM",$.trim($("#inputCOMP_NM").val()));
	        comSubmit.addParam("TEL",$.trim($("#inputTEL").val()));
	        comSubmit.addParam("EMAIL",$.trim($("#inputEMAIL").val()));
	        comSubmit.addParam("AUTH_SETUP",$("select[name=comboAUTH_SETUP]").val());
	        comSubmit.addParam("SMS_RCV_YN",$('input[name="radioSMS_YN"]:checked').val());
	        comSubmit.addParam("USE_YN",$('input[name="radioUSE_YN"]:checked').val());
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        //comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        //comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        comSubmit.submit();
		}
		
	}
	
	//등록 버튼 클릭 이벤트
	function fnRegist(){
		$('#inputUSER_ID').attr('readonly', false);
		$('#inputUSER_ID').css('color',"#ffffff");
		$('#btnDupli').show();
		$('#btnDel').hide();
		
		$("#inputUSER_ID").val('');
		$("#inputUSER_NM").val('');
		$("#inputUSER_PWD").val('');
		$("#inputUSER_PWD_Check").val('');
		$("#comboUSER_POS option:eq(0)").prop("selected", true);
		$("#comboAUTH_SETUP option:eq(0)").prop("selected", true);
		$("#inputCOMP_NM").val('');
		$("#inputTEL").val('');
		$("#inputEMAIL").val('');
		$("input[id='inputUSE_Y']").prop("checked",true);
		$("input[id='inputSMS_Y']").prop("checked",true);
		
		fnPopup();
	}
	
	//수정 버튼 이벤트
	function fnModify(){
		var flag = false;
		
		$('#inputUSER_ID').attr('readonly', true);
		$('#inputUSER_ID').css('color',"#808080");
		$('#btnDupli').hide();
		$('#btnDel').show();
		
		var aryLi = $("#ulBoard li");
		for(key in aryLi){
			if(aryLi[key].className != undefined){
				if(aryLi[key].className.search("selected") != -1){
					var userid = aryLi[key].dataset.value;
					var jsonAry = ${jsonArray};
					
					for(key in jsonAry){
						if(jsonAry[key].USER_ID == userid){
							$("#inputUSER_ID").val(jsonAry[key].USER_ID);
							$("#inputUSER_NM").val(jsonAry[key].USER_NM);
							$("#comboUSER_POS").val(jsonAry[key].USER_POS);
							$("#inputCOMP_NM").val(jsonAry[key].COMP_NM);
							$("#inputTEL").val(jsonAry[key].TEL);
							$("#inputEMAIL").val(jsonAry[key].EMAIL);
							$("input[name='radioUSE_YN']:radio[value='" + jsonAry[key].USE_YN + "']").prop('checked',true);
							$("#comboAUTH_SETUP").val(jsonAry[key].AUTH_SETUP);
							$("input[name='radioSMS_YN']:radio[value='" + jsonAry[key].SMS_RCV_YN + "']").prop('checked',true);
							
							fnPopup();
							
							flag = true;
						}
					}					
				}
			} 
		}
			
		if(flag == false){
			alert("사용자를 선택해 주세요.");
		}
	}

	function fnUserDelete(){
		if(confirm('삭제하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userDelete.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("USER_ID",$("#inputUSER_ID").val());
	        comSubmit.submit();
		}
	}
	
	//팝업창 띄우기
	function fnPopup(){
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show();
        
        wrapWindowByMask();
	}
	
</script>
</head>

<body >
<div id="wrap"  > 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
        <h1 class="logo trans300"><a href="javascript:fnMain()"><img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix">
          <div class="header-lang trans300">
            <span>
                <span>${loginfo.USER_NM}님 환영합니다.&nbsp;&nbsp;</span>
                <a href="javascript:fnLogout()">logout</a>
            </span>
          </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
            <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
      </div>
      <nav id="gnb" class="each-menu">
	      <ul class="clearfix">
	         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep" style="display: none;">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
	      </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
             <div class="header-lang-m " >${loginfo.USER_NM}님 환영합니다.<span > <a  href="#">logout</a> </span>  </div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	         </c:forEach>
        </ul>
      </nav>
    </div>
	  </div>
  </header>
  <!-- //header --> 
  <!-- container -->
  <div id="container" > 
    <!-- visual -->
    
    <section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
      <div class="area visual-txt-con">
        <h2 class="visual-tit trans400"> 사용자관리 </h2>
        <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
      </div>
    </section>
    <!-- //visual --> 
    <!-- middleArea -->
    <div id="middleArea"> 
      <!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
      
<!--       <aside id="sideMenu">
        <div class="side-menu-inner area">
          <ul id="subGnb" class="snb clearfix">
            <div class="nav-on-icon main-move-line">움직이는 바 
              <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
            <li class="on"> <a href="#"><span class="trans300"><em>전체</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사이트PM</em></span></a> </li>
            <li> <a href="#"><span class="trans300"><em>사용자</em></span></a> </li>
          </ul>
        </div>
      </aside> -->
      <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	   	<c:if test="${item_1.MNU_CD == MNU_CD}">
		  <c:forEach items="${MNUList}" var="item_2" varStatus="status">
			  <c:if test="${item_1.UPPER_CD == item_2.MNU_CD}">
			  	<aside id="sideMenuM" class="cm-top-menu clearfix">
			  	<div class="menu-location  location2"><a href="javascript:;" class="cur-location"><span>${item_2.MNU_NM}</span><i class="material-icons arrow"></i></a>
			      <ul class="location-menu-con">
			  		<c:forEach items="${MNUList}" var="item_3" varStatus="status">
			  	      <c:if test="${item_2.MNU_CD == item_3.UPPER_CD}">
			  			  <c:if test="${item_3.MNU_CD == MNU_CD}">
			  			    <li class="on"><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  			  <c:if test="${item_3.MNU_CD != MNU_CD}">
			  				<li><a href="javascript:fnMenuAction('${item_3.MNU_CD_PATH}')"><span class="trans300"><em>${item_3.MNU_NM}</em></span></a></li>
			  			  </c:if>
			  		  </c:if>
			  		</c:forEach>
			  	  </ul>
			  	</div>
			  	</aside>
			  </c:if>
		  </c:forEach>
	   </c:if>
    </c:forEach>
      <!-- // --> 
      
      <!-- 상단정보 (센터정렬) -->
      <aside id="contentInfoCon" class="content-info-style01 trans400">
        <h3 class="content-tit rubik">사용자 관리</h3>
      </aside>
      
      <!-- content -->
      <section id="content" class="area"> 
        
        <!----- 화면분할 시작 ----------->
        
        <div class="admin-cont" > 
          <!--  왼쪽 contens -->
          <div class="admin-left-con w70"> 
            <!----- 게시판 리스트 시작 -----------> 
            <!--<div class="admin-title"><span><i class="fas fa-angle-right"></i> 사용자</span>  </div>--> 
            <!----- 조회 시작 ----------->
            <aticle >
              <div class="search-group "  >
                <fieldset id="bo_sch" style="width: 50%;">
                  <span>
                  <label >구분</label>
                  <select id="comboGBN_1" name="comboGBN_1" style="width: 25%" onchange="fnSelectChanged(this.value)">
                    <option value="*" <c:if test="${'*' eq searchedCondition.searchedItem1Combo}">selected="selected"</c:if>>전체</option>
                    <option value="P" <c:if test="${'P' eq searchedCondition.searchedItem1Combo}">selected="selected"</c:if>>사이트PM</option>
                    <option value="U" <c:if test="${'U' eq searchedCondition.searchedItem1Combo}">selected="selected"</c:if>>사용자</option>
                  </select>
                  
                  <!-- 버튼필요시 주석을 풀고 사용하세요
			<button id="button" style="margin-left: 5px" ><img src="images/icon_input_search.png"  alt=""/></button> 
			--> 
                  </span>
                </fieldset>
                <!--관리자만--> 
                <a href="javascript:fnRegist();"> 등록 </a>
                <a href="javascript:fnModify();"> 수정 </a>
             </div>
             
            </aticle>
            <!----- 조회  끝 ----------->
            
            <article class="bbs-list-con" >
              <h2 class="sound_only">관리자 게시판 목록</h2>
              <div class="bbs-list-Tbl" style="min-width: 100px">
                <ul id="ulBoard">
                  <li class="bbs-list-TblTr bbs-list-TblTh">
                    <div class="mvInlineN">번호</div>
                    <div class="mvInlineN ">구분</div>
                    <div class="mvInlineN ">이름</div>
                    <div class="mvInlineN ">직급</div>
                    <div class="mvInlineN ">아이디</div>
                    <div class="mvInlineN ">회사명</div>
                    <div class="mvInlineN ">연락처</div>
                    <div class="mvInlineN ">이메일</div>
                  </li>

                <c:forEach items="${data}" var="result" varStatus="status">
	              <li <c:if test="${result.USER_ID eq searchedCondition.searchedWord}">class="bbs-list-TblTr bbs-list-TblTd selected" </c:if> 
	                  <c:if test="${result.USER_ID != searchedCondition.searchedWord}">class="bbs-list-TblTr bbs-list-TblTd" </c:if>
	              style="cursor: pointer;" data-value="${result.USER_ID}" onclick="javascript:fnUserSite('<c:out value="${result.USER_ID}"/>')">
		            <div class="td_num2"> ${result.NO} </div>
		            <div class="td_group"> ${result.AUTM_NM} </div>
		            <div class="td_name"> ${result.USER_NM} </div>
		            <div class="td_name"> ${result.USER_POS_NM} </div>
		            <div class="td_id"> ${result.USER_ID} </div>
		            <div class="td_company"> ${result.COMP_NM} </div>
		            <div class="td_tel"> ${result.TEL} </div>
		            <div class="td_email" > ${result.EMAIL} </div>
	             </li>
	           </c:forEach>
	         
                </ul>
              </div>
            </article>
            <!--  게시판 리스트 종료 --> 
            
          </div>
          <!--  왼쪽 contens  끝--> 
          <!--  오른쪽 contens -->
          <div class="admin-right-con w30"> 
            <!----- 게시판 리스트 시작 ----------->
            <div class="admin-title" style="margin-top: 20px" >
            	<a href="javascript:fnMappingSave();"> 매핑저장 </a>
            </div>
            <article class="bbs-list-con" >
              <h2 class="sound_only">관리자 게시판 목록</h2>
              <div class="bbs-list-Tbl" style="min-width: 100px">
                <ul id="ulSite">
                  <li class="bbs-list-TblTr bbs-list-TblTh">
                    <div class="mvInlineN">선택</div>
                    <div class="mvInlineN ">사이트</div>
                    <div class="mvInlineN ">발주처</div>
                  </li>
                  
			   <c:forEach items="${siteList}" var="site" varStatus="status">
	              <li class="bbs-list-TblTr bbs-list-TblTd">
                    <div class="td_chk">
                      <input type="checkbox" value="${site.BIZ_SEQ}" <c:if test="${site.CHK eq 'true'}">checked</c:if>>
                    </div>
                    <div class="td_code_name"> ${site.BIZ_NM} </div>
                    <div class="td_client"> ${site.ORDG_COMP} </div>
	             </li>
	           </c:forEach>
                </ul>
              </div>
            </article>
            <!--  게시판 리스트 종료 --> 
            
          </div>
          <!--  오른쪽 contens  끝--> 
        </div>
        <!-----  화면분할  끝 -----------> 
        
        <!-- //컨텐츠 내용 --> 
      </section>
      <!-- //content --> 
    </div>
    <!-- //middleArea --> 
  </div>
  <!-- //container --> 
  <!-- footer -->
  
  <footer id="footer" class="sub"> <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
    <div id="footerInner" class="clearfix">
      <article id="footerTop" class="">
        <div class="area-box clearfix">
          <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
        </div>
      </article>
    </div>
  </footer>
  
  <!-- 모달 레이어팝업 -->
  <article class="modal-fixed-pop-wrapper">
    <div class="modal-fixed-pop-inner">
      <div class="modal-loading"><span class="loading"></span></div>
      <div class="modal-inner-box">
        <div class="modal-inner-content"> 
          <!-- ajax 내용 --> 
        </div>
      </div>
    </div>
  </article>
  
  <!-- //footer --> 
</div>
<!-- //code --> 
	
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>		  
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
        <!-- 문의 폼 시작 -->
        
         
          <article class="sitemap-wrapper">
		
		<ul >
         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
           <c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP=='U'}">
             <li>
         	   <h2>${item_1.MNU_NM}</h2>
         	     <ul class="sitemap-2dep">
         		   <c:forEach items="${MNUList}" var="item_2" varStatus="status">
         		     <c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
         		 	   <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a> </li>
         			 </c:if>
         		   </c:forEach>
		        </ul>
		     </li>
	       </c:if>
         </c:forEach>
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>

    <!--sitemap Popup End -->

<!--code  Popup Start -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 700px; height: 660px;">
    <article class="layerpop_area">
      <div class="title">사용자 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <div class="layerpop-write-con" style="padding-right: 20px; float: left; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table class="layerpop-write-tbl" >
              <caption>
              사용자 관리 작석폼
              </caption>
              <colgroup>
              <col >
              <col>
              </colgroup>
              <tbody  >
                <tr>
                  <th scope="row">* 아이디</th>
                  <td>
                  	<input id="inputUSER_ID" type="text" class="write-input width70" placeholder="아이디 입력해주세요" autocomplete="off" tabindex="1"
                  	 onkeydown="javascript:if (event.keyCode == 13) { fnDupliCheck(); }"><button id="btnDupli" type="submit" class="td_button " onClick="javascript:fnDupliCheck()">중복확인</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호</th>
                  <td>
                  	<input id="inputUSER_PWD" type="password" class="write-input width100" placeholder="비밀번호를 입력해주세요" autocomplete="off" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <th scope="row">직급</th>
                  <td>
                  	<select id="comboUSER_POS" name="comboUSER_POS" class="write-select width100" >
                  	<option value="">선택</option>
		               <c:forEach items="${ListItem_1}" var="result" varStatus="status">
		               	<option value="${result.DTL_CD}">${result.DTL_NM}</option>
		               </c:forEach>
                    </select></td>
                </tr>
                <tr>
                  <th scope="row">* 연락처</th>
                  <td>
                  	<input id="inputTEL" type="text" class="write-input width100" placeholder="연락처를 입력해주세요" autocomplete="off" tabindex="6">
                  </td>
                </tr>  <tr>
                  <th scope="row"> 접근권한</th>
                  <td>
                  	<select id="comboAUTH_SETUP" name="comboAUTH_SETUP" class="write-select width100" >
                      <option value="U">사용자</option>
                      <option value="P">사이트PM</option>
                    </select>
                  </td>
                </tr>
               
              </tbody>
            </table>
          </article>
        </div>
        <div class="layerpop-write-con" style="padding-left: 20px; float: right; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
              <tbody>
                <tr>
                  <th scope="row">* 이름</th>
                  <td>
                  	<input id="inputUSER_NM" type="text" class="write-input width100" placeholder="이름을 입력해주세요" autocomplete="off" tabindex="2">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호 확인</th>
                  <td>
                  	<input id="inputUSER_PWD_Check" type="password" class="write-input width100" placeholder="비밀번호를 확인해주세요" autocomplete="off" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 회사명</th>
                  <td>
                  	<input id="inputCOMP_NM" type="text" class="write-input width100" placeholder="회사명을 입력해주세요" autocomplete="off" tabindex="5">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 이메일</th>
                  <td>
                  	<input id="inputEMAIL" type="text" class="write-input width100" placeholder="이메일을 입력해주세요" autocomplete="off" tabindex="7">
                  </td>
                </tr>
				  
				  <tr>
                  <th scope="row"> 사용여부</th>
                  <td>
                  	<fieldset class="custom-radio"> 
                  	<span class="radio-item" >
                      <input id="inputUSE_Y" name="radioUSE_YN" type="radio" value="Y" tabindex="10" checked>
                      <label for="inputUSE_Y" >사용</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputUSE_N" name="radioUSE_YN" type="radio" value="N" tabindex="11">
                      <label for="inputUSE_N" >미사용</label>
                    </span>
                    </fieldset>
                  </td>
                </tr>
                <tr>
                  <th scope="row">SMS 수신동의 <span class="info_text"> <i class="fas fa-info-circle"></i> SMS 수신 동의시 문자 발송이 됩니다.</span></th>
                  <td>
                  <fieldset class="custom-radio">
                  	<span class="radio-item" >
                      <input id="inputSMS_Y" name="radioSMS_YN" type="radio" value="Y" tabindex="12" checked>
                      <label for="inputSMS_Y" >동의</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputSMS_N" name="radioSMS_YN" type="radio" value="N" tabindex="13">
                      <label for="inputSMS_N" >미동의</label>
                    </span>
                    </fieldset>
                  </td>
                </tr> 
              
              </tbody>
            </table>
          </article>
        </div>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%" >
          <button type="submit" class="btn-style01" onClick="javascript:fnUserSave();">저장</button>
          <button id="btnDel" type="submit" class="btn-style01" onClick="javascript:fnUserDelete();">삭제</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--Popup End -->

<!-- 액션을 위한 폼, js함수 ComSubmit()에서 사용 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>