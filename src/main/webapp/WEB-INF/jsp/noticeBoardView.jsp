<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>사후관리시스템</title>
<meta name="Subject" content="news">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=" shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.ico">
<!-- 모바일사이트, 반응형사이트 제작시 사용 -->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/layout.css">
<link rel="stylesheet" href="css/layout_responsive.css">
<link rel="stylesheet" href="css/board.css">
<!-- 게시판 제작시 사용 -->
<link rel="stylesheet" href="css/board_responsive.css">
<!-- 게시판(반응형, 모바일) 제작시 사용 --> 
<!-- 아이콘폰트 -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
 <!-- 용도모르는 스크립트 확인요망!!! -->	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-1.8.3.min.js"><\/script>')</script>
<script src="js/common.js"></script> 
<script src="js/layer_popup.js"></script> 
<script src="js/jquery.validate.js"></script> 
<link rel="stylesheet" type="text/css" href="css/slick.css">
<script src="js/slick.js"></script> 
<!-- 스크롤 -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript" src="js/nav.js"></script>
<script src="<c:url value='/js/common2.js'/>" charset="utf-8"></script> 
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<script type="text/javascript">
//삭제된 기존 첨부파일 SEQ
var removeSEQ = new Array();

$(document).ready(function() {
	var resultMsg = "${resultMsg}";
	if(resultMsg != ""){
		alert(resultMsg);
	}
	$("#inputTEL").keyup( function(){ $(this).val( $(this).val().replace(/[^0-9\-]/gi,"") ); } );
	
	//기존에 저장된 첨부파일 목록
	var jsonSavedFiles = ${jsonSavedFiles};
	
	var ul = $('#ulFilesEdit');
	var addCode = "";
	
	for(i in jsonSavedFiles){
		addCode ="";
		addCode += "<li id='liSavedFile_"+ jsonSavedFiles[i].FLE_SEQ +"' style='padding: 5px 5px;'><span style='color:#c89b43'><i class='far fa-file'></i>";
		addCode += jsonSavedFiles[i].FLE_INFO;
		addCode += "</span><a href='javascript:fnSavedFileRemove("+ jsonSavedFiles[i].FLE_SEQ + ")'><i class='fas fa-times' style='padding-left:5px; color:#808080;'></i></a></li>";
		ul.append(addCode);
	}
	
	var modifyReplySeq = "${searchedCondition.modifyReplySeq}";
	if(modifyReplySeq == ""){
		$("#tabReply").css('display','table');
		fnBtnAndMsgShowCheck('newReply');
	}else{
		$("#tabReply").css('display','none');
		fnBtnAndMsgShowCheck('editReply');
	}
	
});

	///////////////////// 공 통 /////////////////////
	//사용자 정보 전역변수
	var loginfo = ${loginfoJSON};

	//메인로고 클릭
	function fnMain(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.submit();
	}
	
	//메뉴navi 클릭
	function fnMenuAction(MNU_CD_PATH){
		var split = MNU_CD_PATH.split('_');
		var MNU_CD = split[0];
		var MNU_PATH = split[1];
		
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD", MNU_CD);
		comSubmit.addParam("MNU_PATH", MNU_PATH);
		comSubmit.submit();
	}
	
	//로그아웃
	function fnLogout(){
	    var comSubmit = new ComSubmit();
	    comSubmit.setUrl("<c:url value='/Logout.do'/>");
	    comSubmit.submit();
	}

	//사용자 정보 수정
	function fnUserInfo(){
		$('#inputUSER_ID').val(loginfo.USER_ID);
		$('#inputUSER_NM').val(loginfo.USER_NM);
		$("#inputUSER_PWD").val('');
		$("#inputUSER_PWD_Check").val('');
		$("#comboUSER_POS").val(loginfo.USER_POS);
		$("#inputCOMP_NM").val(loginfo.COMP_NM);
		$("#inputTEL").val(loginfo.TEL);
		$("#inputEMAIL").val(loginfo.EMAIL);
		$("input[name='radioSMS_YN']:radio[value='" + loginfo.SMS_RCV_YN + "']").prop('checked',true);
		
		//팝업 띄우기
        $('.layerpop').css("position", "absolute");
        //영역 가운에데 레이어를 뛰우기 위해 위치 계산 
        $('.layerpop').css("top",(($(window).height() - $('.layerpop').outerHeight()) / 2) + $(window).scrollTop());
        $('.layerpop').css("left",(($(window).width() - $('.layerpop').outerWidth()) / 2) + $(window).scrollLeft());
        $('.layerpop').draggable();
        $('#layerbox').show();
        
        wrapWindowByMask();
	}
	
	//사용자 저장 이벤트
	function fnUserSave(){
		if ($.trim($("#inputUSER_NM").val()) =="") {
			alert("이름은 필수입력 항목입니다.");
			$("#inputUSER_NM").focus();
			return;
		}
		
		if($("#inputUSER_PWD").val() != "" || $("#inputUSER_PWD_Check").val() != ""){
			if($("#inputUSER_PWD").val() != $("#inputUSER_PWD_Check").val()){
				alert("비밀번호가 일치하지 않습니다.");
				$("#inputUSER_PWD").focus();
				return;
			}
		}
		
		if ($.trim($("#inputCOMP_NM").val()) =="") {
			alert("회사명은 필수입력 항목입니다.");
			$("#inputCOMP_NM").focus();
			return;
		}
		
		if ($.trim($("#inputTEL").val()) =="") {
			alert("연락처는 필수입력 항목입니다.");
			$("#inputTEL").focus();
			return;
		}
		
		if ($.trim($("#inputEMAIL").val()) =="") {
			alert("이메일은 필수입력 항목입니다.");
			$("#inputEMAIL").focus();
			return;
		}
		
		if(confirm('저장하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/userMerge.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("MNU_KIND","view");
	        comSubmit.addParam("USER_ID",$.trim($("#inputUSER_ID").val()));
	        comSubmit.addParam("USER_NM",$.trim($("#inputUSER_NM").val()));
	        comSubmit.addParam("USER_PWD",$("#inputUSER_PWD").val());
	        comSubmit.addParam("USER_POS",$("select[name=comboUSER_POS]").val());
	        comSubmit.addParam("COMP_NM",$.trim($("#inputCOMP_NM").val()));
	        comSubmit.addParam("TEL",$.trim($("#inputTEL").val()));
	        comSubmit.addParam("EMAIL",$.trim($("#inputEMAIL").val()));
	        comSubmit.addParam("AUTH_SETUP",loginfo.AUTH_SETUP);
	        comSubmit.addParam("SMS_RCV_YN",$('input[name="radioSMS_YN"]:checked').val());
	        comSubmit.addParam("USE_YN",loginfo.USE_YN);
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	        comSubmit.submit();
		}
	}
	///////////////////////////////////////////////

	//목록으로
	function fnBoardList(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/board.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
		comSubmit.submit();
	}
	
	//게시글 수정
	function fnEdit(){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/boardEdit.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
		comSubmit.submit();
	}
	
	//게시글 삭제
	function fnDelete(){
		if(confirm('해당 게시글을 삭제하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/boardDelete.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
	        comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	        comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
			comSubmit.submit();
		}
	}
	
	//댓글 삭제
	function fnRplyDelete(REPL_SEQ){
		if(confirm('댓글을 삭제하시겠습니까?')){
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/replyDelete.do'/>");
			comSubmit.addParam("MNU_CD","${MNU_CD}");
	        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
			comSubmit.addParam("modifyReplySeq",REPL_SEQ);
			comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
			comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
			comSubmit.submit();
		}
	}
	
	//댓글 수정 모드
	function fnRplyModify(REPL_SEQ){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/boardView.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
        comSubmit.addParam("modifyReplySeq",REPL_SEQ);
		comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
		comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
		comSubmit.submit();
	}
	
	//파일 다운로드
	function fnFileDownload(FLE_SEQ){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/fileDownload.do'/>");
	    comSubmit.addParam("MNU_CD","${MNU_CD}");
	    comSubmit.addParam("MNU_PATH","${MNU_PATH}");
 	    comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
	    comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
	    comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
	    comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
	    comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
	    comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
	    comSubmit.addParam("FLE_SEQ",FLE_SEQ);
	    comSubmit.submit();
	}

	//댓글
	//파일첨부버튼  & '파일을 선택해주세요' 메세지 표시 체크    
	function fnBtnAndMsgShowCheck(target){
		var ul;
		var inputFileID;
		var lbID;
		var liMsgID;
		
		if(target == "newReply"){
			ul = $('#ulFilesNew');
			inputFileID = "#inputFileNew";
			lbID = "#lb";
			liMsgID = "liMsg";
		}else if(target == "editReply"){
			ul = $('#ulFilesEdit');
			inputFileID = "#inputFileEdit";
			lbID = "#lbReply";
			liMsgID = "liMsgReply";
		}
		
		var ary = new Array();

		for(var i = 1; i < 6; i++){
			//파일불러오기 1~5 버튼(label) 모두 히든처리
			$(lbID + "_" + i).css('display','none');
			
			//파일 첨부 안된 input박스의 index만 ary에 담음
			if($(inputFileID + "_" + i)[0] != undefined){
				if($(inputFileID + "_" + i)[0].files.length != 1){
					ary.push(i);
				}
			}
		}
		
		//파일 첨부 목록이 5개 이하일때만 파일 첨부 버튼 표시
		if(ul.children().size() < 5){
	 		//ary 오름차순 숫자 정렬 
	 		ary.sort(function(a,b){
				return a-b;
			}); 

			//오름차순 정렬된 ary에서 가장 앞의 버튼 표시
			if(ary.length != 0){
				$(lbID + "_" + ary[0]).css('display','block');
			}
		}
		
		//파일 첨부 목록이 없을때 '파일을 선택해주세요' 문구 표시
		if(ul.children().size() == 0){
			var addLiMsg = "<li id='" + liMsgID + "'><p style='color: rgba(255, 255, 255, 0.3); font-size: 15px; padding: 5px;'>파일을 선택해주세요(최대 5개) </p></li>"; 
			ul.append(addLiMsg);
		}else{
			$('li').remove("#" + liMsgID);
		}
	}

	//파일 첨부 이벤트
	function fnInputChanged(obj, target){
		var file = obj.files;
		
		if(file[0].size > 20 * 1024 * 1024){
			alert("20MB 이하 파일만 선택할 수 있습니다.");
			$("#"+obj.id).val('');
			return;
		}
		
		fnAddFileChanged(target);
	}

	//첨부파일 변경 이벤트 
	//첨부파일 추가,삭제시 수행
	//input파일 1~5 에 추가된 파일들을 target 목록에 추가
	function fnAddFileChanged(target){
		var ul;
		var inputFileID;
		
		if(target == "newReply"){
			ul = $('#ulFilesNew');
			inputFileID = "#inputFileNew";
		}else if(target == "editReply"){
			ul = $('#ulFilesEdit');
			inputFileID = "#inputFileEdit";
		}
		
		var addCode = "";
		var isAdd = true;

    	//기존에 저장된 첨부파일 목록(Query result)
    	var jArySavedFiles = ${jsonSavedFiles};
    	
    	//중복검사
    	for(var i = 1; i < 6; i++){
    		var liID = inputFileID + "_" + i;
    		if($(liID)[0].files.length == 1){
    			var targetFileName = $(liID)[0].files[0].name;
    			
	    		//파일input에 추가된 파일끼리 중복검사
	    		for(let j = 1; j < 6; j++){
	    			var liID_2 = inputFileID + "_" + j;
	    			if(i != j && $(liID_2)[0].files.length == 1){
	    				if(targetFileName == $(liID_2)[0].files[0].name){
	    					alert("이미 추가된 파일입니다.");
	    					$(liID_2).val('');
	    					isAdd = false;
	    					break;   					
	    				}
					}
	    		}
    			
	    		//기존 저장된 첨부파일 목록에서 중복검사
 	    		for(let k = 0; k < jArySavedFiles.length; k++){
	    			if(targetFileName == jArySavedFiles[k].FLE_NM_EXT){
	    				alert("이미 추가된 파일입니다.");
	    				$(liID).val('');
	    				isAdd = false;
	    				break;
	    			}
	    		} 
    		}
    	}

    	if(isAdd){
    		//target ul의 하위 항목(li) 
        	var ulChildren = new Array();
        	ulChildren = ul.children();
        	 
        	//id가 없는 li = 저장되기 전 목록에만 추가되어있는 li
        	//input파일 1~5에  있는 모든 파일이 목록에 추가되므로 삭제 후 하단에서 추가
        	for(key in ulChildren){
        		if(ulChildren[key].id == ""){
        			ulChildren[key].remove();
        		}
        	}
        	
    		for(var i = 1; i < 6; i++){
        		var liID = inputFileID + "_" + i;
        		if($(liID)[0].files.length == 1){
        			
    	    		var sizeByte = $(liID)[0].files[0].size;
    	    		var sizeKB = parseInt(sizeByte / 1024);
    	    		var cal = sizeKB / 1024 * 100;
    	    		var sizeMB = Math.floor(cal) / 100;
    	
    	    		addCode = "";
    	    		addCode += "<li style='padding: 5px 5px;'><span style='color:#c89b43;'><i class='far fa-file'></i>";
    	    		if(sizeKB >= 1000){
    	    			addCode += $(liID)[0].files[0].name + " (" + sizeMB + " MB)";
    	    		}else{
    	    			addCode += $(liID)[0].files[0].name + " (" + sizeKB + " KB)";
    	    		}
    	    		var i_target = i +",'"+ target + "'";
    	    		addCode += "</span><a href=javascript:fnFileRemove("+ i_target + ")><i class='fas fa-times' style='padding-left:5px; color:#808080;'></i></a></li>";
    	     		ul.append(addCode);
        		}
    		}
 		}
    	
    	fnBtnAndMsgShowCheck(target);
	}
	
	//추가한 첨부파일 삭제
	//해당 파일 input 초기화한 후 값 있는 input file들로 다시 바인딩됨 
 	function fnFileRemove(key, target){
		var inputFileID;
		//파일 input 초기화 시킴
		if(target == "newReply"){
			inputFileID = "#inputFileNew";
		}else if(target == "editReply"){
			inputFileID = "#inputFileEdit";
		}
		
		$(inputFileID + "_" + key).val('');
		
		fnAddFileChanged(target);
	}
	 
	//기존에 저장한 첨부파일 삭제
	function fnSavedFileRemove(fileSEQ){
		//전역 변수에 삭제할 file SEQ 담아서 저장시 삭제 처리
		removeSEQ.push(fileSEQ);
		console.log("removeSEQ = " + removeSEQ);
		
		//리스트 삭제
		var liID = "#liSavedFile_" + fileSEQ;
		$("li").remove(liID);
		
		fnBtnAndMsgShowCheck("editReply");
	}
	
	//파일 저장
	function fileSave(target){
		var form;
		
		if(target == "newReply"){
			if ($.trim($("#txtReplyNew").val()) =="") {
				alert("답글을 입력해주세요.");
				$("#txtReplyNew").focus();
				return;
			}else if($.trim($("#txtReplyNew").val()).length > 1000){
				alert("답글이 너무 깁니다.\n500자 이하로 작성해주세요.");
				$("#txtReplyNew").focus();
				return;
			}else{
				form = $('#uploadForm')[0];
			}
		}else if(target == "editReply"){
			if ($.trim($("#txtReplyEdit").val()) =="") {
				alert("답글을 입력해주세요.");
				$("#txtReplyEdit").focus();
				return;
			}else if($.trim($("#txtReplyEdit").val().length) > 1000){
				alert("답글이 너무 깁니다.\n500자 이하로 작성해주세요.");
				$("#txtReplyEdit").focus();
				return;
			}else{
				form = $('#uploadFormReply')[0];
			}
		}
		
		if(confirm('저장하시겠습니까?')){
		    var formData = new FormData(form);
		    $.ajax({
		        url : '/fileSave.do',
		        type : 'POST',
		        data : formData,
		        contentType : false,
		        processData : false,
		        enctype: 'multipart/form-data'
		    }).done(function(data){
		    	
		    	fnRplySave(data.savedREPL_SEQ, target);
		    });
		}
	}
	
	//댓글 저장
	function fnRplySave(savedREPL_SEQ, target){
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/replySave.do'/>");
		comSubmit.addParam("MNU_CD","${MNU_CD}");
        comSubmit.addParam("MNU_PATH","${MNU_PATH}");
		comSubmit.addParam("modifyReplySeq",savedREPL_SEQ);
		if(target == "newReply"){
			comSubmit.addParam("CONTENTS",$("#txtReplyNew").val());
		}else{
			comSubmit.addParam("CONTENTS",$("#txtReplyEdit").val());
		}
		comSubmit.addParam("searchedBBSSeq","${searchedCondition.searchedBBSSeq}");
		comSubmit.addParam("searchedPageNo","${searchedCondition.searchedPageNo}");
        comSubmit.addParam("searchedWord","${searchedCondition.searchedWord}");
        comSubmit.addParam("searchedBasicCombo","${searchedCondition.searchedBasicCombo}");
        comSubmit.addParam("searchedBizSeq","${searchedCondition.searchedBizSeq}");
        comSubmit.addParam("searchedItem1Combo","${searchedCondition.searchedItem1Combo}");
        comSubmit.addParam("removeFileSEQ",removeSEQ);
		comSubmit.submit();
	}
</script>
	
<!-- 레이어팝업 스크립트 --> 
<link rel="stylesheet" href="css/layerpop.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 
<script src="js/layerpop.js"></script>
</head>


<body>

<!-- code -->
<div id="wrap"> 
  <!-- header -->
  <header id="header" class="fixed-header clearfix">
    <div class="gnb-overlay-bg"></div>
    <div id="gnbWrap" class="trans300">
      <div id="HeaderInner" class="clearfix">
      	<!-- 3번 -->
        <h1 class="logo trans300"><a href="javascript:fnMain()"><img src="images/logo.png" width="185" height="41" alt="사후관리시스템"/> </a> </h1>
        <div class="header-util-box trans300 clearfix"> 
           <!-- 4번 -->
           <div class="header-lang trans300">
             <span>
                <span><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.</span> 
                <a href="javascript:fnLogout()" style="margin-left:5px;">logout</a>
             </span> 
           </div>
          <!-- 사이트맵 버튼 ( 기본 라인 三 ) -->
         <button onclick="javascript:goDetailsitemap('사이트맵');" class="sitemap-line-btn sitemap-open-btn" title="사이트맵 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3"></span> </button>
          <!-- GNB Mobile --> 
          <a href="javascript;" class="nav-open-btn" title="네비게이션 열기"> <span class="line line1"></span> <span class="line line2"></span> <span class="line line3" ></span> </a> </div>
      </div>
      <!-- 5번 -->
      <nav id="gnb" class="each-menu">
        <ul class="clearfix">
          <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1'}">
	         	  <li class="dep-1"><a href="#">${item_1.MNU_NM}</a>
	         		<ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	      </c:forEach>
        </ul>
      </nav>
      <div class="gnb-overlay-bg-m"></div>
      <nav id="gnbM" class="trans400" >
        <ul id="navigation">
          <li class="has-2dep ">  
             <div class="header-lang-m "><a href="javascript:fnUserInfo()">${loginfo.USER_NM}</a>님 환영합니다.<span ><a href="javascript:fnLogout()">logout</a></span></div>
          </li>
          	 <c:forEach items="${MNUList}" var="item_1" varStatus="status">
	         	<c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP == 'U'}">
	         	  <li class="has-2dep "> <a href="#">${item_1.MNU_NM}</a>
	         	    <ul class="gnb-2dep">
	         			<c:forEach items="${MNUList}" var="item_2" varStatus="status">
	         				<c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
	         				  <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a></li>
	         				</c:if>
	         			</c:forEach>
			        </ul>
			      </li>
		        </c:if>
	        </c:forEach>
        </ul>
      </nav>
    </div>
  </header>
   
  <!-- //header --> 
<!-- container -->

<div id="container">
<!-- visual -->

<section id="visual" style="background:#1e2e47 url(images/008.jpg) no-repeat 50% 0%">
  <div class="area visual-txt-con">
    <h2 class="visual-tit trans400"> 소식 </h2>
    <p class="visual-sub-txt">COMPANY INTRODUCTION</p>
  </div>
</section>
<!-- //visual --> 
<!-- middleArea -->
<div id="middleArea">
<!-- 서브메뉴1 ( 메뉴나열(PC) + 모바일메뉴  )-->
<!-- 6번 -->
<!-- <aside id="sideMenu">
  <div class="side-menu-inner area">
    <ul id="subGnb" class="snb clearfix">
      <div class="nav-on-icon main-move-line">움직이는 바 
        <span style="overflow: hidden; left: 724.719px; width: 171px; display: none;"></span> </div>
      <li class="on"> <a href="info_news.html"><span class="trans300"><em>공지사항</em></span></a> </li>
      <li> <a href="#"><span class="trans300"><em>자유게시판</em></span></a> </li>
      <li> <a href="#"><span class="trans300"><em>FAQ</em></span></a> </li>
    </ul>
  </div>
</aside> -->
	<c:forEach items="${MNUList}" var="item_1" varStatus="status">
	  <c:if test="${item_1.MNU_CD == MNU_CD}">
	    <aside id="sideMenuM" class="cm-top-menu clearfix">
		  <div class="menu-location  location2">
			<span style="line-height:50px; color:#fff; font-weight:500; font-size:15px; margin-left:20px;">${item_1.MNU_NM}</span>
		  </div>
	    </aside>
	  </c:if>
    </c:forEach>
<!-- // --> 

<!-- 상단정보 (정렬) -->
<aside id="contentInfoCon" class="content-info-style01 trans400">
  <h3 class="content-tit rubik">공지사항</h3>
</aside>

<!-- content -->
<section id="content" class="area">

<!-- 컨텐츠 내용 --> 

<!-- 게시판읽기  시작 -->

<div id="bbs-view"  class="bbs-view-con">
<article class="bbs-view-title" >
<header>
  <!-- 7번 -->
  <h2 id="bo_v_title" class="bbs-view-title"> 
  	<span class="bo_v_tit">[${data.ITEM_1_NM}] ${data.TTL}</span> 
  </h2>
</header>
<div  class="bbs-view-info">
  <h2>페이지 정보</h2>
  <span class="sound_only">작성자</span>
  <i class="far fa-user"></i>
  <strong><span class="sv_member"><c:out value="${data.REG_NM}"/></span></strong>
  <span class="sound_only"> 조회 </span>
  <i class="far fa-eye"></i>
  <strong><c:out value="${data.READ_CNT}"/>회</strong>
  <span class="sound_only">작성일</span>
  <i class="far fa-clock" ></i>
  <strong><c:out value="${data.REG_DT}"/></strong> 
</div>
<div class="bo_v_atc">
  <h2 class="bo_v_atc_title">본문</h2>
  
  <!-- 본문 내용 시작  -->
  <div class="bo_v_con" >
  	${data.CONTENTS}
  </div>
  
  	<div id="bo_v_file" class="bo_v_file_cnt" >
		<ul>
		  <c:forEach items="${files}" var="file" varStatus="status">
		  	<li><a href="javascript:fnFileDownload('<c:out value="${file.FLE_SEQ}"/>')"><i class="far fa-file"></i>${file.FLE_INFO}</a></li>
		  </c:forEach>
		</ul>
	</div>
</div>

  <c:if test="${REPL_REG_YN == 'Y'}">
	<!-- 댓글 시작 -->
	<table id="tabReply" class="bbs-write-tbl" style='border-top:0px;'>
	  <tbody>
	    <tr>
	      <td class="file_input">
	      
			<div class="bo_v_share"> 
			  <!-- 댓글 시작 { -->
			  <div class="bbs-view-comment">
			    <textarea id=txtReplyNew name="txtReplyNew" class="bbs-view-comment"></textarea>
			    <div class="com_file_input" style="min-height:40px;">
			      <form id="uploadForm" enctype="multipart/form-data">
			        <div class="bo_v_file_cnt" style="width:50%; min-height:35px; background:#192942; border:1px solid #071a34; float:left;">
			 		  <ul id="ulFilesNew" style="text-align:left; padding: 5px;"> <!--  padding: 12px 10px 0px; -->
					  </ul>
					</div>
				    <div style="float:left; margin-left: 10px;">
					  <label id="lb_1"> 파일불러오기
					    <input id="inputFileNew_1" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'newReply')">
					  </label>
					  <label id="lb_2" style="display:none;"> 파일불러오기
			 		    <input id="inputFileNew_2" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'newReply')">
					  </label>
					  <label id="lb_3" style="display:none;"> 파일불러오기
					    <input id="inputFileNew_3" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'newReply')">
			  		  </label>
					  <label id="lb_4" style="display:none;"> 파일불러오기
						<input id="inputFileNew_4" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'newReply')">
					  </label>
					  <label id="lb_5" style="display:none;"> 파일불러오기
						<input id="inputFileNew_5" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'newReply')">
					  </label>
					</div>
				  </form>
				  	<div style="float:right;">
				  	  <button class="cm-fr btn-style01" onClick="javascript:fileSave('newReply');"><i class="far fa-comments"></i> 답글달기</button>
				    </div>
				</div>
			  </div>
		    </div>
		  </td>
		</tr>
	  </tbody>
	</table>

	<section id="bo_vc" >
      <article>
        <table class="bbs-write-tbl" style="border:0px;">
          <tbody>
            <c:forEach items="${reply}" var="item_1" varStatus="status">
              <tr>
              <td class="file_input" style="border:0px; padding:10px 0px;">
		        <header>
		          <span class="member"><i class="far fa-user"></i>${item_1.REG_NM}</span>
		          	<span class="sound_only">작성일</span>
		          	<span class="bo_vc_hdinfo">
		          	  <i class="fa fa-clock" aria-hidden="true"></i><time datetime="2020-02-07T15:45:00+09:00">${item_1.EDT_DT}</time>
		          	</span>
		          	<c:if test="${item_1.REG_ID == loginfo.USER_ID || loginfo.AUTH_SETUP == 'A'}">
		              <a href="javascript:fnRplyDelete('${item_1.REPL_SEQ}')" class="cm-fr  ml10"  title="삭제"> <i class="fas fa-times"></i><span class="sound_only" >삭제</span></a>
		              
		              <c:if test="${item_1.REPL_SEQ != searchedCondition.modifyReplySeq}"> 
		        	    <a href="javascript:fnRplyModify('${item_1.REPL_SEQ}')" class="cm-fr" title="수정"> <i class="fas fa-edit" ></i><span class="sound_only" >수정</span></a><!-- 수정 -->
		        	  </c:if>
		          	</c:if>
		        </header>
        
		        <c:if test="${item_1.REPL_SEQ == searchedCondition.modifyReplySeq}">
			      <div class="bbs-view-comment" style="margin:0px 0px 50px 0px;">
			        <textarea id="txtReplyEdit" name="txtReplyEdit" class="bbs-view-comment" autofocus>${item_1.CONTENTS}</textarea>
				    <div class="com_file_input">
				      <form id="uploadFormReply" enctype="multipart/form-data">
				        <div class="bo_v_file_cnt" style="width:50%; min-height:35px; background:#192942; border:1px solid #071a34; float:left;">
			 		      <ul id="ulFilesEdit" style="text-align:left; padding: 5px;"> <!--  padding: 12px 10px 0px; -->
					      </ul>
					    </div>
						<div style="float:left; margin-left: 10px;">
						  <label id="lbReply_1"> 파일불러오기
						    <input id="inputFileEdit_1" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'editReply')">
						  </label>
						  <label id="lbReply_2" style="display:none;"> 파일불러오기
				 		    <input id="inputFileEdit_2" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'editReply')">
						  </label>
						  <label id="lbReply_3" style="display:none;"> 파일불러오기
						    <input id="inputFileEdit_3" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'editReply')">
				  		  </label>
						  <label id="lbReply_4" style="display:none;"> 파일불러오기
							<input id="inputFileEdit_4" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'editReply')">
						  </label>
						  <label id="lbReply_5" style="display:none;"> 파일불러오기
							<input id="inputFileEdit_5" name="uploadFiles" type="file" onchange="javascript:fnInputChanged(this, 'editReply')">
						  </label>
					    </div>
					  </form>
					</div>
					<button class="cm-fr btn-style01" style="margin:0px 0px 0px 10px;" onClick="javascript:fnRplyModify('')">취소</button>
			        <button class="cm-fr btn-style01" onClick="javascript:fileSave('editReply')"><i class="far fa-comments"></i> 저장</button>
			      </div>
		        </c:if>
        
		        <c:if test="${item_1.REPL_SEQ != searchedCondition.modifyReplySeq}">
			      <div class="cmt_contents">
			        <p>${item_1.CONTENTS}</p>
					<ul style="text-align:right;">
					  <c:forEach items="${replyFiles}" var="file" varStatus="status">
					  	<c:if test="${file.REPL_SEQ == item_1.REPL_SEQ}">
					  	  <li><a href="javascript:fnFileDownload('<c:out value="${file.FLE_SEQ}"/>')"><span style='color:#c89b43'><i class="far fa-file"></i>${file.FLE_INFO}</span></a></li>
					  	</c:if>
					  </c:forEach>
					</ul>
			      </div>
		        </c:if>
              </td>
			  </tr>
            </c:forEach>
          </tbody>
        </table>
      </article>
    </section>
    <!-- 댓글 끝 -->
  </c:if>

  <div class="cm-btn-controls">
    <button type="submit" class="btn-style01" onClick="javascript:fnEdit()" <c:if test="${loginfo.AUTH_SETUP != 'A'}">style="display: none;"</c:if>> 수정 </button>
    <button type="submit" class="btn-style01" onClick="javascript:fnDelete()" <c:if test="${loginfo.AUTH_SETUP != 'A'}">style="display: none;"</c:if>> 삭제 </button>
    <button type="submit" class="btn-style01" onClick="javascript:fnBoardList()"> 목록 </button>
  </div>

<!-- // 게시판읽  끝 --> 
<!-- //컨텐츠 내용 -->
</article>

<!-- //content -->
</div>
</section>	
<!-- //middleArea -->
</div>

<!-- //container --> 
<!-- footer -->

<footer id="footer" class="sub"> 
  <a href="#wrap" class="to-top-btn"><i class="material-icons"></i></a>
  <div id="footerInner" class="clearfix">
    <article id="footerTop" class="">
      <div class="area-box clearfix">
        <div class="footer-info-cont"> <span class="copyright">© 2020 <em>Greentech INC</em> All rights reserved.</span> </div>
      </div>
    </article>
  </div>
</footer>

<!-- 모달 레이어팝업 -->
<article class="modal-fixed-pop-wrapper">
  <div class="modal-fixed-pop-inner">
    <div class="modal-loading"><span class="loading"></span></div>
    <div class="modal-inner-box">
      <div class="modal-inner-content"> 
        <!-- ajax 내용 --> 
      </div>
    </div>
  </div>
</article>
<!-- //footer -->
</div>
</div>	
	
<!-- //code -->
	<!--sitemap Popup Start -->

	
	<article class="layerpop-cont">
	<div id="mask"></div>		  
    <div id="layerbox-sitemap" class="layerpopsitemap" style="width: 900px; height: 330px;">
        <article class="layerpop_area">
        <div class="title">사이트맵</div>
		
        <a href="javascript:popupClosesitemap();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a> 
        <div class="content">
       
        
         
          <article class="sitemap-wrapper">
		
		<ul >
		<!-- 9번 -->
         <c:forEach items="${MNUList}" var="item_1" varStatus="status">
           <c:if test="${item_1.MNU_STEP=='1' && item_1.AUTH_SETUP=='U'}">
             <li>
         	   <h2>${item_1.MNU_NM}</h2>
         	     <ul class="sitemap-2dep">
         		   <c:forEach items="${MNUList}" var="item_2" varStatus="status">
         		     <c:if test="${item_2.UPPER_CD==item_1.MNU_CD}">
         		 	   <li> <a href="javascript:fnMenuAction('${item_2.MNU_CD_PATH}')"><span class="trans300"><em>${item_2.MNU_NM}</em></span></a> </li>
         			 </c:if>
         		   </c:forEach>
		        </ul>
		     </li>
	       </c:if>
         </c:forEach>
			
          
        </ul>
		
		
	</article>
       
       
		  
    
        </div>
        </article>
    </div>
	</article>

    <!--sitemap Popup End -->

<!-- 사용자 정보 팝업 -->
<article class="layerpop-cont">
  <div id="mask"></div>
  <div id="layerbox" class="layerpop" style="width: 700px; height: 570px;">
    <article class="layerpop_area">
      <div class="title">사용자 관리</div>
      <a href="javascript:popupClose();" class="layerpop_close" id="layerbox_close"><i class="material-icons"></i></a>
      <div class="content"> 
        <!-- 문의 폼 시작 -->
        
        <div class="layerpop-write-con" style="padding-right: 20px; float: left; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table class="layerpop-write-tbl" >
              <caption>
              사용자 관리 작석폼
              </caption>
              <colgroup>
              <col >
              <col>
              </colgroup>
              <tbody  >
                <tr>
                  <th scope="row">* 아이디</th>
                  <td>
                  	<input id="inputUSER_ID" type="text" class="write-input width100" autocomplete="off" readonly style="color:#808080;">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호</th>
                  <td>
                  	<input id="inputUSER_PWD" type="password" class="write-input width100" placeholder="비밀번호를 입력해주세요" autocomplete="off" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <th scope="row">직급</th>
                  <td>
                  	<select id="comboUSER_POS" name="comboUSER_POS" class="write-select width100" >
                  	<option value="">선택</option>
		               <c:forEach items="${listUserPOS}" var="UserPOS" varStatus="status">
		               	<option value="${UserPOS.DTL_CD}">${UserPOS.DTL_NM}</option>
		               </c:forEach>
                    </select></td>
                </tr>
                <tr>
                  <th scope="row">* 연락처</th>
                  <td>
                  	<input id="inputTEL" type="text" class="write-input width100" placeholder="연락처를 입력해주세요" autocomplete="off" tabindex="6">
                  </td>
                </tr>
                <tr>
                  <th scope="row">SMS 수신동의 <span class="info_text"> <i class="fas fa-info-circle"></i> SMS 수신 동의시 문자 발송이 됩니다.</span></th>
                  <td>
                  <fieldset class="custom-radio">
                  	<span class="radio-item" >
                      <input id="inputSMS_Y" name="radioSMS_YN" type="radio" value="Y" tabindex="12" checked>
                      <label for="inputSMS_Y" >동의</label>
                    </span>
                    <span class="radio-item" >
                      <input id="inputSMS_N" name="radioSMS_YN" type="radio" value="N" tabindex="13">
                      <label for="inputSMS_N" >미동의</label>
                    </span>
                    </fieldset>
                  </td>
                </tr> 
              </tbody>
            </table>
          </article>
        </div>
        <div class="layerpop-write-con" style="padding-left: 20px; float: right; width: calc(50% - 20px);">
          <article class="layerpop-write-tbl-box">
            <table  class="layerpop-write-tbl"  >
              <tbody>
                <tr>
                  <th scope="row">* 이름</th>
                  <td>
                  	<input id="inputUSER_NM" type="text" class="write-input width100" placeholder="이름을 입력해주세요" autocomplete="off" tabindex="2">
                  </td>
                </tr>
                <tr>
                  <th scope="row">비밀번호 확인</th>
                  <td>
                  	<input id="inputUSER_PWD_Check" type="password" class="write-input width100" placeholder="비밀번호를 확인해주세요" autocomplete="off" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 회사명</th>
                  <td>
                  	<input id="inputCOMP_NM" type="text" class="write-input width100" placeholder="회사명을 입력해주세요" autocomplete="off" tabindex="5">
                  </td>
                </tr>
                <tr>
                  <th scope="row">* 이메일</th>
                  <td>
                  	<input id="inputEMAIL" type="text" class="write-input width100" placeholder="이메일을 입력해주세요" autocomplete="off" tabindex="7">
                  </td>
                </tr>
              </tbody>
            </table>
          </article>
        </div>
        
        <!-- // 문의 폼 끝 -->
        <div class="btn-controls" style="float: left; display: block; width: 100%" >
          <button type="submit" class="btn-style01" onClick="javascript:fnUserSave();">저장</button>
          <button type="submit" class="btn-style01" onClick="javascript:popupClose();">닫기</button>
        </div>
      </div>
    </article>
  </div>
</article>
<!--사용자 정보 팝업 End -->

<!-- 10번 -->
<form id="commonForm" name="commonForm"></form>
</body>
</html>