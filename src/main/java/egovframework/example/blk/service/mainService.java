package egovframework.example.blk.service;

import java.util.HashMap;
import java.util.List;

public interface mainService {

	Object Insert_USER_LOG_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> SelectLogUser(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_BBS_MST_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_BBS_MST_MNG_View(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_BBS_REPL_MNG(HashMap<String,Object> map) throws Exception;
	
	int Update_BBS_MST_CNT(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_CD_INFO(HashMap<String,Object> map) throws Exception;
	
	int Merge_BBS_MST_MNG(HashMap<String,Object> map) throws Exception;
	
	int Delete_BBS_MST_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_MNU_MNG(HashMap<String,Object> map) throws Exception;
	
	int Delete_BBS_REPL_MNG(HashMap<String,Object> map) throws Exception;
	
	void Merge_BBS_REPL_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_Site_By_User(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_FAQ_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_FAQ_MNG_View(HashMap<String,Object> map) throws Exception;
	
	int Update_FAQ_MNG_CNT(HashMap<String,Object> map) throws Exception;
	
	int Merge_FAQ_MNG(HashMap<String,Object> map) throws Exception;
	
	int Delete_FAQ_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_SITE_MNG(HashMap<String,Object> map) throws Exception;
	
	int Merge_SITE_MNG(HashMap<String,Object> map) throws Exception;
	
	int Delete_SITE_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_USER_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_User_Mapping_Site(HashMap<String,Object> map) throws Exception;
	
	int Delete_SITE_USER_MAP(HashMap<String,Object> map) throws Exception;
	
	Object Insert_SITE_USER_MAP(HashMap<String,Object> map) throws Exception;
	
	Object Merge_USER_MNG(HashMap<String,Object> map) throws Exception;
	
	int Delete_USER_MNG(HashMap<String,Object> map) throws Exception;
	
	Object Update_USER_PWD(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_MST_CD(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_DTL_CD(HashMap<String,Object> map) throws Exception;
	
	Object Merge_MST_INFO(HashMap<String,Object> map) throws Exception;
	
	Object Merge_DTL_INFO(HashMap<String,Object> map) throws Exception;
	
	int Insert_BBS_FLE_MNG(HashMap<String,Object> map) throws Exception;
	
	int Insert_BBS_FLE_MNG_REPL(HashMap<String,Object> map) throws Exception;
	
	int Delete_BBS_FLE_MNG(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_File_By_SEQ(HashMap<String,Object> map) throws Exception;
	
	List<HashMap<String,Object>> Select_REPL_File(HashMap<String,Object> map) throws Exception;
	
	
}
