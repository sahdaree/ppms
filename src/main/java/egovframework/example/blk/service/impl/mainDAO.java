package egovframework.example.blk.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

@Repository("mainDAO")
public class mainDAO extends EgovAbstractDAO{

	@SuppressWarnings("rawtypes")
	public Object Insert_USER_LOG_MNG(HashMap map) throws Exception {
		return (Object) insert("mainDAO.Insert_USER_LOG_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List SelectLogUser(HashMap map) throws Exception {
		return (List) list("mainDAO.SelectLogUser", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_BBS_MST_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_BBS_MST_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_BBS_MST_MNG_View(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_BBS_MST_MNG_View", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_BBS_REPL_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_BBS_REPL_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Update_BBS_MST_CNT(HashMap map) throws Exception {
		return (int)update("mainDAO.Update_BBS_MST_CNT", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_CD_INFO(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_CD_INFO", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_MNU_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_MNU_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Merge_BBS_MST_MNG(HashMap map) throws Exception {
		return (int)insert("mainDAO.Merge_BBS_MST_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_BBS_MST_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_BBS_MST_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_BBS_REPL_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_BBS_REPL_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public void Merge_BBS_REPL_MNG(HashMap map) throws Exception {
		insert("mainDAO.Merge_BBS_REPL_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_Site_By_User(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_Site_By_User", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_FAQ_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_FAQ_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_FAQ_MNG_View(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_FAQ_MNG_View", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Update_FAQ_MNG_CNT(HashMap map) throws Exception {
		return (int)update("mainDAO.Update_FAQ_MNG_CNT", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Merge_FAQ_MNG(HashMap map) throws Exception {
		return (int)insert("mainDAO.Merge_FAQ_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_FAQ_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_FAQ_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_SITE_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_SITE_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Merge_SITE_MNG(HashMap map) throws Exception {
		return (int)insert("mainDAO.Merge_SITE_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_SITE_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_SITE_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_USER_MNG(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_USER_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_User_Mapping_Site(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_User_Mapping_Site", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_SITE_USER_MAP(HashMap map) throws Exception {
		return delete("mainDAO.Delete_SITE_USER_MAP", map);
	}
	
	@SuppressWarnings("rawtypes")
	public Object Insert_SITE_USER_MAP(HashMap map) throws Exception {
		return (Object)insert("mainDAO.Insert_SITE_USER_MAP", map);
	}
	
	@SuppressWarnings("rawtypes")
	public Object Merge_USER_MNG(HashMap map) throws Exception {
		return (Object)insert("mainDAO.Merge_USER_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Delete_USER_MNG(HashMap map) throws Exception {
		return (int)update("mainDAO.Delete_USER_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public Object Update_USER_PWD(HashMap map) throws Exception {
		return (Object)update("mainDAO.Update_USER_PWD", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_MST_CD(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_MST_CD", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_DTL_CD(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_DTL_CD", map);
	}
	
	@SuppressWarnings("rawtypes")
	public Object Merge_MST_INFO(HashMap map) throws Exception {
		return (Object)insert("mainDAO.Merge_MST_INFO", map);
	}

	@SuppressWarnings("rawtypes")
	public Object Merge_DTL_INFO(HashMap map) throws Exception {
		return (Object)insert("mainDAO.Merge_DTL_INFO", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Insert_BBS_FLE_MNG(HashMap map) throws Exception {
		return (int)insert("mainDAO.Insert_BBS_FLE_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public int Insert_BBS_FLE_MNG_REPL(HashMap map) throws Exception {
		return (int)insert("mainDAO.Insert_BBS_FLE_MNG_REPL", map);
	}
	
	
	@SuppressWarnings("rawtypes")
	public int Delete_BBS_FLE_MNG(HashMap map) throws Exception {
		return (int)delete("mainDAO.Delete_BBS_FLE_MNG", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_File_By_SEQ(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_File_By_SEQ", map);
	}
	
	@SuppressWarnings("rawtypes")
	public List Select_REPL_File(HashMap map) throws Exception {
		return (List) list("mainDAO.Select_REPL_File", map);
	}
	
	
}
