package egovframework.example.blk.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.example.blk.service.mainService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("mainService")
public class mainServiceImpl extends EgovAbstractServiceImpl implements mainService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(mainServiceImpl.class);

	//선언부 해당 DAO 사용시 선언
	@Resource(name = "mainDAO")
	private mainDAO mainDAO;

	public Object Insert_USER_LOG_MNG(HashMap map)  throws Exception {
		return mainDAO.Insert_USER_LOG_MNG(map);
	}
	
	public List SelectLogUser(HashMap map) throws Exception {
		return mainDAO.SelectLogUser(map);
	}

	public List Select_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Select_BBS_MST_MNG(map);
	}
	
	public List Select_BBS_MST_MNG_View(HashMap map) throws Exception {
		return mainDAO.Select_BBS_MST_MNG_View(map);
	}
	
	public List Select_BBS_REPL_MNG(HashMap map) throws Exception {
		return mainDAO.Select_BBS_REPL_MNG(map);
	}
	
	public int Update_BBS_MST_CNT(HashMap map) throws Exception {
		return mainDAO.Update_BBS_MST_CNT(map);
	}
	
	public List Select_CD_INFO(HashMap map) throws Exception {
		return mainDAO.Select_CD_INFO(map);
	}
	
	public int Merge_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Merge_BBS_MST_MNG(map);
	}
	
	public int Delete_BBS_MST_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_BBS_MST_MNG(map);
	}
	
	public List Select_MNU_MNG(HashMap map) throws Exception {
		return mainDAO.Select_MNU_MNG(map);
	}
	
	public int Delete_BBS_REPL_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_BBS_REPL_MNG(map);
	}
	
	public void Merge_BBS_REPL_MNG(HashMap map) throws Exception {
		mainDAO.Merge_BBS_REPL_MNG(map);
	}

	public List Select_Site_By_User(HashMap map) throws Exception {
		return mainDAO.Select_Site_By_User(map);
	}
	
	public List Select_FAQ_MNG(HashMap map) throws Exception {
		return mainDAO.Select_FAQ_MNG(map);
	}
	
	public List Select_FAQ_MNG_View(HashMap map) throws Exception {
		return mainDAO.Select_FAQ_MNG_View(map);
	}
	
	public int Update_FAQ_MNG_CNT(HashMap map) throws Exception {
		return mainDAO.Update_FAQ_MNG_CNT(map);
	}
	
	public int Merge_FAQ_MNG(HashMap map) throws Exception {
		return mainDAO.Merge_FAQ_MNG(map);
	}
	
	public int Delete_FAQ_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_FAQ_MNG(map);
	}
		
	public List Select_SITE_MNG(HashMap map) throws Exception {
		return mainDAO.Select_SITE_MNG(map);
	}
	
	public int Merge_SITE_MNG(HashMap map) throws Exception {
		return mainDAO.Merge_SITE_MNG(map);
	}
	
	public int Delete_SITE_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_SITE_MNG(map);
	}
	
	public List Select_USER_MNG(HashMap map) throws Exception {
		return mainDAO.Select_USER_MNG(map);
	}
	
	public List Select_User_Mapping_Site(HashMap map) throws Exception {
		return mainDAO.Select_User_Mapping_Site(map);
	}
	
	public int Delete_SITE_USER_MAP(HashMap map) throws Exception {
		return mainDAO.Delete_SITE_USER_MAP(map);
	}
	
	public Object Insert_SITE_USER_MAP(HashMap map) throws Exception {
		return mainDAO.Insert_SITE_USER_MAP(map);
	}
	
	public Object Merge_USER_MNG(HashMap map) throws Exception {
		return mainDAO.Merge_USER_MNG(map);
	}
	
	public int Delete_USER_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_USER_MNG(map);
	}
	
	public Object Update_USER_PWD(HashMap map) throws Exception {
		return mainDAO.Update_USER_PWD(map);
	}
	
	public List Select_MST_CD(HashMap map) throws Exception {
		return mainDAO.Select_MST_CD(map);
	}

	public List Select_DTL_CD(HashMap map) throws Exception {
		return mainDAO.Select_DTL_CD(map);
	}
	
	public Object Merge_MST_INFO(HashMap map) throws Exception {
		return mainDAO.Merge_MST_INFO(map);
	}
	
	public Object Merge_DTL_INFO(HashMap map) throws Exception {
		return mainDAO.Merge_DTL_INFO(map);
	}
	
	public int Insert_BBS_FLE_MNG(HashMap map) throws Exception {
		return mainDAO.Insert_BBS_FLE_MNG(map);
	}
	
	public int Insert_BBS_FLE_MNG_REPL(HashMap map) throws Exception {
		return mainDAO.Insert_BBS_FLE_MNG_REPL(map);
	}
	
	
	public int Delete_BBS_FLE_MNG(HashMap map) throws Exception {
		return mainDAO.Delete_BBS_FLE_MNG(map);
	}
	
	public List Select_File_By_SEQ(HashMap map) throws Exception {
		return mainDAO.Select_File_By_SEQ(map);
	}
	
	public List Select_REPL_File(HashMap map) throws Exception {
		return mainDAO.Select_REPL_File(map);
	}
	
	
}
