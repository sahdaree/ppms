package egovframework.example.blk.web;

import java.util.HashMap;
import java.util.List;

import egovframework.example.blk.service.blkService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

@Controller
public class blkController {

	/** blkService */
	@Resource(name = "blkService")
	private blkService blkService;
	
	@RequestMapping("/getblk.do")
	protected ModelAndView getblk() throws Exception
	{
		HashMap map = new HashMap();
		map.put("MST_CD", "101");
		
		List blkList = blkService.blktreeselect(map);		
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("data",blkList);
				
		return mav;
	}
	
	@RequestMapping("/getmesr.do")
	protected ModelAndView getmesr() throws Exception
	{
		HashMap map = new HashMap();
		map.put("MST_CD", "101");
		
		List mesrList = blkService.mesrselect(map);
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("data",mesrList);
		
		return mav;
	}
	
	@RequestMapping("/getmesr2.do")
	protected ModelAndView getmesr2() throws Exception
	{
		HashMap map = new HashMap();
		map.put("MST_CD", "101");
		
		List mesrList = blkService.mesrselect2(map);
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("data",mesrList);
		
		return mav;
	}
	
	@RequestMapping("/getParam.do")
	protected ModelAndView getParam(@RequestParam("nodeID") String id, @RequestParam("fromDT") String fDT, @RequestParam("toDT") String tDT) throws Exception
	{
		HashMap map = new HashMap();
		map.put("blkID", id);
		map.put("fromDT", fDT);
		map.put("toDT", tDT);
		
		List mesrList = blkService.mesrselect2(map);
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("data",mesrList);
		
		return mav;
	}

	@RequestMapping("/getBoardData.do")
	protected ModelAndView getBoardData() throws Exception
	{
		HashMap map = new HashMap();
		List boardList = blkService.boardSearch(map);
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		mav.addObject("data", boardList);
		
		return mav;
	}
	
		
	

}
