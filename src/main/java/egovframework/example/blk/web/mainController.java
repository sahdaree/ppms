package egovframework.example.blk.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import egovframework.example.blk.service.mainService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.web.servlet.ModelAndView;





@Controller
public class mainController {

	/** mainService */
	@Resource(name = "mainService")
	private mainService mainService;
	
    /**
     * SHA-256으로 해싱하는 메소드
     * 
     * @param bytes
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public static String sha256(String msg) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(msg.getBytes());
        
        return bytesToHex(md.digest());
    }
 
    /**
     * 바이트를 헥스값으로 변환한다
     * 
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b: bytes) {
          builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }


	private String ChoiceGBNCodeByMenuPath(String MNU_PATH) {

		switch (MNU_PATH) {
		case "noticeBoard":
			return "0010";

		case "FreeBoard":
			return "0020";
			
		case "FAQBoard":
			return "0020";
			
		case "FaultBoard":
			return "0030";
			
		case "PreventionBoard":
			return "0050";
			
		case "VideoBoard":
			return "0030";
			
		case "FileBoard":
			return "0030";
			
		case "adminUserMng":
			return "0080";
			
		default:
			return "";
		}
	}
	
	/*
	 * 세부 코드 조회  
	 */
	private List<HashMap<String,Object>> SelectDetailCodeList(String MST_CD) throws Exception{

		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("MST_CD", MST_CD);
		List<HashMap<String,Object>> listDTL_CD = mainService.Select_CD_INFO(map);
		
		return listDTL_CD;
	}
	
	/*
	 * 첨부파일 삭제
	 */
	private void removeFile(String removeFileSEQ) throws Exception{
		//TODO : 서버파일 삭제 로직 추가?
		String[] aryFileSEQ = removeFileSEQ.split(",");
		
		for (String FLE_SEQ : aryFileSEQ) {
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("FLE_SEQ", FLE_SEQ);
			mainService.Delete_BBS_FLE_MNG(map);
		}
	}

	/**
    * Map을 json으로 변환한다.
    *
    * @param map Map<String, Object>.
    * @return JSONObject.
    */
    public static JSONObject getJsonStringFromMap(Map<String, Object> map)
    {
        JSONObject jsonObject = new JSONObject();
        for( Map.Entry<String, Object> entry : map.entrySet() ) {
            String key = entry.getKey();
            String value ="";
            if(entry.getValue() != null) {
            	value = entry.getValue().toString();	
            }
            jsonObject.put(key, value);
        }
        
        return jsonObject;
    }
	
    /*
     * String 콤마(,) 제거 후 마지막 index 데이터 반환 
     */
    private String CommaRemoveLastData(String str) {
		if(str.contains(",")) {
			int lastIndex = str.lastIndexOf(",") + 1;
			str = str.substring(lastIndex);
		}
    	return str;
    }

    /*
     * 세션 유저 정보 및 유저 메뉴 재바인딩
     */
    private boolean SessionUserInfoRebinding(HashMap<String,Object> map, HttpServletRequest request) throws Exception {
    	//유저 아이디로 조회하기 위해서 구분 줌
    	if(map.get("GBN") == null) {
    		map.put("GBN", "dupliCheck");	
    	}
    	
		List<HashMap<String,Object>> listUser = mainService.SelectLogUser(map);

		if (listUser.size() == 1) {
			//유저 정보 세션에 재바인딩
			request.getSession().setAttribute("loginfo", ((HashMap<String,Object>)listUser.get(0)));
			
			//사용자 권한 체크 후 메뉴리스트 쿼리 파라미터로 사용
			String strAUTH_SETUP = ((HashMap<String,Object>)listUser.get(0)).get("AUTH_SETUP").toString();
			if(strAUTH_SETUP.equals("A")) {
				map.put("AUTH_SETUP", "('A','U','P')");	
			}else if(strAUTH_SETUP.equals("P")) {
				map.put("AUTH_SETUP", "('U','P')");
			}else if(strAUTH_SETUP.equals("U")) {
				map.put("AUTH_SETUP", "('U')");
			}
			
			//유저 메뉴 세션에 재바인딩
			List<HashMap<String,Object>> listMNU = mainService.Select_MNU_MNG(map);
			request.getSession().setAttribute("USER_MENU", listMNU);
			
			return true;
		}else {
			return false;
		}
    }
    
    private boolean MenuAuthCheck(String MNU_CD, HttpServletRequest request) throws Exception {
    	List<HashMap<String,Object>> listUserMenu = (List<HashMap<String,Object>>)request.getSession().getAttribute("USER_MENU");
    	boolean bMenuAuth = false;
    	for (HashMap<String, Object> map : listUserMenu) {
    		if(map.get("MNU_CD").toString().equals(MNU_CD)) {
    			bMenuAuth = true;
    			break;
    		}
		}
    	return bMenuAuth;
    }
    
    private void UserConnectionLog(String strMNU_CD, HttpServletRequest request) throws Exception {
        String strIP = null;
        //HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();

        strIP = request.getHeader("X-Forwarded-For");
        
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("Proxy-Client-IP"); 
        } 
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("WL-Proxy-Client-IP"); 
        } 
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("HTTP_CLIENT_IP"); 
        } 
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("HTTP_X_FORWARDED_FOR"); 
        }
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("X-Real-IP"); 
        }
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("X-RealIP"); 
        }
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getHeader("REMOTE_ADDR");
        }
        if (strIP == null || strIP.length() == 0 || "unknown".equalsIgnoreCase(strIP)) { 
        	strIP = request.getRemoteAddr(); 
        }
        
    	HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		map.put("MNU_CD", strMNU_CD);
		
//	    String strIP = request.getHeader("X-Forwarded-For");
//	    if (strIP == null) {
//	    	strIP = request.getRemoteAddr();
//	    }
	    
//	    if(strIP.equals("0:0:0:0:0:0:0:1")) {
//	    	strIP = "127.0.0.1";
//	    }
	    
	    System.out.println("Client IP Address : " + strIP);
	    map.put("CONN_IP", strIP);
		mainService.Insert_USER_LOG_MNG(map);
    }
    
    /*
     * 메인 화면
     */
	@RequestMapping("/main.do")
	protected ModelAndView main(HttpServletRequest request) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("state","login");
		mav.setViewName("login");

		return mav;
	}
	
	/*
	 * 로그인
	 */
	@RequestMapping(value="/Login.do", method=RequestMethod.POST)
	protected ModelAndView Login(@RequestParam("USER_ID") String USER_ID,
								 @RequestParam("USER_PWD") String USER_PWD,
								 HttpServletRequest request) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", USER_ID);
		map.put("USER_PWD", sha256(USER_PWD));
		map.put("GBN", "login");
		
		//세션에 유저정보 및 유저메뉴 바인딩
		boolean bSessionInfo = SessionUserInfoRebinding(map, request);
		
		if(bSessionInfo) {
			mav.setViewName("forward:/board.do");
		}else {
			//로그인 실패 처리
			mav.setViewName("login");
			//mav.addObject("resultMsg","가입되지 않은 아이디이거나, 잘못된 비밀번호입니다.");
			mav.addObject("resultMsg","로그인 정보가 일치하지 않거나, 미사용 처리된 아이디입니다.");
		}
		return mav;
	}
	
	/*
	 * 로그아웃
	 */
	@RequestMapping(value="/Logout.do", method=RequestMethod.POST)
	protected ModelAndView actionLogout(HttpServletRequest request) throws Exception {
		
		System.out.println("********** 로그아웃 **********");
		System.out.println("세션-USER_ID : " + ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		System.out.println("세션-USER_NM : " + ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_NM"));
		System.out.println("세션-AUTH_SETUP : " + ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("AUTH_SETUP"));
		System.out.println("세션-USER_MENU : " + request.getSession().getAttribute("USER_MENU"));
		request.getSession().removeAttribute("loginfo");
		request.getSession().removeAttribute("USER_MENU");
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName("login");

		return mav;
	}
	
    /*
     * 세션만료
     */
	@RequestMapping(value="/SessionOut.do")
	protected ModelAndView SessionOut(HttpServletRequest request) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("resultMsg", "세션이 만료되어 로그인 화면으로 이동합니다.");
		mav.setViewName("login");

		return mav;
	}
	

	/*
	 * 유저 정보 찾기
	 */
	@RequestMapping(value="/SearchUserInfo.do")
	protected ModelAndView SearchUserInfo(@RequestParam(value="TEL", defaultValue="") String TEL,
										  @RequestParam("GBN") String GBN,
										  @RequestParam(value="USER_ID", defaultValue="") String USER_ID)throws Exception {
		
		System.out.println("TEL : " + TEL);
		System.out.println("GBN : " + GBN);
		System.out.println("USER_ID : " + USER_ID);
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("TEL", TEL);
		map.put("GBN", GBN);
		map.put("USER_ID", USER_ID);

		List<HashMap<String,Object>> userList = mainService.SelectLogUser(map);
		System.out.println("userList : " + userList);
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("data", userList);

		return mav;
	}
	
	/*
	 * 비밀번호 재설정
	 */
	@RequestMapping(value="/resettingPassword.do")
	protected ModelAndView resettingPassword(@RequestParam("USER_ID") String USER_ID,
											 @RequestParam("USER_PWD") String USER_PWD
										  	 )throws Exception {
		
		System.out.println("USER_PWD : " + USER_PWD);
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", USER_ID);
		map.put("USER_PWD", sha256(USER_PWD));
		mainService.Update_USER_PWD(map);

		ModelAndView mav = new ModelAndView("jsonView");

		return mav;
	}
	
	
	
	@RequestMapping(value="/board.do", method=RequestMethod.POST)
	protected ModelAndView actionBoard(@RequestParam(value="MNU_CD", defaultValue="001001") String MNU_CD,
									   @RequestParam(value="MNU_PATH", defaultValue="noticeBoard") String MNU_PATH,
									   @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
									   @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									   @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									   @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									   @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
									   @RequestParam(value="searchedItem2Combo", required=false, defaultValue="*_*") String searchedItem2Combo,
									   HttpServletRequest request) throws Exception {
		
		//파일다운로드 이후 파라미터가 두 번 담긴 @,@,@ 형태로 넘와서 처리
		String strMNU_CD = CommaRemoveLastData(MNU_CD);
		String strMNU_PATH = CommaRemoveLastData(MNU_PATH);
		String strSearchedPageNo = CommaRemoveLastData(searchedPageNo);
		String strSearchedWord = CommaRemoveLastData(searchedWord);
		String strSearchedBasicCombo = CommaRemoveLastData(searchedBasicCombo);
		String strSearchedBizSeq = CommaRemoveLastData(searchedBizSeq);
		String strSearchedItem1Combo = CommaRemoveLastData(searchedItem1Combo);
		String strSearchedItem2Combo = CommaRemoveLastData(searchedItem2Combo);
		
		
		System.out.println("********** 게시판 **********");
		System.out.println("MNU_CD : "+ strMNU_CD);
		System.out.println("MNU_PATH : "+ strMNU_PATH);
		System.out.println("searchedPageNo : "+ strSearchedPageNo);
		System.out.println("searchedWord : "+ strSearchedWord);
		System.out.println("searchedBasicCombo : "+ strSearchedBasicCombo);
		System.out.println("searchedBizSeq : "+ strSearchedBizSeq);
		System.out.println("searchedItem1Combo : "+ strSearchedItem1Combo);
		System.out.println("searchedItem2Combo : "+ strSearchedItem2Combo);
		
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		//메뉴 권한 체크
		if(!MenuAuthCheck(strMNU_CD, request)) {
			mav = actionBoard("001001","noticeBoard","1","","*","*","*","*",request);
			mav.addObject("serverMsg", "메뉴 권한이 없습니다. 메인메뉴로 이동합니다.");
			return mav;
		}
		
		UserConnectionLog(strMNU_CD,request);
		
		
		//Model&View 객체 변수 세팅
		mav.setViewName(strMNU_PATH);
		//사용자정보
		mav.addObject("loginfo", request.getSession().getAttribute("loginfo"));
		mav.addObject("loginfoJSON", getJsonStringFromMap((HashMap<String,Object>)request.getSession().getAttribute("loginfo")));
		//직급 콤보박스 item 목록
		List<HashMap<String,Object>> listUserPOS = SelectDetailCodeList("0080");
		mav.addObject("listUserPOS", listUserPOS);
		mav.addObject("MNUList", request.getSession().getAttribute("USER_MENU"));
		mav.addObject("MNU_CD", strMNU_CD);
		mav.addObject("MNU_PATH", strMNU_PATH);
		//메뉴 댓글 사용 Y,N
		List<HashMap<String,Object>> listMNU = (List<HashMap<String,Object>>)request.getSession().getAttribute("USER_MENU");
		for (HashMap<String, Object> hMap : listMNU) {
			if(hMap.get("MNU_CD").toString().equals(strMNU_CD)) {
				mav.addObject("REPL_REG_YN", hMap.get("REPL_REG_YN").toString());
				break;
			}
		}
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		
		//사업소 콤보박스 세팅
		//장애관리, 예방점검관리일떄 
		if(strMNU_PATH.equals("FaultBoard") || strMNU_PATH.equals("PreventionBoard")) {
			//관리자일 경우
			if(((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("AUTH_SETUP").equals("A")) {
				map.put("USER_ID", "*");
			}else {
				map.put("USER_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));	
			}
			//2021-01-14
			//HashMap<String,String> 에서  HashMap<String,Object> 으로 바꿈.. 문제있을시 확인 
			List<HashMap<String,Object>> listSite = mainService.Select_Site_By_User(map);
			mav.addObject("SiteList", listSite);
		}else {
			map.put("USER_ID", "*");
		}
		//사업소 콤보박스 세팅 끝
		
		//구분 콤보박스 세팅
		String strItemCode = ChoiceGBNCodeByMenuPath(strMNU_PATH);
		if(strItemCode.equals("")) {
			System.out.println("strItemCode : 구분코드 코딩 안됨");	
		}else {
			System.out.println("strItemCode : " +strItemCode);
		}
		
		List<HashMap<String,Object>> list_1 = SelectDetailCodeList(strItemCode);
		mav.addObject("ListItem_1", list_1);
		//구분 콤보박스 세팅 끝
		
		//페이징
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(Integer.parseInt(strSearchedPageNo)); // 현재 페이지 번호
		paginationInfo.setRecordCountPerPage(15); // 한 페이지당 게시되는 게시물 건 수
		paginationInfo.setPageSize(10); // 페이지 리스트에 게시되는 페이지 건 수
		
		map.put("BIZ_SEQ", strSearchedBizSeq);
		map.put("MNU_CD", strMNU_CD);
		map.put("ITEM_1_MST", strItemCode);

		if(strSearchedWord.trim().equals("")) {
			map.put("BASIC_COMBO", "*");
		}else{
			map.put("BASIC_COMBO", strSearchedBasicCombo);
		}
		map.put("SEARCH_WORD", strSearchedWord);
		
		int intStart = paginationInfo.getFirstRecordIndex() + 1;
		int intEnd = intStart + paginationInfo.getRecordCountPerPage() - 1;
		System.out.println("intStart : " + intStart);
		System.out.println("intEnd : " + intEnd);
		
		
		map.put("START", intStart);
		map.put("END", intEnd);
		map.put("ITEM_1", strSearchedItem1Combo);
		map.put("ITEM_2", strSearchedItem2Combo);

		//게시판 목록 데이터 세팅
		List<HashMap<String,Object>> listData;
		JSONArray jsonArray = new JSONArray();
		
		switch (strMNU_PATH) {
		case "FAQBoard":
			listData = mainService.Select_FAQ_MNG(map);
			break;
			
		case "adminSiteMng":
			listData = mainService.Select_SITE_MNG(map);
			
	        //사이트 데이터(JSON)
	        for( Map<String, Object> hMap : listData ) {
	            jsonArray.add(getJsonStringFromMap(hMap));
	        }
	        mav.addObject("jsonArray", jsonArray);
	        break;
	        
		case "adminUserMng":
			map.put("AUTH_SETUP", strSearchedItem1Combo);
			listData = mainService.Select_USER_MNG(map);
			
			//사이트 데이터
			map.put("USER_ID", strSearchedWord);
			List siteList = mainService.Select_User_Mapping_Site(map);
			
			//사용자 데이터(JSON)
	        for( Map<String, Object> hMap : listData ) {
	            jsonArray.add(getJsonStringFromMap(hMap));
	        }
	        mav.addObject("jsonArray", jsonArray);
	        
			mav.addObject("siteList", siteList);
			break;
			
		case "adminCodeMng":
			//상위코드 데이터
			listData = mainService.Select_MST_CD(null);
			
			//상위 코드(JSON)
	        for( Map<String, Object> hMap : listData ) {
	            jsonArray.add(getJsonStringFromMap(hMap));
	        }
	        mav.addObject("jsonArray_MST", jsonArray);
	        
	        //하위 코드 데이터
			map.put("MST_CD", strSearchedWord);
			List<HashMap<String,Object>> data2 = mainService.Select_DTL_CD(map);
			
			mav.addObject("data2", data2);
			
			//하위 코드(JSON)
			JSONArray jsonArray_DTL = new JSONArray();
	        for( Map<String, Object> hMap : data2 ) {
	            jsonArray_DTL.add(getJsonStringFromMap(hMap));
	        }
	        mav.addObject("jsonArray_DTL", jsonArray_DTL);
	        
			break;
			
		case "VideoBoard":
		case "FileBoard":
			//동영상 자료 메뉴 2번째 콤보박스 전체일때
			if(strSearchedItem2Combo.equals("*_*")) {
				map.put("ITEM_2", strSearchedItem2Combo);
			}else{
				String[] aryItem = strSearchedItem2Combo.split("_");
				if(aryItem.length == 2) {
					map.put("ITEM_1", aryItem[0]);
					map.put("ITEM_2", aryItem[1]);
				}
			}

		default:
			listData = mainService.Select_BBS_MST_MNG(map);
			break;
		}
		mav.addObject("data", listData);
		//게시판 목록 데이터 세팅 끝
		
		if (listData.size() == 0) {
			//마지막 페이지의 하나의 게시물이 있을경우 삭제되면 그 전 페이지번호로 이동 
			//페이지  1이 아닐때 쿼리 결과가 없으면 페이지 번호-1 해서 리다이렉트
			if(!strSearchedPageNo.equals("1")) {
				mav = actionBoard(strMNU_CD, strMNU_PATH, String.valueOf(Integer.parseInt(strSearchedPageNo)-1), strSearchedWord, strSearchedBasicCombo, strSearchedBizSeq, strSearchedItem1Combo, strSearchedItem2Combo, request);
				return mav;
			}
		}else {
			Object objCNT = ((HashMap<String,Object>)(listData.get(0))).get("TOTAL_CNT");
			if(objCNT != null) {
				int intTotalCNT = Integer.parseInt(objCNT.toString());
				paginationInfo.setTotalRecordCount(intTotalCNT);
			}
		}
		mav.addObject(paginationInfo);
		
		//검색조건 저장
		HashMap<String,Object> searchedCondition = new HashMap<>();
		searchedCondition.put("searchedPageNo", strSearchedPageNo);
		System.out.println(strSearchedWord);
		if(strSearchedWord.equals("\\")){
			searchedCondition.put("searchedWord","＼");
		}else {
			searchedCondition.put("searchedWord",strSearchedWord);
		}
		searchedCondition.put("searchedBasicCombo", strSearchedBasicCombo);
		searchedCondition.put("searchedBizSeq", strSearchedBizSeq);
		searchedCondition.put("searchedItem1Combo", strSearchedItem1Combo);
		searchedCondition.put("searchedItem2Combo", strSearchedItem2Combo);
		
		mav.addObject("searchedCondition", searchedCondition);
		mav.addObject("json_SC", getJsonStringFromMap(searchedCondition));
		
		return mav;
	}
	
	
	@RequestMapping(value="/ComboChanged.do")
	public ModelAndView ComboChanged(@RequestParam("MNU_PATH") String MNU_PATH,
									 @RequestParam("DTL_CD") String DTL_CD) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		if(DTL_CD.equals("*")) {
			String strItemCode = ChoiceGBNCodeByMenuPath(MNU_PATH);
			
			List<HashMap<String,Object>> listAllGBN = new ArrayList<>();
			
			List<HashMap<String,Object>> list_1 = SelectDetailCodeList(strItemCode);
			
			for (HashMap<String,Object> hMap : list_1) {
				List<HashMap<String,Object>> list_1_sub = SelectDetailCodeList(hMap.get("DTL_CD").toString());
				listAllGBN.addAll(list_1_sub);
			}
			System.out.println("listAllGBN : " + listAllGBN);
			
			mav.addObject("combo2Items", listAllGBN);

		}else {
			List list = SelectDetailCodeList(DTL_CD);
			
			mav.addObject("combo2Items", list);
		}

		return mav;
	}
	
	@RequestMapping(value="/boardView.do", method=RequestMethod.POST)
	protected ModelAndView boardView(@RequestParam("MNU_CD") String MNU_CD,
								     @RequestParam("MNU_PATH") String MNU_PATH,
								     @RequestParam("searchedBBSSeq") String searchedBBSSeq,
								     @RequestParam(value="modifyReplySeq", required=false, defaultValue="") String modifyReplySeq,
								     @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
								     @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									 @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									 @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									 @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
								     HttpServletRequest request) throws Exception {
		
		//파일다운로드 이후 파라미터가 두 번 담긴 @,@,@ 형태로 넘와서 처리
		String strMNU_CD = CommaRemoveLastData(MNU_CD);
		String strMNU_PATH = CommaRemoveLastData(MNU_PATH);
		String strSearchedBBSSeq = CommaRemoveLastData(searchedBBSSeq);
		String strModifyReplySeq = CommaRemoveLastData(modifyReplySeq);
		String strSearchedPageNo = CommaRemoveLastData(searchedPageNo);
		String strSearchedWord = CommaRemoveLastData(searchedWord);
		String strSearchedBasicCombo = CommaRemoveLastData(searchedBasicCombo);
		String strSearchedBizSeq = CommaRemoveLastData(searchedBizSeq);
		String strSearchedItem1Combo = CommaRemoveLastData(searchedItem1Combo);
		
		System.out.println("********** 게시판 글보기 **********");
		System.out.println("MNU_CD : " + strMNU_CD);
		System.out.println("MNU_PATH : " + strMNU_PATH);
		System.out.println("modifyReplySeq : " + strModifyReplySeq);
		System.out.println("searchedBBSSeq : " + strSearchedBBSSeq);
		System.out.println("searchedPageNo : " + strSearchedPageNo);
		System.out.println("searchedWord : " + strSearchedWord);
		System.out.println("searchedBasicCombo : " + strSearchedBasicCombo);
		System.out.println("searchedBizSeq : " + strSearchedBizSeq);
		System.out.println("searchedItem1Combo : " + strSearchedItem1Combo);
		
		//세션에 게시글 SEQ 담음 
		((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("BBS_SEQ", strSearchedBBSSeq);
		if(strModifyReplySeq.equals("")) {
			((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("REPL_SEQ", "-1");
		}else {
			((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("REPL_SEQ", strModifyReplySeq);
		}
		
		String strITEM_1_MST = null;
		String strITEM_2_MST = null;
		String strGBN = ChoiceGBNCodeByMenuPath(strMNU_PATH);
		
		if(strGBN.contains("_")) {
			String[] aryGBN = strGBN.split("_");
			strITEM_1_MST = aryGBN[0];
			strITEM_2_MST = aryGBN[1];
		}else {
			strITEM_1_MST = strGBN;
		}
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName(strMNU_PATH+"View");
		mav.addObject("loginfo", request.getSession().getAttribute("loginfo"));
		mav.addObject("loginfoJSON", getJsonStringFromMap((HashMap<String,Object>)request.getSession().getAttribute("loginfo")));
		//직급 콤보박스 item 목록
		List<HashMap<String,Object>> listUserPOS = SelectDetailCodeList("0080");
		mav.addObject("listUserPOS", listUserPOS);
		mav.addObject("MNUList", request.getSession().getAttribute("USER_MENU"));
		mav.addObject("MNU_CD", strMNU_CD);
		mav.addObject("MNU_PATH", strMNU_PATH);
		//메뉴 댓글 사용 Y,N
		List<HashMap<String,Object>> listMNU = (List<HashMap<String,Object>>)request.getSession().getAttribute("USER_MENU");
		for (HashMap<String, Object> hMap : listMNU) {
			if(hMap.get("MNU_CD").toString().equals(strMNU_CD)) {
				mav.addObject("REPL_REG_YN", hMap.get("REPL_REG_YN").toString());
				break;
			}
		}
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("BBS_SEQ", strSearchedBBSSeq);
		map.put("ITEM_1_MST", strITEM_1_MST);
		map.put("ITEM_2_MST", strITEM_2_MST);
		
		
		//FAQ일때 
		if(strMNU_PATH.equals("FAQBoard")) {
			//조회 수 ++
			mainService.Update_FAQ_MNG_CNT(map);
			List listFAQ_View = mainService.Select_FAQ_MNG_View(map);
			
			if(listFAQ_View.size()==1) {
				System.out.println(listFAQ_View);
				HashMap<String,Object> mapData = (HashMap<String,Object>)listFAQ_View.get(0);
				mapData.put("QUESTION", (mapData.get("QUESTION") != null)? StringEscapeUtils.unescapeHtml3(mapData.get("QUESTION").toString()) : "");
				mapData.put("REPL", (mapData.get("REPL") != null)? StringEscapeUtils.unescapeHtml3(mapData.get("REPL").toString()) : "");
				
/*				mapData.put("QUESTION", (mapData.get("QUESTION") != null)? mapData.get("QUESTION").toString().replace("\r\n","<br>") : "");
				mapData.put("REPL", (mapData.get("REPL") != null)? mapData.get("REPL").toString().replace("\r\n","<br>") : "");*/
				mav.addObject("data", mapData);
			}
		}else {
			//조회 수 ++
			mainService.Update_BBS_MST_CNT(map);
			List listBBS_View = mainService.Select_BBS_MST_MNG_View(map);
			
			if(listBBS_View.size()==1) {
				System.out.println(listBBS_View);
				HashMap<String,Object> mapData = (HashMap<String,Object>)listBBS_View.get(0);
				mapData.put("CONTENTS", (mapData.get("CONTENTS") != null)? StringEscapeUtils.unescapeHtml3(mapData.get("CONTENTS").toString()) : "");
				//mapData.put("CONTENTS", (mapData.get("CONTENTS") != null)? mapData.get("CONTENTS").toString().replace("\r\n","<br>") : "");
				mav.addObject("data", mapData);
			}
			
			//댓글
			List<HashMap<String,Object>> listREPL = mainService.Select_BBS_REPL_MNG(map);
			
			for (HashMap<String, Object> mapREPL : listREPL) {
				//수정 대상인 댓글은 replace 제외
				if(!mapREPL.get("REPL_SEQ").toString().equals(strModifyReplySeq)){
					mapREPL.put("CONTENTS", (mapREPL.get("CONTENTS") != null)? mapREPL.get("CONTENTS").toString().replace("\r\n","<br>") : "");	
				}
			}
			mav.addObject("reply",listREPL);
			
			//해당 게시글의 댓글 파일첨부 전체 리스트
			map.put("REPL_SEQ","*");
			List<HashMap<String,Object>> listAllReplyFiles = mainService.Select_REPL_File(map);
			mav.addObject("replyFiles",listAllReplyFiles);
			

			map.put("REPL_SEQ", strModifyReplySeq);
			List<HashMap<String,Object>> listSavedReplyFile = mainService.Select_REPL_File(map);
			
			JSONArray jsonArray = new JSONArray();
			
	        for( Map<String, Object> hMap : listSavedReplyFile ) {
	            jsonArray.add(getJsonStringFromMap(hMap));
	        }
	        mav.addObject("jsonSavedFiles", jsonArray);
		        
		        
			//게시글 첨부 파일
			map.put("GBN", "BBS");
			List<HashMap<String,Object>> listFile = mainService.Select_File_By_SEQ(map);
			mav.addObject("files",listFile);
		}
		
		HashMap<String,Object> searchedCondition = new HashMap<>();
		searchedCondition.put("modifyReplySeq", strModifyReplySeq); //수정 모드일때 들어오는 댓글 SEQ
		searchedCondition.put("searchedBBSSeq", strSearchedBBSSeq);
		searchedCondition.put("searchedPageNo", strSearchedPageNo);
		searchedCondition.put("searchedWord", strSearchedWord.trim());
		searchedCondition.put("searchedBasicCombo", strSearchedBasicCombo);
		searchedCondition.put("searchedBizSeq", strSearchedBizSeq);
		searchedCondition.put("searchedItem1Combo", strSearchedItem1Combo);
		
		mav.addObject("searchedCondition", searchedCondition);
		
		return mav;
	}
	
	@RequestMapping(value="/boardEdit.do", method=RequestMethod.POST)
	protected ModelAndView boardEdit(@RequestParam("MNU_CD") String MNU_CD,
										   @RequestParam("MNU_PATH") String MNU_PATH,
										   @RequestParam("searchedBBSSeq") String searchedBBSSeq,
										   @RequestParam(value="searchedReplySeq", required=false, defaultValue="") String searchedReplySeq,
										   @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
								     	   @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
										   @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
										   @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
										   @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
										   HttpServletRequest request) throws Exception{
		
		String strMNU_CD = CommaRemoveLastData(MNU_CD);
		String strMNU_PATH = CommaRemoveLastData(MNU_PATH);
		String strSearchedBBSSeq = CommaRemoveLastData(searchedBBSSeq);
		String strSearchedPageNo = CommaRemoveLastData(searchedPageNo);
		String strSearchedWord = CommaRemoveLastData(searchedWord);
		String strSearchedBasicCombo = CommaRemoveLastData(searchedBasicCombo);
		String strSearchedBizSeq = CommaRemoveLastData(searchedBizSeq);
		String strSearchedItem1Combo = CommaRemoveLastData(searchedItem1Combo);
		
		System.out.println("********** 게시판 등록&수정 **********");
		System.out.println("MNU_CD : " + strMNU_CD);
		System.out.println("MNU_PATH : " + strMNU_PATH);
		System.out.println("searchedBBSSeq : " + strSearchedBBSSeq);
		System.out.println("searchedPageNo : " + strSearchedPageNo);
		System.out.println("searchedWord : " + strSearchedWord);
		System.out.println("searchedBasicCombo : " + strSearchedBasicCombo);
		System.out.println("searchedBizSeq : " + strSearchedBizSeq);
		System.out.println("searchedItem1Combo : " + strSearchedItem1Combo);
		
		//세션 - loginfo에 게시글 SEQ 담음 
		((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("BBS_SEQ", strSearchedBBSSeq);
		((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("REPL_SEQ", "");
		
		String strItemCode = ChoiceGBNCodeByMenuPath(strMNU_PATH);
		
		System.out.println("strITEM_1_MST : " + strItemCode);
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.setViewName(strMNU_PATH+"Edit");
		mav.addObject("loginfo", request.getSession().getAttribute("loginfo"));
		mav.addObject("loginfoJSON", getJsonStringFromMap((HashMap<String,Object>)request.getSession().getAttribute("loginfo")));
		//직급 콤보박스 item 목록
		List<HashMap<String,Object>> listUserPOS = SelectDetailCodeList("0080");
		mav.addObject("listUserPOS", listUserPOS);
		mav.addObject("MNUList", request.getSession().getAttribute("USER_MENU"));
		mav.addObject("MNU_CD", strMNU_CD);
		mav.addObject("MNU_PATH", strMNU_PATH);
		mav.addObject("ITEM_1_MST", strItemCode);
		
		
		//콤보박스 바인딩 데이터
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("MST_CD", strItemCode);
		List<HashMap<String,Object>> listITEM_1 = mainService.Select_CD_INFO(map);
		mav.addObject("ListItem_1", listITEM_1);
		
		
		//게시글 수정 일때
		if(!strSearchedBBSSeq.equals("-1")) {
			map.put("BBS_SEQ", strSearchedBBSSeq);
			
			//FAQ일때 
			if(strMNU_PATH.equals("FAQBoard")) {
				List<HashMap<String,Object>> listFAQ_View = mainService.Select_FAQ_MNG_View(map);
				
				if(listFAQ_View.size()==1) {
					System.out.println("********** FAQ 등록&수정, 게시글 가져오기 성공 **********");
					mav.addObject("data", listFAQ_View.get(0));
				}
			}else {
				List<HashMap<String,Object>> listBBS_View = mainService.Select_BBS_MST_MNG_View(map);

				if(listBBS_View.size()==1) {
					System.out.println("********** 게시판 등록&수정, 게시글 가져오기 성공 **********");
					mav.addObject("data", listBBS_View.get(0));
					
					JSONArray jsonArray = new JSONArray();
					jsonArray.add(getJsonStringFromMap(listBBS_View.get(0)));
					
			        mav.addObject("jsonArray", jsonArray);
					
				}
			}
		}else{
			mav.addObject("jsonArray", "[]");
		}//end 게시글 수정일때

		//기존 첨부파일 목록
		map.put("GBN", "BBS");
		List<HashMap<String,Object>> listFile = mainService.Select_File_By_SEQ(map);
		
		JSONArray jsonArray = new JSONArray();
		
        for( Map<String, Object> hMap : listFile ) {
            jsonArray.add(getJsonStringFromMap(hMap));
        }
        mav.addObject("jsonSavedFiles", jsonArray);
		
        
		//장애관리, 예방점검관리 일 경우 사업소 콤보박스 바인딩 리스트 
		if(strMNU_PATH.equals("FaultBoard") || strMNU_PATH.equals("PreventionBoard")) {
			//관리자일 경우
			if(((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("AUTH_SETUP").equals("A")) {
				map.put("USER_ID", "*");
			}else {
				map.put("USER_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));	
			}
			
			List listSite_Fault = mainService.Select_Site_By_User(map);
			mav.addObject("SiteList", listSite_Fault);
		}
		
		HashMap<String,Object> searchedCondition = new HashMap<>();
		searchedCondition.put("searchedBBSSeq", strSearchedBBSSeq);
		searchedCondition.put("searchedPageNo", strSearchedPageNo);
		searchedCondition.put("searchedWord", strSearchedWord.trim());
		searchedCondition.put("searchedBasicCombo", strSearchedBasicCombo);
		searchedCondition.put("searchedBizSeq", strSearchedBizSeq);
		searchedCondition.put("searchedItem1Combo", strSearchedItem1Combo);
		
		mav.addObject("searchedCondition", searchedCondition);
		
		return mav;
	}
	
	@RequestMapping(value="/boardSave.do", method=RequestMethod.POST)
	protected ModelAndView boardSave(@RequestParam("MNU_CD") String MNU_CD,
									 @RequestParam("MNU_PATH") String MNU_PATH,
									 @RequestParam("searchedBBSSeq") String searchedBBSSeq,
									 @RequestParam(value="BIZ_SEQ", required=false, defaultValue="") String BIZ_SEQ,
									 @RequestParam("TTL") String TTL,
									 @RequestParam("ITEM_1_DTL") String ITEM_1_DTL, 						  //FAQ일때 구분(CAT) 데이터
									 @RequestParam(value="ITEM_2_DTL", required=false, defaultValue="") String ITEM_2_DTL,
									 @RequestParam("CONTENTS") String CONTENTS, 							  //FAQ일때 질문(QUESTION) 데이터
									 @RequestParam(value="ETC", required=false, defaultValue="") String ETC, //FAQ일때 답변(REPL) 데이터
									 @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
							     	 @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
							     	 @RequestParam(value="searchedReplySeq", required=false, defaultValue="") String searchedReplySeq,
									 @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									 @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									 @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
									 @RequestParam(value="searchedItem2Combo", required=false, defaultValue="*_*") String searchedItem2Combo,
									 @RequestParam(value="removeFileSEQ", required=false, defaultValue="") String removeFileSEQ,
									 HttpServletRequest request) throws Exception{
		
		System.out.println("********** 게시글 저장 **********");
		System.out.println("BBS_SEQ = " + searchedBBSSeq);
		System.out.println("ITEM_1_DTL = " + ITEM_1_DTL);
		System.out.println("ITEM_2_DTL = " + ITEM_2_DTL);
		System.out.println("BIZ_SEQ = " + BIZ_SEQ);
		System.out.println("*****************************");
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("BBS_SEQ", searchedBBSSeq); //신규 = -1로 처리
		
		if(BIZ_SEQ.equals("undefined")) {
			map.put("BIZ_SEQ", "");	
		}else {
			map.put("BIZ_SEQ", BIZ_SEQ);
		}
		map.put("MNU_CD", MNU_CD);
		map.put("ITEM_1_DTL", ITEM_1_DTL);
		map.put("ITEM_2_DTL", ITEM_2_DTL);
		map.put("TTL", TTL);
		map.put("CONTENTS", CONTENTS);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		int intSelectKey_BBS_SEQ;
		if(MNU_PATH.equals("FAQBoard")) {
			map.put("CAT", ITEM_1_DTL);
			map.put("QUESTION", CONTENTS);
			map.put("REPL", ETC);

			intSelectKey_BBS_SEQ = mainService.Merge_FAQ_MNG(map);
		}else {
			intSelectKey_BBS_SEQ = mainService.Merge_BBS_MST_MNG(map);
		}
		
		String strBBS_SEQ;
		//신규일때 
		if(searchedBBSSeq.equals("-1")) {
			strBBS_SEQ = Integer.toString(intSelectKey_BBS_SEQ);
		}else {
			strBBS_SEQ = searchedBBSSeq;
		}
		
		mav.addObject("savedBBS_SEQ", strBBS_SEQ );
		
		//첨부파일 삭제
		removeFile(removeFileSEQ);

		if(MNU_PATH.equals("VideoBoard")) {
			mav = actionBoard(MNU_CD, MNU_PATH, searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, searchedItem2Combo, request);
		}else {			
			mav = boardView(MNU_CD, MNU_PATH, strBBS_SEQ, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
		}

		return mav;
	}
	
	@RequestMapping(value="/boardDelete.do", method=RequestMethod.POST)
	protected ModelAndView actionDelete(@RequestParam("MNU_CD") String MNU_CD,
										@RequestParam("MNU_PATH") String MNU_PATH,
										@RequestParam("searchedBBSSeq") String searchedBBSSeq,
										@RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
										@RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
										@RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
										@RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
										@RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
										HttpServletRequest request) throws Exception{
		
		//파일다운로드 이후 파라미터들이 @,@,@ 형태로 넘와서 처리
		String strSearchedBBSSeq = CommaRemoveLastData(searchedBBSSeq);
		
		System.out.println("********** 게시글 삭제 **********");
		System.out.println("BBS_SEQ = " + strSearchedBBSSeq);
		System.out.println("*****************************");
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("BBS_SEQ", strSearchedBBSSeq);
		if(MNU_PATH.equals("FAQBoard")) {
			mainService.Delete_FAQ_MNG(map);
		}else {
			mainService.Delete_BBS_MST_MNG(map);	
		}
		
		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	
	@RequestMapping(value="/replyDelete.do", method=RequestMethod.POST)
	protected ModelAndView replyDelete(@RequestParam("MNU_CD") String MNU_CD,
									   @RequestParam("MNU_PATH") String MNU_PATH,
									   @RequestParam("modifyReplySeq") String modifyReplySeq,
									   @RequestParam("searchedBBSSeq") String searchedBBSSeq,
									   @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
									   @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									   @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									   @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									   @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
									   HttpServletRequest request) throws Exception{
		
		System.out.println("********** 댓글 삭제 **********");
		System.out.println("modifyReplySeq = " + modifyReplySeq);
		System.out.println("*****************************");
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("REPL_SEQ", modifyReplySeq);
		
		mainService.Delete_BBS_REPL_MNG(map);
		
		mav = boardView(MNU_CD, MNU_PATH, searchedBBSSeq, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
		//mav.setViewName("forward:/boardView.do");
		
		return mav;
	}
	
	@RequestMapping(value="/replySave.do", method=RequestMethod.POST)
	protected ModelAndView replySave(@RequestParam("MNU_CD") String MNU_CD,
									 @RequestParam("MNU_PATH") String MNU_PATH,
									 @RequestParam("modifyReplySeq") String modifyReplySeq,
									 @RequestParam("CONTENTS") String CONTENTS,
									 @RequestParam("searchedBBSSeq") String searchedBBSSeq,
									 @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
									 @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									 @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									 @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									 @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
									 @RequestParam(value="removeFileSEQ", required=false, defaultValue="") String removeFileSEQ,
									 HttpServletRequest request) throws Exception{
		
		System.out.println("********** 댓글 저장 **********");
		System.out.println("modifyReplySeq = " + modifyReplySeq);
		System.out.println("searchedBBSSeq = " + searchedBBSSeq);
		System.out.println("CONTENTS = " + CONTENTS);
		System.out.println("searchedPageNo = " + searchedPageNo);
		System.out.println("searchedWord = " + searchedWord);
		System.out.println("searchedBasicCombo = " + searchedBasicCombo);
		System.out.println("searchedBizSeq = " + searchedBizSeq);
		System.out.println("searchedItem1Combo = " + searchedItem1Combo);
		
		System.out.println("***************************");
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("BBS_SEQ", searchedBBSSeq);
		map.put("REPL_SEQ", modifyReplySeq);
		map.put("CONTENTS", CONTENTS);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		mainService.Merge_BBS_REPL_MNG(map);
		
		//첨부파일 삭제
		removeFile(removeFileSEQ);
		
		mav = boardView(MNU_CD, MNU_PATH, searchedBBSSeq, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
		
		return mav;
	}
	
	@RequestMapping(value="/siteSave.do", method=RequestMethod.POST)
	protected ModelAndView siteSave(@RequestParam("MNU_CD") String MNU_CD,
									@RequestParam("MNU_PATH") String MNU_PATH,
									@RequestParam("BIZ_SEQ") String BIZ_SEQ,
									@RequestParam("BIZ_NM") String BIZ_NM,
									@RequestParam("BIZ_NM_NICK") String BIZ_NM_NICK,
									@RequestParam("BIZ_START_DT") String BIZ_START_DT,
									@RequestParam("BIZ_END_DT") String BIZ_END_DT,
									@RequestParam("MATNC_END_DT") String MATNC_END_DT,
									@RequestParam("ORDG_COMP") String ORDG_COMP,
									@RequestParam("CONT_NM") String CONT_NM,
									@RequestParam("CONT_TEL") String CONT_TEL,
									@RequestParam("BIZ_ADMIN") String BIZ_ADMIN,
									@RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
									HttpServletRequest request) throws Exception{
		System.out.println("********** 사이트저장 **********");
		System.out.println("BIZ_SEQ = " + BIZ_SEQ);
		System.out.println("BIZ_NM = " + BIZ_NM);
		System.out.println("BIZ_NM_NICK = " + BIZ_NM_NICK);
		System.out.println("BIZ_START_DT = " + BIZ_START_DT);
		System.out.println("BIZ_END_DT = " + BIZ_END_DT);
		System.out.println("MATNC_END_DT = " + MATNC_END_DT);
		System.out.println("MATNC_END_DT = " + MATNC_END_DT);
		System.out.println("ORDG_COMP = " + ORDG_COMP);
		System.out.println("ORDG_COMP = " + ORDG_COMP);
		System.out.println("CONT_NM = " + CONT_NM);
		System.out.println("CONT_TEL = " + CONT_TEL);
		System.out.println("BIZ_ADMIN = " + BIZ_ADMIN);
		System.out.println("***************************");
		
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		if(BIZ_SEQ.equals("undefined")) {
			map.put("BIZ_SEQ", "");	
		}else {
			map.put("BIZ_SEQ", BIZ_SEQ);
		}
		map.put("BIZ_NM", BIZ_NM);
		map.put("BIZ_NM_NICK", BIZ_NM_NICK);
		map.put("BIZ_START_DT", BIZ_START_DT.replace("-", ""));
		map.put("BIZ_END_DT", BIZ_END_DT.replace("-", ""));
		map.put("MATNC_END_DT", MATNC_END_DT.replace("-", ""));
		map.put("ORDG_COMP", ORDG_COMP);
		map.put("CONT_NM", CONT_NM);
		map.put("CONT_TEL", CONT_TEL);
		map.put("BIZ_ADMIN", BIZ_ADMIN);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		mainService.Merge_SITE_MNG(map);
		
		mav = actionBoard(MNU_CD, MNU_PATH, searchedPageNo, "", "", "", "", "", request);
		return mav;
	}
	
	@RequestMapping(value="/siteDelete.do", method=RequestMethod.POST)
	protected ModelAndView siteDelete(@RequestParam("MNU_CD") String MNU_CD,
									  @RequestParam("MNU_PATH") String MNU_PATH,
									  @RequestParam("BIZ_SEQ") String BIZ_SEQ,
									  @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
									  HttpServletRequest request) throws Exception{
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("BIZ_SEQ", BIZ_SEQ);
		
		mainService.Delete_SITE_MNG(map);
		
		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	@RequestMapping(value="/userSiteSave.do", method=RequestMethod.POST)
	protected ModelAndView userSiteSave(@RequestParam("MNU_CD") String MNU_CD,
									    @RequestParam("MNU_PATH") String MNU_PATH,
									    @RequestParam(value="BIZ_SEQ", required=false, defaultValue="") String BIZ_SEQ,
									    @RequestParam("searchedWord") String searchedWord,
									    HttpServletRequest request) throws Exception{
		
		System.out.println("********** 매핑저장 **********");
		System.out.println("BIZ_SEQ = " + BIZ_SEQ);
		System.out.println("USER_ID = " + searchedWord);
		System.out.println("***************************");
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", searchedWord);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		mainService.Delete_SITE_USER_MAP(map);
		
		if(!BIZ_SEQ.equals("")) {
			if(BIZ_SEQ.contains(",")) {
				String[] listBIZ = BIZ_SEQ.split(",");
				
				for (String strBIZ_SEQ : listBIZ) {
					map.put("BIZ_SEQ", strBIZ_SEQ);
					mainService.Insert_SITE_USER_MAP(map);
				}
			}else {
				map.put("BIZ_SEQ", BIZ_SEQ);
				mainService.Insert_SITE_USER_MAP(map);
			}
		}

		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	@RequestMapping(value="/userMerge.do", method=RequestMethod.POST)
	protected ModelAndView userMerge(@RequestParam("MNU_CD") String MNU_CD,
									 @RequestParam("MNU_PATH") String MNU_PATH,
									 @RequestParam("MNU_KIND") String MNU_KIND,
									 @RequestParam("USER_ID") String USER_ID,
									 @RequestParam("USER_NM") String USER_NM,
									 @RequestParam("USER_PWD") String USER_PWD,
									 @RequestParam("USER_POS") String USER_POS,
									 @RequestParam("COMP_NM") String COMP_NM,
									 @RequestParam("TEL") String TEL,
									 @RequestParam("EMAIL") String EMAIL,
									 @RequestParam("SMS_RCV_YN") String SMS_RCV_YN,
									 @RequestParam("AUTH_SETUP") String AUTH_SETUP,
									 @RequestParam("USE_YN") String USE_YN,
								     @RequestParam("searchedBBSSeq") String searchedBBSSeq,
								     @RequestParam(value="modifyReplySeq", required=false, defaultValue="") String modifyReplySeq,
								     @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
								     @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									 @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									 @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									 @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
									 @RequestParam(value="searchedItem2Combo", required=false, defaultValue="*_*") String searchedItem2Combo,
									 HttpServletRequest request) throws Exception{
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", USER_ID);
		map.put("USER_NM", USER_NM);
		if(USER_PWD.equals("")) {
			map.put("USER_PWD", "noChange");	
		}else {
			map.put("USER_PWD", sha256(USER_PWD));
		}
		map.put("USER_POS", USER_POS);
		map.put("COMP_NM", COMP_NM);
		map.put("TEL", TEL);
		map.put("EMAIL", EMAIL);
		map.put("SMS_RCV_YN", SMS_RCV_YN);
		map.put("AUTH_SETUP", AUTH_SETUP);
		map.put("USE_YN", USE_YN);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		mainService.Merge_USER_MNG(map);
		
		//기존 사용자의 정보가 업데이트되었을경우
		if(USER_ID.equals(((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"))) {
			//세션에 유저정보 및 유저메뉴 바인딩
			SessionUserInfoRebinding(map, request);	
		}
		
		switch (MNU_KIND) {
		case "board":
			mav = actionBoard(MNU_CD, MNU_PATH, searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, searchedItem2Combo, request);
			break;
			
		case "view":
			mav = boardView(MNU_CD, MNU_PATH, searchedBBSSeq, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
			break;
			
		case "edit":
			mav = boardEdit(MNU_CD, MNU_PATH, searchedBBSSeq, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
			break;	
		}

		return mav;
	}
	
	@RequestMapping(value="/userDelete.do", method=RequestMethod.POST)
	protected ModelAndView userDelete(@RequestParam("MNU_CD") String MNU_CD,
									  @RequestParam("MNU_PATH") String MNU_PATH,
									  @RequestParam("USER_ID") String USER_ID,
									  HttpServletRequest request) throws Exception{
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("USER_ID", USER_ID);
		System.out.println("USER_ID = " + USER_ID);
		
		mainService.Delete_USER_MNG(map);
		
		//mainService.Delete_SITE_USER_MAP(map);
		
		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	
	@RequestMapping(value="/masterCodeMerge.do", method=RequestMethod.POST)
	protected ModelAndView masterCodeMerge(@RequestParam("MNU_CD") String MNU_CD,
										   @RequestParam("MNU_PATH") String MNU_PATH,
										   @RequestParam("MST_CD") String MST_CD,
										   @RequestParam("CD_NM") String CD_NM,
										   @RequestParam("ETC") String ETC,
										   @RequestParam("ORD") String ORD,
										   HttpServletRequest request) throws Exception{
		
		ModelAndView mav = new ModelAndView("jsonView");

		System.out.println("MST_CD = " + MST_CD);
		System.out.println("CD_NM = " + CD_NM);
		System.out.println("ETC = " + ETC);
		System.out.println("ORD = " + ORD);
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("MST_CD", MST_CD);
		map.put("CD_NM", CD_NM);
		map.put("ETC", ETC);
		map.put("ORD", ORD);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		mainService.Merge_MST_INFO(map);
		
		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	@RequestMapping(value="/detailCodeMerge.do", method=RequestMethod.POST)
	protected ModelAndView detailCodeMerge(@RequestParam("MNU_CD") String MNU_CD,
										   @RequestParam("MNU_PATH") String MNU_PATH,
										   @RequestParam("MST_CD") String MST_CD,
										   @RequestParam("DTL_CD") String DTL_CD,
										   @RequestParam("CD_NM") String CD_NM,
										   @RequestParam("ETC") String ETC,
										   @RequestParam("ORD") String ORD,
										   @RequestParam("USE_YN") String USE_YN,
										   HttpServletRequest request) throws Exception{
		
		ModelAndView mav = new ModelAndView("jsonView");

		System.out.println("MST_CD = " + MST_CD);
		System.out.println("DTL_CD = " + DTL_CD);
		System.out.println("CD_NM = " + CD_NM);
		System.out.println("ETC = " + ETC);
		System.out.println("ORD = " + ORD);
		System.out.println("USE_YN = " + USE_YN);
		
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("MST_CD", MST_CD);
		map.put("DTL_CD", DTL_CD);
		map.put("CD_NM", CD_NM);
		map.put("ETC", ETC);
		map.put("ORD", ORD);
		map.put("USE_YN", USE_YN);
		map.put("EDT_ID", ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("USER_ID"));
		
		mainService.Merge_DTL_INFO(map);
		
		mav.setViewName("forward:/board.do");
		
		return mav;
	}
	
	/*
	 * 게시글 파일저장 & 댓글 파일저장
	 */
	@RequestMapping(value="/fileSave.do", method=RequestMethod.POST)
	public ModelAndView fileSave(@RequestParam("uploadFiles") MultipartFile[] uploadFiles,
								 HttpServletRequest request) throws Exception {
		try 
		{
			System.out.println("*********** 파일 저장 ***********");
			String strSessionBBS_SEQ = ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("BBS_SEQ").toString();
			String strSessionREPL_SEQ = ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("REPL_SEQ").toString();
			System.out.println("세션 BBS_SEQ = " + strSessionBBS_SEQ);
			//댓글 파일 저장일때만 값 존재
			System.out.println("세션 REPL_SEQ = " + strSessionREPL_SEQ);

			int SelectKey_BBS_SEQ = -1;
			
			int SelectKey_REPL_SEQ = -1;

			System.out.println("파일 갯수 = " + uploadFiles.length);
			
			for (MultipartFile file : uploadFiles) {
				if (!file.getOriginalFilename().isEmpty()) {
					String strFileFullName = file.getOriginalFilename();
					String strFileName = strFileFullName.substring(0, strFileFullName.lastIndexOf("."));
					String strFileExtension = strFileFullName.substring(strFileFullName.lastIndexOf(".")+1);
					String strFileSizeKB = Integer.toString((int)Math.floor(((double)file.getSize()) / 1024));
					System.out.println("저장 이름 : " + strFileName);
					System.out.println("저장 확장자  : " + strFileExtension);
					System.out.println("파일 용량(KB) : " + strFileSizeKB);
					
					HashMap<String,Object> map = new HashMap<String,Object>();
					map.put("BBS_SEQ", strSessionBBS_SEQ);
					map.put("REPL_SEQ", strSessionREPL_SEQ);
					map.put("FLE_NM", strFileName);
					map.put("FLE_SIZE", strFileSizeKB);
					map.put("FLE_EXT", strFileExtension);
					
					List<HashMap<String,Object>> fileList = null;
					//게시판 파일 저장과 댓글 파일저장 분기
					//게시글 파일저장
					if(strSessionREPL_SEQ.equals("")) {
						System.out.println("*********** 게시글 파일 저장 ***********");
						//게시판 - 파일 매핑 테이블 저장
						SelectKey_BBS_SEQ = mainService.Insert_BBS_FLE_MNG(map);
						System.out.println("SelectKey_BBS_SEQ : " + SelectKey_BBS_SEQ);
						//바로전 저장된 게시판 번호로 파일 목록 조회 
						map.put("BBS_SEQ", SelectKey_BBS_SEQ);
						map.put("GBN", "BBS");
						fileList = mainService.Select_File_By_SEQ(map);
						
					//댓글 파일저장
					}else {
						System.out.println("*********** 댓글 파일 저장 ***********");
						SelectKey_REPL_SEQ = mainService.Insert_BBS_FLE_MNG_REPL(map);
						System.out.println("SelectKey_REPL_SEQ : " + SelectKey_REPL_SEQ);
						//바로전 저장된 댓글 번호로 파일 목록 조회 
						map.put("REPL_SEQ", SelectKey_REPL_SEQ);
						map.put("GBN", "REPL");
						fileList = mainService.Select_File_By_SEQ(map);
					}
					

					//분기 끝
					
					//파일 저장시 파일명에 File SEQ 달면서 저장함
					for (HashMap<String,Object> mapFile : fileList) {
						if(mapFile.get("FLE_NM").toString().equals(strFileName) && mapFile.get("FLE_SIZE").toString().equals(strFileSizeKB)
								&& mapFile.get("FLE_EXT").toString().equals(strFileExtension)) {
							System.out.println("************* 서버 로컬 파일 저장 *************");
							String strFLE_SEQ = mapFile.get("FLE_SEQ").toString();
							
							String savePath = "D:/ppms/uploadFiles/";
							
							File Directory = new File(savePath);
							if(!Directory.exists()) {
								System.out.println("directory생성 : " + savePath);
								Directory.mkdirs();
							}
							
							System.out.println("strFileFullName : " + strFileFullName);
							FileOutputStream fos = new FileOutputStream(savePath + strFLE_SEQ + "_" + strFileFullName);
							InputStream fis = file.getInputStream();
							
							int readCount = 0;
							byte[] buffer = new byte[1024];
							while((readCount = fis.read(buffer)) != -1){
								fos.write(buffer,0,readCount);
							}
						}
					}
				}
			}

			ModelAndView mav = new ModelAndView("jsonView");
			
			//신규 게시글 저장시 파일 저장할때 신규 게시글SEQ 정해짐.
			//SELECTKEY로 리턴받아서 뷰단으로 넘겨주면 이후 게시글 테이블에 저장할때 참조
			if(strSessionBBS_SEQ.equals("-1") && SelectKey_BBS_SEQ != -1 ) {
				System.out.println("신규 게시글 저장, savedBBS_SEQ = " + Integer.toString(SelectKey_BBS_SEQ));
				mav.addObject("savedBBS_SEQ", Integer.toString(SelectKey_BBS_SEQ));
			//신규 아닐때 기존 SEQ
			}else {
				System.out.println("기존 게시글 수정, savedBBS_SEQ = " + strSessionBBS_SEQ);
				mav.addObject("savedBBS_SEQ", strSessionBBS_SEQ);

			}
			
			if(strSessionREPL_SEQ.equals("-1") && SelectKey_REPL_SEQ != -1) {
				System.out.println("신규 댓글 저장, savedREPL_SEQ = " + Integer.toString(SelectKey_REPL_SEQ));
				mav.addObject("savedREPL_SEQ", Integer.toString(SelectKey_REPL_SEQ));
			}else {
				System.out.println("기존 댓글 수정, savedREPL_SEQ = " + strSessionREPL_SEQ);
				mav.addObject("savedREPL_SEQ", strSessionREPL_SEQ);
			}
			
			((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).put("REPL_SEQ","");
			
			return mav;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * 게시글 파일저장 & 댓글 파일저장
	 */
	@RequestMapping(value="/videoFileSave.do", method=RequestMethod.POST)
	public ModelAndView videoFileSave(@RequestParam("uploadFiles") MultipartFile[] uploadFiles,
								 HttpServletRequest request) throws Exception {
		try 
		{
			System.out.println("*********** 동영상 저장 ***********");
			String strSessionBBS_SEQ = ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("BBS_SEQ").toString();
			String strSessionREPL_SEQ = ((HashMap<String,Object>)(request.getSession().getAttribute("loginfo"))).get("REPL_SEQ").toString();
			System.out.println("세션 BBS_SEQ = " + strSessionBBS_SEQ);
			//댓글 파일 저장일때만 값 존재
			//System.out.println("세션 REPL_SEQ = " + strSessionREPL_SEQ);

			int SelectKey_BBS_SEQ = -1;
			
			//int SelectKey_REPL_SEQ = -1;

			System.out.println("파일 갯수 = " + uploadFiles.length);
			
			for (MultipartFile file : uploadFiles) {
				if (!file.getOriginalFilename().isEmpty()) {
					String strFileFullName = file.getOriginalFilename();
					String strFileName = strFileFullName.substring(0, strFileFullName.lastIndexOf("."));
					String strFileExtension = strFileFullName.substring(strFileFullName.lastIndexOf(".")+1);
					String strFileSizeKB = Integer.toString((int)Math.floor(((double)file.getSize()) / 1024));
					System.out.println("저장 이름 : " + strFileName);
					System.out.println("저장 확장자  : " + strFileExtension);
					System.out.println("파일 용량(KB) : " + strFileSizeKB);
					
					HashMap<String,Object> map = new HashMap<String,Object>();
					map.put("BBS_SEQ", strSessionBBS_SEQ);
					map.put("REPL_SEQ", strSessionREPL_SEQ);
					map.put("FLE_NM", strFileName);
					map.put("FLE_SIZE", strFileSizeKB);
					map.put("FLE_EXT", strFileExtension);
					
					//게시판 - 파일 매핑 테이블 저장
					SelectKey_BBS_SEQ = mainService.Insert_BBS_FLE_MNG(map);
					System.out.println("SelectKey_BBS_SEQ : " + SelectKey_BBS_SEQ);
					//바로전 저장된 게시판 번호로 파일 목록 조회 
					map.put("BBS_SEQ", SelectKey_BBS_SEQ);
					map.put("GBN", "BBS");
					List<HashMap<String,Object>> fileList = mainService.Select_File_By_SEQ(map);

					//분기 끝
					
					//파일 저장시 파일명에 File SEQ 달면서 저장함
					for (HashMap<String,Object> mapFile : fileList) {
						if(mapFile.get("FLE_NM").toString().equals(strFileName) && mapFile.get("FLE_SIZE").toString().equals(strFileSizeKB)
								&& mapFile.get("FLE_EXT").toString().equals(strFileExtension)) {
							
							String strFLE_SEQ = mapFile.get("FLE_SEQ").toString();
							
							String savePath = "D:/ppms/uploadFiles/";
							
							File Directory = new File(savePath);
							if(!Directory.exists()) {
								System.out.println("directory생성 : " + savePath);
								Directory.mkdirs();
							}
							
							System.out.println("strFileFullName : " + strFileFullName);
							FileOutputStream fos = new FileOutputStream(savePath + strFLE_SEQ + "_" + strFileFullName);
							InputStream fis = file.getInputStream();
							
							int readCount = 0;
							byte[] buffer = new byte[1024];
							while((readCount = fis.read(buffer)) != -1){
								fos.write(buffer,0,readCount);
							}
						}
					}
				}
			}

			ModelAndView mav = new ModelAndView("jsonView");
			
			//신규 게시글 저장시 파일 저장할때 신규 게시글SEQ 정해짐.
			//SELECTKEY로 리턴받아서 뷰단으로 넘겨주면 이후 게시글 테이블에 저장할때 참조
			if(strSessionBBS_SEQ.equals("-1") && SelectKey_BBS_SEQ != -1 ) {
				System.out.println("신규 게시글 저장, savedBBS_SEQ = " + Integer.toString(SelectKey_BBS_SEQ));
				mav.addObject("savedBBS_SEQ", Integer.toString(SelectKey_BBS_SEQ));
			//신규 아닐때 기존 SEQ
			}else {
				System.out.println("기존 게시글 수정, savedBBS_SEQ = " + strSessionBBS_SEQ);
				mav.addObject("savedBBS_SEQ", strSessionBBS_SEQ);
			}
			
			return mav;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value="/fileDownload.do", method=RequestMethod.POST)
	public ModelAndView fileDownload(@RequestParam("MNU_CD") String MNU_CD,
							     	 @RequestParam("MNU_PATH") String MNU_PATH,
							     	 @RequestParam("searchedBBSSeq") String searchedBBSSeq,
							     	 @RequestParam(value="modifyReplySeq", required=false, defaultValue="") String modifyReplySeq,
							     	 @RequestParam(value="searchedPageNo", required=false, defaultValue="1") String searchedPageNo,
							     	 @RequestParam(value="searchedWord", required=false, defaultValue="") String searchedWord,
									 @RequestParam(value="searchedBasicCombo", required=false, defaultValue="*") String searchedBasicCombo,
									 @RequestParam(value="searchedBizSeq", required=false, defaultValue="*") String searchedBizSeq,
									 @RequestParam(value="searchedItem1Combo", required=false, defaultValue="*") String searchedItem1Combo,
			   						 @RequestParam("FLE_SEQ") String FLE_SEQ,	
							         HttpServletRequest request,
							         HttpServletResponse response) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		boolean fileDown = false;
		
		try 
		{
			String strFLE_SEQ = CommaRemoveLastData(FLE_SEQ);
/*			String strFLE_SEQ = FLE_SEQ;
			if(FLE_SEQ.contains(",")) {
				int lastIndex = FLE_SEQ.lastIndexOf(",")+1;
				strFLE_SEQ = FLE_SEQ.substring(lastIndex);
				System.out.println("strFLE_SEQ : " + strFLE_SEQ);
			}*/
		    //파일 업로드된 서버 경로
		    String savePath = "D:/ppms/uploadFiles/";
		 
		    HashMap<String,Object> map = new HashMap<String,Object>();
		    map.put("GBN", "FLE");
		    map.put("FLE_SEQ", strFLE_SEQ);
		    
		    List<HashMap<String,Object>> listFile = mainService.Select_File_By_SEQ(map);
		    
		    if(listFile.size() == 1) {
		    	String strFILE_NM_EXT = ((HashMap<String,Object>)listFile.get(0)).get("FLE_NM_EXT").toString();
		    	
		    	// 서버에 실제 저장된 파일명
			    String fileName = strFLE_SEQ + "_" +  strFILE_NM_EXT;
			    
			    // 실제 내보낼 파일명
			    String saveFileName = strFILE_NM_EXT;
			    
			    System.out.println("********** 파일다운로드 **********");
			    System.out.println("fileName : " + fileName);
			    System.out.println("saveFileName : " + saveFileName);
			    
			    File file = new File(savePath + fileName);
			    
				if (file.exists() && file.isFile()) {
					response.setContentType("application/octet-stream; charset=utf-8");
					response.setContentLength((int) file.length());
					String browser = getBrowser(request);
					System.out.println("browser = " + browser);
					String disposition = getDisposition(saveFileName, browser);
					
					response.setHeader("Content-Disposition", disposition);
					response.setHeader("Content-Transfer-Encoding", "binary");

					OutputStream out = response.getOutputStream();
					
					FileInputStream fis = null;
					fis = new FileInputStream(file);
					
					FileCopyUtils.copy(fis, out);
					if (fis != null)
						fis.close();
					out.flush();
					out.close();
					
					fileDown = true;
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
		    if(!fileDown) {
		    	mav.addObject("resultMsg", "파일을 찾을 수 없습니다. 관리자에게 문의하세요.");
		    	mav.setViewName("forward:/boardView.do");
		    	//mav = boardView(MNU_CD, MNU_PATH, searchedBBSSeq, "" , searchedPageNo, searchedWord, searchedBasicCombo, searchedBizSeq, searchedItem1Combo, request);
		    	return mav;
		    }else {
		    	return null;	
		    }
		}
	}
	
	private String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1)
			return "MSIE";
		else if (header.indexOf("Chrome") > -1)
			return "Chrome";
		else if (header.indexOf("Opera") > -1)
			return "Opera";
		return "Firefox";
	}

	private String getDisposition(String filename, String browser)throws UnsupportedEncodingException {
		String dispositionPrefix = "attachment;filename=";
		String encodedFilename = null;
		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll(
					"\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\""
					+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\""
					+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		}
		return dispositionPrefix + encodedFilename;
	}
	
	
	@RequestMapping(value="/videoInfo.do")
	public ModelAndView videoInfo(@RequestParam("BBS_SEQ") String BBS_SEQ) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("GBN", "BBS");
		map.put("BBS_SEQ", BBS_SEQ);
		
		List<HashMap<String,Object>> listVideo = mainService.Select_File_By_SEQ(map);
		
		if(listVideo.size() == 1) {
			mav.addObject("videoInfo",listVideo.get(0));
			//조회수 ++
			mainService.Update_BBS_MST_CNT(map);
		}
		

		return mav;
	}
	
	
}
