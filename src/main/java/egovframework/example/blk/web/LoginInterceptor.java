package egovframework.example.blk.web;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	/*
	 * preHandle = 컨트롤러 처리 전 수행
	 * postHandle = 컨트롤러 처리 후 수행
	 */
	@Override // 이부분은 컨트롤러 타기전에
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception {
		System.out.println("********인터셉터 preHandle********");
		if("XMLHttpRequest".equals(request.getHeader("x-requested-with"))){
			System.out.println("ajax 호출. 컨트롤러 실행");
			return true;
		}else {

			if (request.getSession().getAttribute("loginfo") == null) {
				
				// 로그인 페이지로 이동
				response.sendRedirect("SessionOut.do");

				// 컨트롤러를 실행하지 않는다.(요청페이지로 이동하지 않는다)
				return false;

			} else {
				System.out.println("컨트롤러 실행");
				// 컨트롤러를 실행(요청페이지로 이동한다.)
				return true;
			}
		}

		// 헤더 네임,벨류 조회
		/*
		 * Enumeration<String> headerNames = request.getHeaderNames();
		 * while(headerNames.hasMoreElements()) { String headerName =
		 * headerNames.nextElement(); String headerValue =
		 * request.getHeader(headerName); System.out.println("headerName = " +
		 * headerName); System.out.println("headerValue = " + headerValue); }
		 */
	}

	/*@Override // 이건 컨트롤러에서 뷰로 넘어가기 직전
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("**************postHandle**************");
		System.out.println("mavName = " + modelAndView.getViewName());
		System.out.println("loginfo = " + request.getSession().getAttribute("loginfo"));

		if (request.getSession().getAttribute("loginfo") == null) {
				modelAndView.addObject("resultMsg", "세션이 만료되어 로그인 화면으로 이동합니다.");
		}

		
		 * // 세션 객체 생성 HttpSession session = request.getSession();
		 * 
		 * // 세션에 id가 null이면 if(session.getAttribute("loginfo") == null) { // 로그인 페이지로
		 * 이동 response.sendRedirect("main.do");
		 * 
		 * // 컨트롤러를 실행하지 않는다.(요청페이지로 이동하지 않는다) }
		 * 
		 * HttpSession session = request.getSession();
		 * 
		 * if(session.getAttribute("userSession") != null) { // 로그인 페이지로 이동
		 * modelAndView.addObject("userSession", session.getAttribute("userSession"));
		 * // 컨트롤러를 실행하지 않는다.(요청페이지로 이동하지 않는다) }
		 

	}*/
}