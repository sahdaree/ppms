package egovframework.example.code.service.impl;

import java.util.HashMap;
import java.util.List;

import egovframework.example.code.service.CodeService;
import egovframework.example.sample.service.SampleDefaultVO;
import egovframework.example.sample.service.impl.SampleDAO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service("codeService")
public class CodeServiceImpl extends EgovAbstractServiceImpl implements CodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CodeServiceImpl.class);

	//선언부 해당 DAO 사용시 선언
	@Resource(name = "codeDAO")
	private CodeDAO codeDAO;

	
	@Override
	public List detailCodeselect(HashMap map) throws Exception {
		return codeDAO.detailCodeselect(map);
	}
}
